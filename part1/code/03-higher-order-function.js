
const map = (data,fn)=>{
    
    let result = []
    for(let item of data){
        result.push(fn(item))
    }
    return result

}

console.log(map([1,2,3],item=>item*2));

const every = (data,fn)=>{
    let result=true
    for(let item of data){
        result = fn(item)
        if(!result) break
    }
    return result
}

console.log(every([1,2,3],item=>item<4));
console.log(every([1,2,3],item=>item<3));

const some = (data,fn)=>{
    let result=false
    for(let item of data){
        result = fn(item)
        if(result) break
    }
    return result
}

console.log(some([1,2,3],item=>item>2));
console.log(some([1,2,3],item=>item>3));


const queuedObservers = new Set();

const observe = fn => queuedObservers.add(fn);
const observable = obj => new Proxy(obj, {set});

function set(target, key, value, receiver) {
  const result = Reflect.set(target, key, value, receiver);
  queuedObservers.forEach(observer => observer());
  return result;
}

const person = observable({
    name: '张三',
    age: 20
  });
  
  function print() {
    console.log(`${person.name}, ${person.age}`)
  }
  
  observe(print);
  person.name = '李四';