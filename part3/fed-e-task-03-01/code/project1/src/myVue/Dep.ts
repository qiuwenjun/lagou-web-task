class Dep{
    subs:Watcher[]
    static target:Watcher
    constructor(){
        this.subs = []  //存储所有的观察者
    }
    addSub(watcher:Watcher){ //添加观察者
        if(watcher?.update){
            this.subs.push(watcher)
        }
    }
    notify(){   //发送通知
        this.subs.forEach(item=>item.update())
    }
}