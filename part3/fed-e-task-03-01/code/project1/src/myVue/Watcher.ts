class Watcher{
    oldValue:any
    constructor(public vm:Vue,public key:string,public cb:Function){
        Dep.target = this
        this.oldValue = vm[key]
        Dep.target = null
    }
    update(){
        let newVal = this.vm[this.key];
        if(this.oldValue === newVal) return;
        this.oldValue = newVal
        this.cb(newVal)
    }
}