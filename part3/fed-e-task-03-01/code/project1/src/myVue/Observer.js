var Observer = /** @class */ (function () {
    function Observer(data) {
        this.data = data;
        this.walk(data);
    }
    Observer.prototype.walk = function (data) {
        if (!data || typeof data !== 'object')
            return;
        for (var attr in data) {
            this.defineReactive(data, attr, data[attr]);
        }
    };
    Observer.prototype.defineReactive = function (obj, attr, val) {
        var _this = this;
        this.walk(val); //如果val是对象，把val内部的属性转换成响应式数据
        var dep = new Dep(); //收集依赖发送通知
        Object.defineProperty(obj, attr, {
            enumerable: true,
            configurable: true,
            get: function () {
                Dep.target && dep.addSub(Dep.target);
                return val; //不能是obj[attr],这里会发生死递归
            },
            set: function (newVal) {
                if (newVal === val)
                    return;
                val = newVal;
                dep.notify();
                _this.walk(newVal);
            }
        });
    };
    return Observer;
}());
