// function miniTable<Type extends { length:number }>(a:Type,miniNum:Number):Type{
//     if(a.length >= miniNum){
//         return a
//     }else{
//         type tType = typeof a;
//         if(Array.isArray(a)){

//         }
//     }
// }
// type numList=(string)[]
// type num = keyof numList
// let num:num = 1

// type strMap={a:string,b:number}
// type str = keyof strMap
// let str:str = 'a'
// let arr2=[1,2,3]
// type num2=typeof arr2[0]
// let num2:num2=1

// let num = 
function combile<type>(arr1:type[],arr2:type[]):type[]{
    return arr1.concat(arr2)
}
const numArr1 = combile([1,2,3],[4,'5',6])
const numArr2 = combile([1,2,3],[4,5,6])

export {}

/*
    1 可能的情况下，使用类型参数本身，而不是对其进行约束
    2 总是尽可能少地使用类型参数
    3 如果一个类型的参数只出现在一个地方，请重新考虑是否真的需要它
*/
function Fn1<type>(type:type[]){
    return type
}
Fn1([1,2,3])  //function Fn1<number>(type:number[]):number[]  这里是ts帮我们进行类型推导，传入的实参类型是number[],而泛型是type。默认泛型传参就是number[]，但是这时候实参对应的形参值类型是type[]，所以这时候ts就把type推导成number相当于解构赋值一样type[] = number[]
function Fn2<type extends any[]>(type:type[]){
    return type
}
Fn2([[1,2,3]]) //function Fn2<number[]>(type:number[]):number[]  这里是ts帮我们进行类型推导，传入的实参类型是number[],而泛型是type。默认泛型传参就是number[]，但是这时候实参对应的形参值类型是type，所以这时候ts就把type推导成number[]

function Fn3<type>(type:type){
    return type
}
Fn3([1,2,3])

function MyForEach(arr:any[],callback:(item:any,index:number)=>void){
    let index=0
    for(let item of arr){
        callback(item,index++)
    }
}
MyForEach([1,2,3],(item,index)=>{
    console.log(item)
    console.log(index);
})

/*
    函数重载
        重载签名
            function makeDate(timeStamp):Date
            function makeDate(m:number,d:number,y:number):Date
        实现签名
            function makeDate(mOrTimestamp:number,d:number,y:number):Date

        实现签名是对重载签名的兼容,实现签名的形参名称并不一定需要和重载签名的形参名称一致
        当我们使用函数的时候，ts在做静态类型检查的时候，是拿函数中的重载签名进行检查
*/
function makeDate(timeStamp:number):Date
function makeDate(m:number,d:number,y:number):Date
// function makeDate(m:number,d?:number,y?:number):Date
function makeDate(mOrTimestamp:number,d?:number,y?:number):Date{
    if(d !== undefined && y !== undefined){
        return new Date(y,mOrTimestamp,d)
    }else{
        return new Date(mOrTimestamp)
    }
}
makeDate(1000)
makeDate(1,2,3)
makeDate(1,2)

//参数不正确
// function fn(x:number):void
// function fn(){

// }
// fn(1)

//参数类型不正确
//返回值类型不正确

interface iUser{
    id:number,
    admin:boolean,
    becomeAdmin<This=iUser>(this:This):void
}

const user:iUser = {
    id:123,
    admin:false,
    becomeAdmin(){
        this.admin=true
    }
}

interface Fn{
    getName(str:string):string
    getName(str:number):number
}

interface User{
    admin:boolean
}
interface iDb{
    filterUsers(filter:(this:User)=>Boolean):User[]
}

const db:iDb={
    filterUsers(filter){
        let user1:User={
            admin:true
        }
        let user2:User={
            admin:false
        }
        return [user1,user2].filter(filter)
    }
}

db.filterUsers(function(this:User){
    return this.admin
})

let Obj:object=new String

type iArr = number[]
let iArr:iArr = [1,2,3]
function map(this:iArr,callback:<num>(item:num)=>num)
function map(this:iArr,callback:Function){
    console.log(this)
    for(let item of this){
        callback(item)
    }
}
map.call(iArr,(item)=>{
    return item
})
interface Arr<T>{
    map<U>(callback:(item:T,index:number)=>U):U[]
}
iArr.map(item=>{
    console.log(item)
})
let arr3:number[]=[1,2,3]
arr3.push(4,5,6)


function sum({a,b,c:d}:{a:number,b:number,c:number}){
    console.log(a,b,d);
    
}

type voidFunc = () => void;
const f1:voidFunc = function(){
    return true
}

interface Person{
    name:string;
    age:number
}
type rPerson = Readonly<Person>

let writabelPerson:Person = {
    name:'邱文军',
    age:25
}
let readonlyPerson:rPerson = {
    name:"邱文军",
    age:25
}

writabelPerson=readonlyPerson
writabelPerson.name='222'


interface Point {
    readonly [prop: number]: number,
    readonly [prop: string]: Object,
    0:number,
    x: string;
    y: number;
} 

/*
    索引签名(任意属性)
    分为两种
        1 属性名类型为string [prop:string]:blob
        2 属性名类型为number [prop:number]:file

        1 属性名类型为number的值类型永远是string的子类型
        2 当前类型的索引签名只对当前的类型起作用
        比如
            interface Point {
                readonly [prop: number]: number;  //索引签名为number的后续所有属性都受到当前类型控制
                0:number,
                // 0:string, 
                readonly [prop: string]: Object;  //索引签名为string的后续所有属性都受到当前类型控制
                x: string;
                y: number;
            } 
         属性类型为number索引签名，受上面的number索引控制，这时候值类型必须是指定类型
         属性类型为string索引签名，受上面的number索引控制，这时候值类型必须是指定类型
         当字符串索引和数字索引同时存在时，数字索引的值类型必须是字符串索引值类型的子类型

*/

let Point:Point = {
    0:1,
    1:2,
    x:'x',
    y:0
}
// Point[0]=2
Point.x='y'
Point[0]=3
Point[1]=4
// Point[1]=3
