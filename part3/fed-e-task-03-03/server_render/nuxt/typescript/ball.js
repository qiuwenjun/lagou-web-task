"use strict";
class Ball {
    constructor(props) {
        this.x = 0;
        this.y = 0;
        this.w = 0;
        this.h = 0;
        this.scaleX = 1;
        this.scaleY = 1;
        this.alpha = 1;
        this.r = 0;
        this.fillStyle = 'rgb(68 186 255)';
        this.strokeStyle = 'rgb(163,163,163)';
        Object.assign(this, props);
    }
    render(ctx) {
        ctx.save();
        ctx.beginPath();
        ctx.clearRect(0, 0, 800, 800);
        ctx.resetTransform();
        ctx.translate(this.x, this.y);
        ctx.scale(this.scaleX, this.scaleY);
        ctx.arc(0, 0, this.r, 0, 2 * Math.PI);
        ctx.fillStyle = this.fillStyle;
        ctx.strokeStyle = this.strokeStyle;
        ctx.globalAlpha = this.alpha;
        ctx.fill();
        ctx.stroke();
        ctx.restore();
    }
}
function fn(fn) {
    console.log(fn);
}
fn(true);
class A {
    constructor(a, b) {
    }
}
new A(1, '2');
let Config = {
    square: (event) => {
    }
};
function move(str) {
    return str;
}
let As = move;
As('1');
function Move(args) {
    return args;
}
let MoveInstance1 = Move;
let MoveInstance2 = Move;
let MoveInstance3 = Move;
let MoveInstance4 = Move;
let MoveInstance5 = function (arg) {
    return arg;
};
const obj = {
    a: 1
};
function getAjax(args) {
    return args;
}
getAjax({ a: 1, b: 2, c: 3, length: 3 });
function getAjax2(args, key) { }
getAjax2({ a: 1, b: 2, c: 3, length: 3 }, 'a');
class Zoom {
    constructor(length) {
        this.length = length;
    }
    static push() { }
    static pop() { }
    push(a) { }
}
let Zoom2 = Zoom;
function Type(fn) {
    return new fn;
}
Type(Blob);
Type(File);
let val1 = '1';
let val2 = 2;
// let val4:strList2 = '1'
let val5 = 2;
//原因就是属性中，number是string的子类型
function Fn2() {
    return { name: '邱文军', age: 25 };
}
let box = () => ({
    name: '123',
    age: 123
});
