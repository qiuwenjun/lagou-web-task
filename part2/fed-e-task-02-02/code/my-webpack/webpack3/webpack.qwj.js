const Path = require('path');
// const PostCssPresetEnv = require('postcss-preset-env')
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const { DefinePlugin } = require('webpack')
const CopyWebpackPlugin = require('copy-webpack-plugin')

module.exports = {
    devtool:false,
    mode:"development",
    entry:'./src/index.js',
    output:{
        filename:'[name].[hash:6].js',
        path:Path.resolve(__dirname,'dist')
    },
    devServer:{
        hot:true
    },
    module:{
        rules:[
            {
                test:/\.css$/,
                use:[
                    "style-loader",
                    {
                        loader:"css-loader",
                        options:{
                            esModule:false,
                            importLoaders:1
                        }
                    },
                    'postcss-loader',
                    // {
                    //     loader:"postcss-loader",
                    //     options:{
                    //         postcssOptions:{
                    //             plugins:[
                    //                 PostCssPresetEnv()
                    //             ]
                    //         }
                    //     }
                    // }
                ]
            },
            {
                test:/\.less$/,
                use:[
                    "style-loader",
                    {
                        loader:"css-loader",
                        options:{
                            esModule:false,
                            importLoaders:1
                        }
                    },
                    'postcss-loader',
                    // {
                    //     loader:"postcss-loader",
                    //     options:{
                    //         postcssOptions:{
                    //             plugins:[
                    //                 require('postcss-preset-env')
                    //             ]
                    //         }
                    //     }
                    // },
                    "less-loader"
                ]
            },
            {
                test:/\.js$/,
                exclude:/node_modules/,
                use:[
                    'babel-loader'
                    // {
                    //     loader:'babel-loader',
                    //     options:{
                    //         presets:[
                    //             [
                    //                 "@babel/preset-env",
                    //                 {
                    //                     "useBuiltIns":"usage",
                    //                     "corejs":3
                    //                 }
                    //             ]
                    //         ]
                    //     }
                    // }
                ]
            },
            {
                test:/\.(jpe?g|png|gif|svg)$/,
                type:'asset',
                generator:{
                    filename:"./img/[name].[hash:6][ext]"
                },
                parser:{
                    dataUrlCondition:{
                        maxSize:1000*1024
                    }
                }
            },
            {
                test:/\.(ttf|woff2?)$/,
                type:'asset/resource',
                generator:{
                    filename:"./font/[name].[hash:6][ext]"
                },
            }
        ]
    },
    plugins:[
        new CleanWebpackPlugin(),
        new HtmlWebpackPlugin({
            title:'邱文军-webpack-学习',
            template:'./public/index.html'
        }),
        new DefinePlugin({
            "BASE_URL" : JSON.stringify('./')
        }),
        new CopyWebpackPlugin({
            "patterns":[
                {
                    from:"./public",
                    globOptions:{
                        ignore:['**/index.html']
                    }
                }
            ]
        })
    ]
}