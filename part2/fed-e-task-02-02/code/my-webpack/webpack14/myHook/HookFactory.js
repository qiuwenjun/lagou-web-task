"use strict";
exports.__esModule = true;
require("./common");
var HookFactory = /** @class */ (function () {
    function HookFactory() {
        this.options = {
            taps: [],
            args: []
        };
    }
    HookFactory.prototype.args = function (opt) {
        var Args = '';
        if (opt === null || opt === void 0 ? void 0 : opt.before)
            Args += opt.before + ',';
        if (this.options.args)
            Args += this.options.args.join();
        if (opt === null || opt === void 0 ? void 0 : opt.after)
            Args += ',' + opt.after;
        return Args.replace(/,$/, '');
    };
    HookFactory.prototype.head = function () {
        return "var _countNum = this.taps.length; var _x = this._x;";
    };
    HookFactory.prototype.content = function () {
        if (this.type === 'asyncFactoryHook') {
            return "\n                const next = () => {\n                    --_countNum <= 0 && _callback()\n                };\n                for(let i=0; i<_x.length; i++){\n                    _x[i](...this._argument.slice(0,this._formal.length),next)\n                }\n            ";
        }
        else if (this.type === 'SyncFactory') {
            return this.options.taps.reduce(function (prev, item, index) {
                prev += "var _fn".concat(index, " = _x[").concat(index, "]; _fn").concat(index, "(...this._argument);");
                return prev;
            }, '');
        }
    };
    return HookFactory;
}());
exports["default"] = HookFactory;
