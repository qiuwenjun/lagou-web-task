const Path = require('./path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const InlineChunkHtmlPlugin = require('inline-chunk-html-plugin');
const { DefinePlugin,DllReferencePlugin } = require('webpack');
const { merge } = require('webpack-merge');
const Prod = require('./webpack.prod');
const Dev = require('./webpack.dev');
const TerserPlugin = require('terser-webpack-plugin');
const AddAssetHtmlPlugin = require('add-asset-html-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const SpeedMeasurePlugin = require("speed-measure-webpack-plugin");
const smp = new SpeedMeasurePlugin();

const BaseOpt = env => {
    return {
        entry:{     
            "index":"./src/index.js"
            // index: './src/index.js',
            // main: './src/main.js'
        },             
        output:{
            filename:'js/[name].[contenthash:8].js',
            path:Path('dist'),
        },
        optimization:{
            runtimeChunk:true,//当我们使用
            // chunkIds:'named',
            minimize:true,
            minimizer:[
                new TerserPlugin({              
                    extractComments:false,          //当我们去提取第三方插件的时候，webpack会把第三方插件的一些说明单独提取成一个文件，当我们不想要的时候可以配置这个
                }),
            ],
            splitChunks:{
                chunks:'all', //有效值为async(只处理异步值),initial(只处理同步值),all(同步和异步都处理),
                // minSize:20000, //要生成的块的最小大小（以字节为单位），当小于该值时，不会单独打包，是maxAsyncSize和maxInitialSize集成
                // maxSize:20000, //当大于这个大小时，会进行拆包，而每一个拆包出来的文件大小就是minSize的大小，是maxAsyncSize和maxInitialSize集成
                // minChunks:3,//在拆分之前，模块必须要使用到的最小次数。
                /*
                    这里有优先级问题,如果设置了minSize&maxSize，优先级高于minChunks
                */
                cacheGroups:{   //缓存组，其实就是自定义缓存规则，是私有的规则，这里面的规则高于外部的规则
                    default: {
                        minChunks: 1,
                        filename: 'js/syy_[id].js',
                        priority: -20,      //设置优先级，数值越大优先级越高
                      },
                      qwjRules:{
                        test:/[\\/]node_modules[\\/]/,      //匹配node_modules中的文件
                        filename:'js/[id].bundle.js',  //当且仅当它是初始块时才允许覆盖文件名。此处提供的所有占位符
                        priority: -10,
                    },
                }
            }
        },
        resolve:{
            extensions:[".js",".ts",".jsx",".tsx",".vue",'.css'],
            alias:{
                "@":Path('src')
            }
        },
        module:{
            rules:[
                {
                    test:/\.css$/,
                    sideEffects:true,
                    use:[
                        env ? MiniCssExtractPlugin.loader : "style-loader",
                        {
                            loader:"css-loader",
                            options:{
                                esModule:false,
                                importLoaders:1
                            }
                        },
                        'postcss-loader',
                        // {
                        //     loader:"postcss-loader",
                        //     options:{
                        //         postcssOptions:{
                        //             plugins:[
                        //                 PostCssPresetEnv()
                        //             ]
                        //         }
                        //     }
                        // }
                    ]
                },
                {
                    test:/\.less$/,
                    use:[
                        env ? MiniCssExtractPlugin.loader : "style-loader",
                        {
                            loader:"css-loader",
                            options:{
                                esModule:false,
                                importLoaders:1
                            }
                        },
                        'postcss-loader',
                        // {
                        //     loader:"postcss-loader",
                        //     options:{
                        //         postcssOptions:{
                        //             plugins:[
                        //                 require('postcss-preset-env')
                        //             ]
                        //         }
                        //     }
                        // },
                        "less-loader"
                    ]
                },
                {
                    test:/\.js$/,
                    exclude:/node_modules/,
                    use:[
                        'babel-loader'
                        // {
                        //     loader:'babel-loader',
                        //     options:{
                        //         presets:[
                        //             [
                        //                 "@babel/preset-env",
                        //                 {
                        //                     "useBuiltIns":"usage",
                        //                     "corejs":3
                        //                 }
                        //             ]
                        //         ]
                        //     }
                        // }
                    ]
                },
                {
                    test:/\.(jpe?g|png|gif|svg)$/,
                    type:'asset',
                    generator:{
                        filename:"./img/[name].[hash:6][ext]"
                    },
                    parser:{
                        dataUrlCondition:{
                            maxSize:1000*1024
                        }
                    }
                },
                {
                    test:/\.(ttf|woff2?)$/,
                    type:'asset/resource',
                    generator:{
                        filename:"./font/[name].[hash:6][ext]"
                    },
                },
                {
                    test:/\.jsx$/,
                    exclude:/node_modules/,
                    use:[
                        {
                            loader:'babel-loader',
                        }
                    ]
                }
            ]
        },
        plugins:[
            new HtmlWebpackPlugin({
                title:'邱文军-webpack-学习',
                template:'./public/index.html'
            }),
            new InlineChunkHtmlPlugin(HtmlWebpackPlugin,[/\.js$/]),
            new DefinePlugin({
                "BASE_URL" : JSON.stringify('./')
            }),
            new DllReferencePlugin({
                manifest:Path('./dll/react.manifest.json'),
                context:Path('./')
            }),
            new AddAssetHtmlPlugin({
                outputPath:'auto',
                filepath:Path('./dll/dll_react.js')
            })
        ],
        externals:{ //当我们有些第三方包想直接通过cdn引入，而不是项目中打包时候，就可以配置当前选项
            lodash1:'_',  //这里的key就是当前在项目中引用的包名称，value就是引入的第三方库的名称，这里的lodash的简写就是_
            jQuery:'jQuery'
        }
    }
}

module.exports = function(env){
    console.log(arguments);
    process.env.NODE_ENV = env.production ? 'production' : 'development'
    return smp.wrap(env.production ? merge(BaseOpt(env.production),Prod) : merge(BaseOpt(env.production),Dev))
}
