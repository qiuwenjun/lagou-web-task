var Watcher = /** @class */ (function () {
    function Watcher(vm, key, cb) {
        this.vm = vm;
        this.key = key;
        this.cb = cb;
        Dep.target = this;
        this.oldValue = vm[key];
        Dep.target = null;
    }
    Watcher.prototype.update = function () {
        var newVal = this.vm[this.key];
        if (this.oldValue === newVal)
            return;
        this.oldValue = newVal;
        this.cb(newVal);
    };
    return Watcher;
}());
