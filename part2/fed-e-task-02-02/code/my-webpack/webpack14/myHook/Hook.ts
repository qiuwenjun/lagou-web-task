import './common'
export default abstract class Hook{
    taps:Array<TapOptions>
    _formal:any[]
    _argument:any[]
    _x:Function[]
    constructor(public args:string[]){
        this.taps = [];
        this._formal = args
        this._argument = []
        this._x = []
    }
    tap(name:string|{ name:string },fn:Function){
        if(typeof name === 'string'){
            name = { name }
        }
        this.taps.push(Object.assign({type:"sync",fn},name))
    }
    tapAsync(name:string|{ name:string },fn:Function){
        if(typeof name === 'string'){
            name = { name }
        }
        this.taps.push(Object.assign({type:"async",fn},name))
    }
    call(..._args:any[]){
        this._argument = _args;
        return this.compile().apply(this,_args) 
    }
    callAsync(..._args:any[]){
        this._argument = _args;
        return this.compile().apply(this,_args)
    }
    abstract compile():Function
} 