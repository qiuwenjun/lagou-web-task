import Vue from 'vue'
import Router from 'vue-router'
import { normalizeURL, decode } from 'ufo'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _34a3fea6 = () => interopDefault(import('..\\pages\\order\\index.vue' /* webpackChunkName: "pages/order/index" */))
const _3058c098 = () => interopDefault(import('..\\pages\\user.vue' /* webpackChunkName: "pages/user" */))
const _030e7dae = () => interopDefault(import('..\\pages\\order\\order.vue' /* webpackChunkName: "pages/order/order" */))
const _2d8734b0 = () => interopDefault(import('..\\pages\\_hhh\\index.vue' /* webpackChunkName: "pages/_hhh/index" */))
const _2d2366b6 = () => interopDefault(import('..\\pages\\_home.vue' /* webpackChunkName: "pages/_home" */))
const _cecee908 = () => interopDefault(import('..\\pages\\_index.vue' /* webpackChunkName: "pages/_index" */))
const _62ff04a9 = () => interopDefault(import('..\\pages\\index\\index.vue' /* webpackChunkName: "pages/index/index" */))
const _5e3bc48e = () => interopDefault(import('..\\pages\\index.vue' /* webpackChunkName: "pages/index" */))
const _5c563dc6 = () => interopDefault(import('..\\pages\\_hhh\\hhh.vue' /* webpackChunkName: "pages/_hhh/hhh" */))

const emptyFn = () => {}

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: '/',
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/order",
    component: _34a3fea6,
    name: "order"
  }, {
    path: "/user",
    component: _3058c098,
    name: "user"
  }, {
    path: "/order/order",
    component: _030e7dae,
    name: "order-order"
  }, {
    path: "/:hhh",
    component: _2d8734b0,
    name: "hhh"
  }, {
    path: "/:home",
    component: _2d2366b6,
    name: "home"
  }, {
    path: "/:index",
    component: _cecee908,
    children: [{
      path: "",
      component: _62ff04a9,
      name: "index"
    }, {
      path: "",
      component: _5e3bc48e,
      name: "index"
    }]
  }, {
    path: "/:hhh/hhh",
    component: _5c563dc6,
    name: "hhh-hhh"
  }],

  fallback: false
}

export function createRouter (ssrContext, config) {
  const base = (config._app && config._app.basePath) || routerOptions.base
  const router = new Router({ ...routerOptions, base  })

  // TODO: remove in Nuxt 3
  const originalPush = router.push
  router.push = function push (location, onComplete = emptyFn, onAbort) {
    return originalPush.call(this, location, onComplete, onAbort)
  }

  const resolve = router.resolve.bind(router)
  router.resolve = (to, current, append) => {
    if (typeof to === 'string') {
      to = normalizeURL(to)
    }
    return resolve(to, current, append)
  }

  return router
}
