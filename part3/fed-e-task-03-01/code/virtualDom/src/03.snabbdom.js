import { init, h, classModule, styleModule, propsModule, attributesModule, eventListenersModule } from 'snabbdom'

const Patch = init([
    classModule,
    styleModule,
    propsModule,
    attributesModule,
    eventListenersModule
])

const vNode = h('div#app.cls',{
    class:{a:1},
    style:{
        'font-size':"18px"
    },
    props:{
        hello:"world"
    },
    attrs:{
        hello:"world"
    },
    on:{
        "click":function(){
            console.log('哦吼!!!')
        }
    },

},'邱文军')

console.log(vNode);

const app = document.querySelector('#app');
debugger
Patch(app,vNode)