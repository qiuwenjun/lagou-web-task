class MayBe{
    static of(value){
        return new MayBe(value)
    }
    constructor(private value){}
    map(callback){
        return this.isNothing() ? MayBe.of(null) : MayBe.of(callback(this.value))
    }
    isNothing(){
        return this.value === null || this.value === undefined
    }
}

let m = MayBe.of('qiuwenjun & wujieyin')
console.log(m.map(val=>val.toUpperCase()));
