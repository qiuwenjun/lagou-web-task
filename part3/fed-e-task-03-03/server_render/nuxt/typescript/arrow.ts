type kx<obj>={
    [prop in keyof obj]?:obj[prop]
}
class Arrow{
    x:number
    y:number
    w:number
    h:number
    relation:number
    fillStyle:string
    strokeStyle:string
    constructor(props:kx<Arrow>){
        this.x = 0;
        this.y = 0;
        this.w = 80
        this.h = 30
        this.relation=0
        this.fillStyle='rgb(68 186 255)'
        this.strokeStyle='rgb(163,163,163)'
        Object.assign(this,props)
    }
    createRect(ctx:CanvasRenderingContext2D){
        ctx.beginPath()
        ctx.moveTo(-this.w/2,-this.h/2)
        ctx.lineTo(this.w/2-15,-this.h/2)
        ctx.lineTo(this.w/2-15,-this.h/2-10)
        ctx.lineTo(this.w/2,0)
        ctx.lineTo(this.w/2-15,this.h/2+10)
        ctx.lineTo(this.w/2-15,this.h/2)
        ctx.lineTo(-this.w/2,this.h/2)
        ctx.closePath()
        return this
    }
    render(ctx:CanvasRenderingContext2D){
        ctx.clearRect(-400,-400,800,800)
        ctx.resetTransform()
        ctx.translate(this.x,this.y)
        ctx.rotate(this.relation)
        this.createRect(ctx)
        ctx.strokeStyle = this.strokeStyle
        ctx.stroke()
        ctx.fillStyle = this.fillStyle
        ctx.fill()

    }
}
