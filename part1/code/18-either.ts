class Left{
    static of(value){
        return new Left(value)
    }
    constructor(private value){}
    map(){
        return this
    }
}

class Right{
    static of(value){
        return new Right(value)
    }
    constructor(private value){}
    map(callback){
        return Right.of(callback(this.value))
    }
}

function parseJSON(str){
    try{
        return Right.of(JSON.parse(str))
    }catch(e){
        return Left.of({err:e.message})
    }
}

console.log(parseJSON('{"name":"邱文军"}').map(x=>x.name.split('')));
