import { ref, reactive, computed, watch, watchEffect, defineComponent } from 'vue'

export default defineComponent({
    name:`qiuwenjun`,
    setup(){

        const computed_number = ref(0)

        return {
            computed_number,
            addCount(){
                computed_number.value++
            }
        }
    }
})