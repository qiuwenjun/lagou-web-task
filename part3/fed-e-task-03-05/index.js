#!/usr/bin/env node

const Koa=require('koa')
const send=require('koa-send')

const app=new Koa();
const streamString= stream=>{
    return new Promise((resolve,reject)=>{
        const chunks=[]
        stream.on("data",chunk=>chunks.push(chunk))
        stream.on('end',()=>{
            resolve(Buffer.concat(chunks).toString('utf-8'))
        })
        stream.on('error',reject)
    })
}
app.use(async(ctx,next)=>{
    // console.dir(ctx.request)
    await send(ctx, ctx.path, { root: process.cwd()+"/index.html" });
    await next()
})
app.use(async(ctx,next)=>{
    console.dir(ctx.type)
    if(ctx.type === 'application/javascript'){
        const content=await streamString(ctx.body)
        ctx.body=content.replace(/(form\s+['"])(?!\.\/)/g,'$1/@modules/')
    }
    
    await next()
})
app.listen('3000')
console.log('listening on port 3000');