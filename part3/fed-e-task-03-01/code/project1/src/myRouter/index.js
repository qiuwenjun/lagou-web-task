"use strict";
exports.__esModule = true;
var Parameter = function (data) {
    return typeof data === 'object' ? Object.entries(data).reduce(function (prev, item) { return "".concat(prev).concat(prev === '?' ? '' : '&').concat(item[0], "=").concat(item[1]); }, '?') : data.replace(/^\?/, '').split('&').reduce(function (prev, item) {
        var value = item.split('=');
        prev[value[0]] = value[1];
        return prev;
    }, {});
};
var _Vue;
var VueRouter = /** @class */ (function () {
    function VueRouter(options) {
        this.options = options;
        if (!/history|hash/.test(options.mode)) {
            throw new TypeError("路由类型错误!!!");
        }
        this.routeMap = {};
        this.data = _Vue.observable({ current: this.path() });
    }
    VueRouter.prototype.path = function () {
        if (this.options.mode === 'history') {
            return location.pathname;
        }
        else if (this.options.mode === 'hash') {
            return location.hash.replace('#', '');
        }
    };
    VueRouter.prototype.init = function () {
        this.createRouteMap();
        this.initComponents();
        this.initEvent();
    };
    VueRouter.prototype.initEvent = function () {
        var _this = this;
        if (this.options.mode === 'history') {
            window.addEventListener('popstate', function (_a) {
                var state = _a.state;
                _this.data.current = state;
            }, false);
        }
        else if (this.options.mode === 'hash') {
            window.addEventListener('hashchange', function (_a) {
                var newURL = _a.newURL;
                _this.data.current = new URL(newURL).hash.replace('#', '');
            }, false);
        }
    };
    VueRouter.prototype.createRouteMap = function () {
        this.routeMap = this.options.routes.reduce(function (prev, item) {
            prev[item.path] = item.component;
            return prev;
        }, {});
    };
    VueRouter.prototype.initComponents = function () {
        var This = this;
        _Vue.component('router-link', {
            props: {
                to: {
                    type: [Object, String]
                },
                tag: {
                    type: String,
                    "default": 'a'
                }
            },
            render: function (h) {
                var _this = this;
                var configObj = [this.tag];
                if (this.tag === 'a') {
                    configObj.push({
                        attrs: {
                            href: typeof this.to === 'string' ? this.to : this.to.path + Parameter(this.to.query)
                        },
                        on: {
                            click: function (e) {
                                e.preventDefault();
                                _this.$router.push(typeof _this.to === 'string' ? _this.to : _this.to.path + Parameter(_this.to.query));
                            }
                        }
                    });
                }
                else {
                    configObj.push({
                        on: {
                            click: function () {
                                _this.$router.push(typeof _this.to === 'string' ? _this.to : _this.to.path + Parameter(_this.to.query));
                            }
                        }
                    });
                }
                configObj.push(this.$slots["default"]);
                return h.apply(void 0, configObj);
            }
        });
        _Vue.component('router-view', {
            render: function (h) {
                var Component = This.routeMap[This.data.current];
                return h(Component);
            }
        });
    };
    VueRouter.prototype.push = function (url) {
        if (this.options.mode === 'history') {
            history.pushState(url, '', url);
        }
        else if (this.options.mode === 'hash') {
            location.hash = '#' + url;
        }
        this.data.current = url;
    };
    VueRouter.install = function (Vue) {
        //首先判断是否注册过
        if (VueRouter.isInstall)
            return;
        VueRouter.isInstall = true;
        _Vue = Vue;
        _Vue.mixin({
            beforeCreate: function () {
                if (_Vue.prototype.$router instanceof VueRouter)
                    return;
                _Vue.prototype.$router = this.$options.router;
                this.$router.init();
            }
        });
    };
    return VueRouter;
}());
exports["default"] = VueRouter;
