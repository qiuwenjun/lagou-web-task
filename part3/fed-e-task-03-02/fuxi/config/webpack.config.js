const Path = require('./path');
const Dev = require('./webpack.dev');
const Prod = require('./webpack.prod');
const { merge } = require('webpack-merge');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const baseOpt = () => (
    {
        entry:{
            main:'./src/main.js'
        },
        output:{
            filename:'[name].[contenthash:8].js',
            path:Path('dist'),
            publicPath:''
        },
        resolve:{
            alias:{
                '@':Path('src')
            }
        },
        module:{
            rules:[
                {
                    test:/\.less$/,
                    use:[
                        'style-loader',
                        {
                            loader:'css-loader',
                            options:{
                                esModule:false,
                                importLoaders:2
                            }
                        },
                        {
                            loader:'less-loader',
                        },
                        'postcss-loader'
                    ]
                },
                {
                    test:/\.tsx?$/,
                    use:[
                        {
                            "loader":"babel-loader"
                        },
                        {
                            "loader":"ts-loader"
                        }
                    ]
                }
            ]
        },
        plugins:[
            new HtmlWebpackPlugin({
                filename:Path('./public/index.html')
            })
        ]
    }
)
module.exports = (env)=>{
    console.log(env);
    return env.development ? merge(baseOpt(),Dev()) : merge(baseOpt(),Prod())
}