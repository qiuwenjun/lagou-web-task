const Path = require('path');

module.exports = {
    entry:"./src/index.js",
    mode:'development',
    devtool:false,
    output:{
        filename:"[name].[hash:8].js",
        path:Path.resolve(__dirname,'dist'),
        library:"Last",         //库名称,也就是当前模块的名称
        libraryTarget:'umd',    //打包出来的文件使用什么模块化规范
        globalObject:"this"     //内部self指向谁，this 浏览器下指向window，node下指向global
    }
}