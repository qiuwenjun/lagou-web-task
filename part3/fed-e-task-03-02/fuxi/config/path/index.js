const Path = require('path');

const baseDir = process.cwd();
module.exports = (__dirname)=>{
    return Path.resolve(baseDir,__dirname)
}