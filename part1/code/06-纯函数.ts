const _ = require('lodash')

function getArea(r){
    return Math.PI * (r ** 2)
}

// const getAreaWithMemory = _.memoize(getArea)
// console.log(getAreaWithMemory);
// console.log(getAreaWithMemory(4));
// console.log(getAreaWithMemory(4));
// console.log(getAreaWithMemory(4));


function customMemoize(fn){
    const catchs = new Map()
    return function(){
        let argumentsify = JSON.stringify(arguments)
        if(!catchs.has(argumentsify)){
            catchs.set(argumentsify,fn(...arguments))
        }
        return catchs.get(argumentsify)
    }
}

const getAreaWithMemory = customMemoize(getArea)
console.log(getAreaWithMemory.toString());
console.log(getAreaWithMemory(4));
console.log(getAreaWithMemory(4));
console.log(getAreaWithMemory(4));