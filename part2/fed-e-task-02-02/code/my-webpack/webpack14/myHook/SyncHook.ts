import Hook from './Hook'
import HookFactory from './HookFactory'
import './common'

class SyncFactory extends HookFactory{
    constructor(){
        super()
        this.type='SyncFactory'
    }
    setup(instance:SyncHook,options:FactoryOpt){
        this.options = options
        instance._x = options.taps.map((item:TapOptions)=>item.fn)
    }
    create(options:FactoryOpt){
        return new Function(this.args(),this.head()+this.content())
    }
}

const factory = new SyncFactory()

export default class SyncHook extends Hook{
    constructor(args:string[]){
        super(args)
    }
    compile() {
        factory.setup(this,{
            taps:this.taps,
            args:this.args
        })
        return factory.create({
            taps:this.taps,
            args:this.args
        })
    }
}