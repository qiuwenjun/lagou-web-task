/*
    grunt的基本使用
*/
module.exports = grunt => {

    // console.log(grunt);
    //qwj1任务，两个参数
    grunt.registerTask('qwj1',()=>{
        console.log('qwj1');
    })

    //qwj2任务，三个参数，第二参数是当前任务描述
    grunt.registerTask('qwj2','qwj2',()=>{
        console.log('qwj2');
        return false
    })

    //qwj3任务，标记任务失败
    grunt.registerTask('qwj3',()=>{
        console.log('qwj3');
    })

    //async-task1 异步任务
    grunt.registerTask('async-task1',function(){
        setTimeout(()=>{
            console.log('async-task异步任务');
            done()
        },1000)
    })

    //async-task2 异步任务失败
    grunt.registerTask('async-task2',function(){
        const done = this.async()
        setTimeout(()=>{
            console.log('async-task异步任务');
            done(false)
        },1000)
    })

    //组合任务
    grunt.registerTask('qwj4',[
        'async-task',
        'qwj1',
        'qwj2',
        'qwj3'
    ])

    //默认任务
    grunt.registerTask('default','默认任务',()=>{
        console.log('默认任务123');
    })

}