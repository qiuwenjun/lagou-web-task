module.exports = plop => {
    plop.setGenerator('vue_component_template',{
        description: '创建文件，注意必须符合项目约束规范', // 命令启动描述
        prompts:[
            {
                type:'input',           // 命令方式，input-输入，list-选择，confirm-是否
                name:"path",            // 当前这个用户输入后的接收字段
                message:"项目路径名称",  // 提示给用户的信息
                // default:'MyComponent'   //默认值
            },
            {
                type:'input',
                name:"project",
                message:"文件夹名称",
                // default:'MyComponent'   //默认值
            },
            {
                type:'input',
                name:'file',
                message:"文件名称",
                // default:'MyComponent'   //默认值
            }
        ],
        actions:data => {
            
            const { path, project, file } = data
            const actions = []
                  
            //视图层
            actions.push({
                type: 'add',
                path: `./src/{{ path }}/{{ project }}/{{ file }}.html`, // 生成的文件存放路径
                templateFile: './plop_temp/index_html.hbs', // 模板文件路径
                data: { // 传递的数据给模板文件
                    path, project, file
                }
            })
            
            //样式层
            actions.push({
                type:'add',
                path: `./src/{{ path }}/{{ project }}/{{ file }}.less`, // 生成的文件存放路径
                templateFile: './plop_temp/index_less.hbs', // 模板文件路径
            })

            //逻辑层
            actions.push({
                type:'add',
                path: `./src/{{ path }}/{{ project }}/{{ file }}.ts`, // 生成的文件存放路径
                templateFile: './plop_temp/index_ts.hbs', // 模板文件路径
                data: { // 传递的数据给模板文件
                    path, project, file
                }
            })

            //最后的拼接到vue文件
            actions.push({
                type:'add',
                path: `./src/{{ path }}/{{ project }}/index.vue`, // 生成的文件存放路径
                templateFile: './plop_temp/index.hbs', // 模板文件路径
            })

            return actions
        }
    })
}