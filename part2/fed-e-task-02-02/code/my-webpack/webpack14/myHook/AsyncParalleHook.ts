import Hook from './Hook';
import HookFactory from './HookFactory';
class AsyncFactory extends HookFactory{
    constructor(){
        super()
        this.type = 'asyncFactoryHook'
    }
    setup(instance:AsyncParalleHook,options:FactoryOpt){
        instance._x = options.taps.map(item=>item.fn)
        this.options = options
    }
    create(options:FactoryOpt){
        return new Function(this.args({ "after":"_callback" }),this.head()+this.content())
    }
}

const factory = new AsyncFactory()
export default class AsyncParalleHook extends Hook{
    compile(){
        factory.setup(this,{
            taps:this.taps,
            args:this.args
        })
        return factory.create({
            taps:this.taps,
            args:this.args
        })
    }
}
