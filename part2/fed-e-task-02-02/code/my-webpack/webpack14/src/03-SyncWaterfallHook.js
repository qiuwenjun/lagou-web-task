const { SyncWaterfallHook } = require('tapable')

const sync = new SyncWaterfallHook(['name', 'age'])

sync.tap('fn1', function (name, age) {
  console.log(name, age)
})

sync.tap('fn2', function (name, age) {
  console.log(name, age)
  return false
})

sync.tap('fn3', function (name, age) {
  console.log(name, age)
})

sync.call('邱文军', 25)
