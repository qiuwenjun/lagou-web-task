module.exports = grunt => {
    
    grunt.initConfig({
        build:{
            foo:123,
            bar:456
        },
        serve:{
            options:{
                foo:'bar'
            },
            one:1,
            two:{
                options:{
                    foo:'baz'
                }
            },
        }
    });

    // grunt.registerTask('build',()=>{
    //     console.log(grunt.config('build'));
    // })
    //registerMultiTask 可以将一个初始化任务下的多个子任务按顺序执行
    grunt.registerMultiTask("build",function(){         
        console.log(this);
        console.log(grunt.config('build'));
    })

    grunt.registerMultiTask("serve",function(){
        console.log(this);
        console.log(this.options());
        console.log(grunt.config('serve'));
    })

}