import { first } from 'lodash';
import fp = require('lodash/fp')

const f = fp.flowRight(str=>str.replace(/\s+/,'_'),fp.toLower)
// const f = str => str.replace(/([A-Z])([a-z]+)\s+([A-Z])([a-z]+)/,function(all,firstLetter,first,lastLetter,last){
//     console.log(arguments);
//     return `${firstLetter.toLowerCase()}${first}_${lastLetter.toLowerCase()}${last}`
// })

console.log(f('Hello     World'));  //hello_world

// 把一个字符串中的首字母提取并转换成大写, 使用. 作为分隔符
// world wild web ==> W. W. W

const f1 = fp.flowRight(fp.join('. '),fp.map(fp.flowRight(fp.first,fp.toUpper)),fp.split(/\s+/))

console.log(f1('world wild web'));  //W. W. W