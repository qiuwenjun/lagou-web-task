module.exports = () => (
    {
        mode:'development',
        devtool:'eval-cheap-module-source-map',
        devServer:{
            hot:'only',
        }
    }
)