import { partial } from "lodash"

function sum(data:Array<{[prop:string]:any}>,num?:number):Promise<number>
function sum(data:{[prop:string]:any},num?:number):Promise<number>
function sum(data:any,num=0):any{
    if(Object.prototype.toString.call(data) === '[object Object]'){
        data = [data]
    }
    return new Promise<number>(function(resolve){
        let pList:Promise<number>[]=[]
        for(let item of data){
            for(let attr in item){
                if(attr === 'num'){
                    num+=item[attr]
                }else if(attr === 'children'){
                    pList.push(Promise.resolve().then(()=>sum(item[attr])))
                }
            }            
        }
        Promise.all<Promise<number>[]>(pList).then((res:number[])=>{
            let nums:number = res.reduce((prev:number,item:number)=>prev+item,0)
            resolve(nums+num)
        }) 
    })

}

type objects = Partial<{
    num:number,
    children:objects[]
}>
const node:objects = {
    num: 1,
    // children 无限个
    children: [{
      num: 3,
      // children的子项都是一样的数据结构
      children: [{
          num:12,
          children:[
              {
                  num:34,
                  children:[]
              }
          ]
      },{
          num:55,
          children:[]
      }]
    },{
      num: 8,
      children: [{
          num:23
      },{
          num:4
      }]
    }]
}
sum(node).then(num=>{
    console.log(num);
})