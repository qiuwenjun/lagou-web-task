const fs = require('fs')
import fp = require('lodash/fp')

class Io{
    static of(value){
        return new Io(function(){
            return value
        })
    }
    constructor(private value:()=>void){}
    map(fn){
        return new Io(fp.flowRight(fn,this.value))
    }
}

let readFile = function(filename){
    return new Io(function(){
        return fs.readFileSync(filename,'utf-8')
    })
}

let print = function(file){
    return new Io(function(){
        return file
    })
}

let f = readFile('package.json')