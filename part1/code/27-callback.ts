function foo(callback:(a:string,b:number)=>Array<string|number>){}

foo(function(a:string,b:number):Array<string|number>{
    return [a,b]
})

let a:unknown = '1'
let b:any = '2'
// let c:number = a as number
// let c:number = <number>a
let d:undefined = undefined
let e:any = a
let f:unknown = b

export {}

// declare global{
//     interface Array<T>{
//         indexData:<T>(callback:()=>)=>
//     }
// }
[[1,2,3]].flatMap(item=>item.reduce((prev,item)=>prev+item,0))
abstract class App{
    constructor(){

    }
}

interface Post{
    title:string;
    content:string
}
function getAjax(post:Post){
    console.log(post.title);
    console.log(post.content);
}
let post = {
    id:1,
    title:"邱文军",
    content:"我的天哪"
}
getAjax(post)

function getPost({name,title}:{name:string,title:string}){
    
}
getPost({
    name:'123',
    title:"123"
})

export default {}