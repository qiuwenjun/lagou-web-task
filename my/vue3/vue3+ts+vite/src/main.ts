import { createApp } from 'vue'
import { createPinia } from 'pinia'

import App from './App.vue'
import router from './router'

import './assets/main.css'

const app = createApp(App)

app.use(createPinia())
app.use(router)

app.mount('#app')


let a:Array = new Array<string>('1','2','3')

interface As{
    new (num:number):number
}

var As:As

new As(1)