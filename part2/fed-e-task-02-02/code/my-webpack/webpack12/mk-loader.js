const md = require('markdown').markdown
// 每一个webpack的loader都需要去导出一个函数，这个函数就是我们这个loader的对我们所加载到的资源的一个处理过程，它的输入就是我们资源文件的内容，输出就是我们此次加工过后的一个结果。
module.exports = source => {
  const data = md.parse(source)
  // console.log(data)
  return `
        //mk-loader开始
        const dataMd=${JSON.stringify(data)};
        export default dataMd
        //mk-loader结束
    `
}