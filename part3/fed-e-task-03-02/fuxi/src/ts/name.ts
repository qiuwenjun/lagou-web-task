/*
 * @Author: 非洲小白狼 1501480244@qq.com
 * @Date: 2022-08-15 17:01:24
 * @LastEditors: 非洲小白狼 1501480244@qq.com
 * @LastEditTime: 2022-08-15 17:08:14
 * @FilePath: \fuxi\src\ts\name.ts
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
namespace A1{
    export interface User{
        name:string,
        age:number,
        gender:boolean
    }
    export type one=1
    export let a:string='1'
    export let b:one=1
    export let c:boolean=true
    export let d:symbol=Symbol(1)
    export let User:User={
        name:'邱文军',
        age:25,
        gender:true
    }
}

namespace A2{
    let a:A1.User={
        name:'程明',
        age:23,
        gender:true,
    }
    let value:A1.User=A1.User
    let one:A1.one=1
}

namespace k1 {
    let a = 10;
    export var obj = {
        a
    }
    export let c=1
}

namespace k1 {
    let b = 10;
    export var obj2 = {
        b
    }
    export let c=2
}
namespace K2{
    let a=k1.obj
    let type:string=typeof a
    console.log(type)
    type obj=typeof a
    let b:typeof k1.obj2={
        b:10
    }
    console.log(b)
}