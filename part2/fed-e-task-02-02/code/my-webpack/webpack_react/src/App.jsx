import React,{Component} from "react";

export default class App extends Component{
    state={
        title:'前端开发'
    }
    constructor(props){
        super(props);
    }
    render(){
        return (
            <div className="app">
                <h2>{this.state.title}</h2>
            </div>
        )
    }
}