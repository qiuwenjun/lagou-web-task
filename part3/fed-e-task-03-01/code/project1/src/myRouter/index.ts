import Vue, { AsyncComponent } from 'vue'

interface Path{
    path:string
    name?:string
    component:AsyncComponent,
    props?:boolean|Object|(()=>Object)
    alias?:string
    redirect?:string|Object|(()=>Object)
    children?:Path[]
    beforeEnter?:Function
    meta?:any,
}

interface iOpt{
    mode:'history'|'hash'
    routes:Path[]
    scrollBehavior?:Function
}

const Parameter = (data:object|string)=>{
    return typeof data === 'object' ? Object.entries(data).reduce((prev,item)=>`${prev}${prev === '?' ? '' : '&'}${item[0]}=${item[1]}`,'?') : data.replace(/^\?/,'').split('&').reduce((prev,item)=>{
        let value = item.split('=')
        prev[value[0]] = value[1]
        return prev
    },{});
}

let _Vue;
export default class VueRouter{
    routeMap:{[prop:string]:string}
    data:{[prop:string]:string}
    constructor(public options:iOpt){
        if(!/history|hash/.test(options.mode)){
            throw new TypeError("路由类型错误!!!")
        }
        this.routeMap = {}
        this.data = _Vue.observable({current:this.path()})
    }
    path(){
        if(this.options.mode === 'history'){
            return location.pathname
        }else if(this.options.mode === 'hash'){
            return location.hash.replace('#','')
        }
    }
    init(){
        this.createRouteMap()
        this.initComponents()
        this.initEvent()
    }
    initEvent(){
        if(this.options.mode === 'history'){
            window.addEventListener('popstate',({state})=>{
                this.data.current = state
            },false)
        }else if(this.options.mode === 'hash'){
            window.addEventListener('hashchange',({newURL})=>{
                this.data.current = new URL(newURL).hash.replace('#','')
            },false)
        }
    }
    createRouteMap(){               //创建路由和组件的映射关系    
        this.routeMap = this.options.routes.reduce((prev,item)=>{
            prev[item.path] = item.component
            return prev
        },{})
    }
    initComponents(){               //初始化组件
        const This = this
        _Vue.component('router-link',{
            props:{
                to:{
                    type:[Object,String]
                },
                tag:{
                    type:String,
                    default:'a'
                }
            },
            render(h) {
                const configObj = [this.tag]
                if(this.tag === 'a'){
                    configObj.push({
                        attrs:{
                            href:typeof this.to === 'string' ? this.to : this.to.path + Parameter(this.to.query)
                        },
                        on: {
                            click:(e)=>{
                                e.preventDefault()
                                this.$router.push(typeof this.to === 'string' ? this.to : this.to.path + Parameter(this.to.query))
                            }
                        },
                    })
                }else{
                    configObj.push({
                        on: {
                            click:()=>{
                                this.$router.push(typeof this.to === 'string' ? this.to : this.to.path + Parameter(this.to.query))
                            }
                        },
                    })
                }
                configObj.push(this.$slots.default)
                return h(...configObj)
            },
        });
        _Vue.component('router-view',{
            render(h) {
                const Component = This.routeMap[This.data.current]
                return h(Component)
            },
        });
    }
    push(url){
        if(this.options.mode === 'history'){
            history.pushState(url,'',url)
        }else if(this.options.mode === 'hash'){
            location.hash='#'+url
        }
        this.data.current = url
    }
    static isInstall:Boolean        //判断是否注册过
    static install(Vue:Vue){
        //首先判断是否注册过
        if(VueRouter.isInstall) return;
        VueRouter.isInstall = true;
        _Vue = Vue;
        _Vue.mixin({
            beforeCreate() {
                if(_Vue.prototype.$router instanceof VueRouter) return;
                _Vue.prototype.$router = this.$options.router
                this.$router.init()
            },
        })
    }
}