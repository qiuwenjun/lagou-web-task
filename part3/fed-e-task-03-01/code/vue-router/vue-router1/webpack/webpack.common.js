const Path = require('./path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { DefinePlugin } = require('webpack');
const { merge:Merge } = require('webpack-merge');
// const CssMinimizerWebpackPlugin= require('css-minimizer-webpack-plugin');
const { default:VueLoaderPlugin } = require('vue-loader/dist/plugin.js');

const baseOpt = env => ({
    entry:{
        main:'./src/main.ts'      
    },
    output:{
        filename:"js/[name].[contenthash:8].js",
        path:Path('dist')
    },
    resolve:{
        extensions:['.js','.vue','.ts','.tsx'],
        alias:{
            '@':Path('src')
        }
    },
    optimization:{
        minimize:true
    },
    module:{
        rules:[
            {
                test:/\.ts$/,
                exclude:/node_modules/,
                use:[
                    'babel-loader'
                ]
            },
            {
                test:/\.vue$/,
                exclude:/node_modules/,
                use:[
                    {
                        loader:'vue-loader',
                        options:{
                            reactivityTransform:true,
                            enableTsInTemplate:true
                        }
                    },
                ]
            },
            {
              test: /\.ts$/,
              exclude:/node_modules/,
              use: [
                {
                    loader:"ts-loader",
                    options:{
                        appendTsSuffixTo: [/\.vue$/]
                    }
                }
              ],
            },
            {
                test:/\.(png|gif|svg|jpe?g)$/,
                exclude:/node_modules/,
                type:"asset",
                generator:{
                    filename:'img/[name].[hash:8][ext]'
                },
                parser:{
                    dataUrlCondition:{
                        maxSize:1024
                    }
                }
            },
            {
                test:/\.(ttf|woff2?)$/,
                exclude:/node_modules/,
                type:"asset/resource",
                generator:{
                    filename:'font/[name].[chunkhash:8][ext]'
                },
            },
            {
                test:/\.css$/,
                exclude:/node_modules/,
                use:[
                    'style-loader',
                    {
                        loader:'css-loader',
                        options:{
                            esModule:false,
                            importLoaders:1
                        }
                    },
                    'postcss-loader',
                ]
            },
            {
                test:/\.s(a|c)ss$/,
                exclude:/node_modules/,
                use:[
                    'style-loader',
                    {
                        loader:'css-loader',
                        options:{
                            esModule:false,
                            importLoaders:2
                        }
                    },
                    'postcss-loader',
                    'sass-loader'
                ]
            },
            {
                test:/\.less$/,
                exclude:/node_modules/,
                use:[
                    'style-loader',
                    {
                        loader:'css-loader',
                        options:{
                            esModule:false,
                            importLoaders:2
                        }
                    },
                    'postcss-loader',
                    'less-loader'
                ]
            }
        ]
    },
    plugins:[
        new HtmlWebpackPlugin({
            title:"VUE2.0",
            template:Path('public/index.html'),
            chunks:['main']
        }),
        new DefinePlugin({
            'BASE_URL':JSON.stringify('./')
        }),
        new VueLoaderPlugin()
    ]
})

module.exports = env => {
    return env.production ? Merge(baseOpt(true),require('./webpack.prod.js')) : Merge(baseOpt(false),require('./webpack.dev.js'))
}