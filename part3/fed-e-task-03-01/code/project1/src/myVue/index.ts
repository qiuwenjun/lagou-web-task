/** person information */
interface iProps{
    [prop:string]:Function|{type:Function,default?:()=>any|any,required?:Boolean,validate?:Function}
}
interface Options{
    props?:iProps|Array<string>
    data?:()=>({[prop:string]:any}) | {[prop:string]:any},
    el:string | HTMLElement
    methods:{[prop:string]:Function}
}
class Vue{
    $el:HTMLElement
    $data:{[prop:string]:any}
    _Observer:Observer
    constructor(public $options:Options){
        this.$el = typeof $options.el === 'string' ? document.querySelector($options.el) as HTMLElement : $options.el; 
        this.$data = typeof $options.data === 'function' ? $options.data() : typeof $options.data === 'object' ? $options.data : {}
        this._proxyData(this.$data)
        this._methods()
        this._Observer = new Observer(this.$data)
        new Compiler(this)
    }
    _proxyData(data:typeof this.$data){
        for(let attr in data){
            Object.defineProperty(this,attr,{
                enumerable:true,
                configurable:true,
                get(){
                    return data[attr]
                },
                set(val){
                    if(val === data[attr]) return;
                    data[attr] = val
                }
            })
        }
    }
    _methods(){
        for(let attr in this.$options.methods){
            this[attr] = this.$options.methods[attr]
        }
    }
    static set(target,key,value){
        return Observer.prototype.defineReactive(target,key,value)
    }
    $set(target,key,value){
        return this._Observer.defineReactive(target,key,value)
    }
}