let C:{
    eventWrapper?:(e:MouseEvent)=>({x:number,y:number})
    getOffset?:(el:HTMLElement)=>({x:Number,y:Number})
    toRad?:(deg:number)=>number
    toDeg?:(rad:number)=>number
} = {}

C.getOffset = function(el){
    let pos = {
        x:0,
        y:0
    }
    el.onmousemove = function(e){
      let position = C.eventWrapper ? C.eventWrapper(e) : {x:0,y:0};
        pos.x=position.x
        pos.y=position.y
    }
    return pos
}

C.eventWrapper = function(e){
    const {clientX,clientY,target} = e
    const {left,top} = (target as Element).getBoundingClientRect()
    return {
        x:clientX-left,
        y:clientY-top
    }
}

C.toRad=deg=>deg/180*Math.PI
C.toDeg=rad=>rad*180/Math.PI