import { init, h } from "snabbdom";

const Patch = init([]);

let vNode = h('div#container.cls','hello world!!!')
let app = document.querySelector('#app')

let oldNode = Patch(app,vNode)

setTimeout(()=>{
    oldNode = Patch(oldNode,h('div#app.app',[
        h('h3','呦吼'),
        h('p','123')
    ]))
    setTimeout(()=>{
        Patch(oldNode,h('div#app.app',h('!')))
    },0)
},2000)