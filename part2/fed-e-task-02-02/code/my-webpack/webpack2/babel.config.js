module.exports = {
    // plugins:[
    //     '@babel/plugin-transform-arrow-functions',
    //     '@babel/plugin-transform-block-scoping'
    // ],	
    presets:[
        [ 
            "@babel/preset-env",
            {
                // "targets":"chrome 102"
                /*
                    false: 不对当前的js处理做polyfill的填充
                    usage: 会自动检查用户所使用到的新api或表达式进行兼容
                    entry: 根据browserslistrc配置文件所查找出来的浏览器进行兼容
                    需要在使用到的地方手动引入
                    import "core-js/stable";
                    import "regenerator-runtime/runtime";
                */ 
                "useBuiltIns":'entry',
                "corejs":3  //当babel执行的时候默认使用的是2的版本，而我们现在安装的是3的版本，所以需要我们手动指定版本
            }
        ]
    ]
}