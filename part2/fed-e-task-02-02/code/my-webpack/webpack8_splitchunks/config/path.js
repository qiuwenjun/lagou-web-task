const Path = require('path');
const dirPath = process.cwd();   //执行这个文件的命令行所在路径

module.exports = (relativePath) => {
    return Path.resolve(dirPath,relativePath)    
}