const Path = require('./path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { DefinePlugin } = require('webpack');
const { merge } = require('webpack-merge')
const Prod = require('./webpack.prod')
const Dev = require('./webpack.dev')
const TerserPlugin = require('terser-webpack-plugin');

const BaseOpt = {
    // context:'',//默认就是执行当前文件的命令行文件所在的路径和process.cwd()方法获取路径的方式保持一致,
    entry:{     //这个没有路径问题，因为这时候的entry路径前面会拼接context上下文路径，而context上下文路径是以执行当前文件的命令行路径为基准的
        // 不依赖任何其他第三方模块或文件
        // 'main1':'./src/main1.js',
        // 'main2':'./src/main2.js'
        // 依赖第三方模块或文件
        // 'lodash_my':'lodash',      //所需要依赖的第三方库
        // 'jquery_my':'jquery',      //所需要依赖的第三方库
        // 'main1':{
        //     import:'./src/main1.js',
        //     dependOn:['lodash','jquery'],
        //     filename:'./main1.js'
        //     // chunkLoading: true,
        // },
        // 'main2':{
        //     import:'./src/main2.js',
        //     dependOn:['lodash','jquery'],
        //     filename:'./main2.js'
        //     // chunkLoading: true,
        // },
        // 'shared':['lodash','jquery'],  //或者使用这种组合的方式
        // 'main1':{
        //     import:'./src/main1.js',
        //     dependOn:'shared',
        //     filename:'./main1.js'
        //     // chunkLoading: true,
        // },
        // 'main2':{
        //     import:'./src/main2.js',
        //     dependOn:'shared',
        //     filename:'./main2.js'
        //     // chunkLoading: true,
        // },
        // 'main':{
        //     import:['./src/main1.js','./src/main2.js'],
        //     dependOn:"shared"
        // },
        'home':"./src/home.js"
    },             
    output:{
        filename:'[name].[hash:6].js',
        path:Path('dist')
    },
    optimization:{
        minimize:true,
        minimizer:[
            new TerserPlugin({              
                extractComments:false,          //当我们去提取第三方插件的时候，webpack会把第三方插件的一些说明单独提取成一个文件，当我们不想要的时候可以配置这个
            }),
        ],
        splitChunks:{
            chunks:'all'
        }
    },
    resolve:{
        extensions:[".js",".ts",".jsx",".tsx",".vue"],
        alias:{
            "@":Path('src')
        }
    },
    module:{
        rules:[
            {
                test:/\.css$/,
                use:[
                    "style-loader",
                    {
                        loader:"css-loader",
                        options:{
                            esModule:false,
                            importLoaders:1
                        }
                    },
                    'postcss-loader',
                    // {
                    //     loader:"postcss-loader",
                    //     options:{
                    //         postcssOptions:{
                    //             plugins:[
                    //                 PostCssPresetEnv()
                    //             ]
                    //         }
                    //     }
                    // }
                ]
            },
            {
                test:/\.less$/,
                use:[
                    "style-loader",
                    {
                        loader:"css-loader",
                        options:{
                            esModule:false,
                            importLoaders:1
                        }
                    },
                    'postcss-loader',
                    // {
                    //     loader:"postcss-loader",
                    //     options:{
                    //         postcssOptions:{
                    //             plugins:[
                    //                 require('postcss-preset-env')
                    //             ]
                    //         }
                    //     }
                    // },
                    "less-loader"
                ]
            },
            {
                test:/\.js$/,
                exclude:/node_modules/,
                use:[
                    'babel-loader'
                    // {
                    //     loader:'babel-loader',
                    //     options:{
                    //         presets:[
                    //             [
                    //                 "@babel/preset-env",
                    //                 {
                    //                     "useBuiltIns":"usage",
                    //                     "corejs":3
                    //                 }
                    //             ]
                    //         ]
                    //     }
                    // }
                ]
            },
            {
                test:/\.(jpe?g|png|gif|svg)$/,
                type:'asset',
                generator:{
                    filename:"./img/[name].[hash:6][ext]"
                },
                parser:{
                    dataUrlCondition:{
                        maxSize:1000*1024
                    }
                }
            },
            {
                test:/\.(ttf|woff2?)$/,
                type:'asset/resource',
                generator:{
                    filename:"./font/[name].[hash:6][ext]"
                },
            },
            {
                test:/\.jsx$/,
                exclude:/node_modules/,
                use:[
                    {
                        loader:'babel-loader',
                    }
                ]
            }
        ]
    },
    plugins:[
        new HtmlWebpackPlugin({
            title:'邱文军-webpack-学习',
            template:'./public/index.html'
        }),
        new DefinePlugin({
            "BASE_URL" : JSON.stringify('./')
        }),
    ]
}

module.exports = function(env){
    console.log(arguments);
    process.env.NODE_ENV = env.production ? 'production' : 'development'
    return env.production ? merge(BaseOpt,Prod) : merge(BaseOpt,Dev)
}
