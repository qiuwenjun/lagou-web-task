const { AsyncParallelHook } = require('tapable') // AsyncParallelBailHook, AsyncSeriesWaterfallHook

const asyncP = new AsyncParallelHook(['name'])

// asyncP.tap('fn1', function (name) {
//   console.log('fn1', name)
// })

// asyncP.tap('fn2', function (name) {
//   console.log('fn2', name)
// })

// asyncP.tap('fn3', function (name) {
//   console.log('fn3', name)
// })

// asyncP.callAsync('邱文军', function () {
//   console.log('最后执行回调操作')
// })

asyncP.tapAsync('fn1', function (name, next) {
  console.log('fn1', name)
  next()
})

asyncP.tapAsync('fn2', function (name, next) {
  console.log('fn2', name)
  next()
})

asyncP.tapAsync('fn3', function (name, next) {
  console.log('fn3', name)
  next()
})

asyncP.callAsync('邱文军', function () {
  console.log(arguments)
})

// asyncP.tapPromise('fn1', async (name) => {
//   console.log('fn1', name)
// })

// asyncP.tapPromise('fn2', async (name) => {
//   console.log('fn2', name)
//   return true
// })

// asyncP.tapPromise('fn3', async (name) => {
//   console.log('fn3', name)
// })

// asyncP.callAsync('qiuwenjun', function () {
//   console.log(arguments)
// })

// asyncP.promise('邱文军').then(e => {
//   console.log(e)
// })
