module.exports = grunt => {
    
    grunt.initConfig({
        clean:{
            build : ['temp'],
            release : ['release']
        }
    })

    grunt.loadNpmTasks('grunt-contrib-clean')
    grunt.registerTask('default', ['clean']);

}