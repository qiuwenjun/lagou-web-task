class Observer{
    constructor(public data:any){
        this.walk(data)
    }
    walk(data:any){
        if(!data || typeof data !== 'object') return
        for(let attr in data){
            this.defineReactive(data,attr,data[attr])
        }
    }
    defineReactive(obj:object,attr:string,val:any){
        this.walk(val); //如果val是对象，把val内部的属性转换成响应式数据
        const dep = new Dep(); //收集依赖发送通知
        Object.defineProperty(obj,attr,{
            enumerable:true,
            configurable:true,
            get:()=>{
                Dep.target && dep.addSub(Dep.target)
                return val    //不能是obj[attr],这里会发生死递归
            },
            set:(newVal)=>{
                if(newVal === val) return
                val = newVal
                dep.notify()
                this.walk(newVal)
            }
        })
    }
}