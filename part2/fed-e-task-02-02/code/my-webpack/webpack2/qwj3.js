const Path = require('path');
const PostCssPreset = require('postcss-preset-env');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { DefinePlugin } = require('webpack')
const CopyWebpackPlugin = require('copy-webpack-plugin')
// console.log(process.env);
module.exports = {
    watch:false,
    devtool:false,
    mode:"development",
    entry:"./src/index.js",
    output:{
        filename:"build.js",
        path:Path.resolve(__dirname,'dist'),
        // assetModuleFilename:'./img/[name].[hash:6][ext]'     
    },
    module:{
        rules:[
            {            
                test:/\.css$/,
                loader:'style-loader'
            },
            {            
                test:/\.css$/,
                loader:'css-loader',
                options:{
                    esModule:false,
                    importLoaders:1         //这个配置参数代表，如果css-loader处理的文件中出现了通过@import加载的文件，那这时候这个文件还需要上一层的postcss-loader处理，那这时候就需要配置此参数，参数类型为数字，当处理文件需要之前的几个loader在重新处理，那这里的数字就是几,这里只需要被postCss-loader处理，所以写1
                }
            },
            {
                test:/\.css$/,
                rules:[
                    {
                        loader:'postcss-loader',
                    }
                ]
            },
            {            
                test:/\.less$/,
                use:[
                    "style-loader",
                    {
                        loader:"css-loader",
                        options:{
                            importLoaders:1,
                            esModule:false
                        }
                    },
                    {
                        loader:'postcss-loader',
                    },
                    "less-loader"
                ]
            },
            // {
            //     test:/\.(png|jpe?g|gif|svg)/,
            //     use:[
            //         {
            //             loader:'file-loader',
            //             options:{
            //                 esModule:false,
            //                 name:'./img/[name].[hash:6].[ext]',     //输出目录img，或者下面这种方式
            //                 // outputPath:'img'
            //             }
            //         }
            //     ]
            // },
            // {
            //     test:/\.(png|jpe?g|gif|svg)/,
            //     use:[
            //         {
            //             loader:'url-loader',
            //             options:{
            //                 limit:1000*1024,  //大小限制，当图片的大小小于这个时，转换为base64，大于时，自动调用file-loader
            //                 name:'./img/[name].[hash:6].[ext]',     //输出目录img，或者下面这种方式
            //                 // outputPath:'img'
            //             }
            //         }
            //     ]
            // },
            // {
            //     test:/\.(png|svg|jpe?g|png)$/,
            //     type:'asset/resource'           //相当于使用file-loader,处理文件后
            // },
            // {
            //     test:/\.(png|svg|jpe?g|png)$/,
            //     type:'asset/resource',              
            //     generator:{
            //         filename:"img/[name].[hash:6][ext]"     //当我们对不同的文件类型做处理后要输出到不同的目录下，那这时候就不适用全局的配置，而要使用这种单独的配置方式
            //     }
            // },
            // {
            //     test:/\.(png|svg|jpe?g|png)$/,
            //     type:'asset/inline'           //相当于使用url-loader,处理文件后
            // },
            {
                test:/\.(png|svg|jpe?g|png)$/,
                type:'asset',           //统一使用，当处理的图片小于我们配置的大小时候，就输出base64，否则输出文件
                generator:{
                    filename:"img/[name].[hash:6][ext]" //输出目录
                },
                parser:{
                    dataUrlCondition:{
                        maxSize:1000*1024       //图片大小
                    }
                }
            },
            {
                test:/\.(ttf|woff2?)$/,
                type:'asset/resource',           //相当于使用file-loader,直接当静态资源拷贝
                generator:{
                    filename:"font/[name].[hash:6][ext]" //输出目录
                },
            },
            {
                test:/\.js$/,
                exclude:/node_modules/,
                use:[
                    {
                        loader:'babel-loader',
                    }
                    // {
                    //     loader:'babel-loader',
                    //     options:{
                    //         // plugins:[
                    //         //     '@babel/plugin-transform-arrow-functions',
                    //         //     '@babel/plugin-transform-block-scoping'
                    //         // ],		//单独一个一个的插件
                    //         presets:[
                    //             [
                    //                 "@babel/preset-env",
                    //                 {
                    //                     "targets":"chrome 102"
                    //                 }
                    //             ]
                    //         ]
                    //     }
                    // }
                ]
            }
        ]
    },
    plugins:[
        new CleanWebpackPlugin(),
        // new HtmlWebpackPlugin(),     //走插件的默认配置
        // new HtmlWebpackPlugin({      //自定义title
        //     title:'my webpack app'
        // }),
        new HtmlWebpackPlugin({         //直接使用自定义模板
            title:"my webpack app",
            template:"./public/index.html"
        }),
        new DefinePlugin({          //允许在 编译时 将你代码中的变量替换为其他值或表达式
            BASE_URL:"'./'",         //定义时的变量值都必须是JSON
        }),
        new CopyWebpackPlugin({
            patterns:[
                {
                    from:'public',  //要复制哪个文件夹下的内容
                    // to:'dist',    //输出目录，但是一般不用配置，默认直接使用output.path
                    globOptions:{
                        ignore:['**/index.html']  //在指定的复制文件夹下，要忽略的文件，必须以**开头
                    }
                }
            ]
        })
    ]
}

/*
    插件本质上就是一个类
*/
/**
 * [ext]:文件扩展名
 * [name]:文件名称
 * [hash]|[chunkhash]|[contenthash]:都是用来给文件添加唯一值的，但是这三者有所不同
    hash
    hash是跟整个项目的构建相关，只要项目里有文件更改，整个项目构建的hash值都会更改，并且全部文件都共用相同的hash值

    chunkhash
    chunkhash根据不同的入口文件(Entry)进行依赖文件解析、构建对应的chunk，生成对应的哈希值。我们在生产环境里把一些公共库和程序入口文件区分开，单独打包构建，接着我们采用chunkhash的方式生成哈希值，那么只要我们不改动公共库的代码，就可以保证其哈希值不会受影响。

    contenthash
    假设index.js引用了index.css,若使用chunkhash的话，只要js改变，那么css也会被重新构建。如何防止css被重新构建呢？
    可以使用extract-text-webpack-plugin提取单独的css文件并使用contenthash
    contenthash只要文件内容不变，那么不会重复构建。

    [hash:<length>]|[chunkhash:<length>]|[contenthash:<length>]   这里的length代表截取hash的长度
 *  [path]:文件路径
 * */ 