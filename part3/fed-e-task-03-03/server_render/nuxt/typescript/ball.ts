class Ball{
    x:number
    y:number
    w:number
    h:number
    scaleX:number
    scaleY:number
    alpha:number
    r:number
    fillStyle:string
    strokeStyle:string
    constructor(props:Partial<Ball>){
        this.x = 0;
        this.y = 0;
        this.w = 0;
        this.h = 0;
        this.scaleX = 1;
        this.scaleY = 1;
        this.alpha = 1
        this.r = 0;
        this.fillStyle='rgb(68 186 255)'
        this.strokeStyle='rgb(163,163,163)'
        Object.assign(this,props)
    }
    render(ctx:CanvasRenderingContext2D){
        ctx.save()
        ctx.beginPath()
        ctx.clearRect(0,0,800,800)
        ctx.resetTransform()
        ctx.translate(this.x,this.y)
        ctx.scale(this.scaleX,this.scaleY)
        ctx.arc(0,0,this.r,0,2*Math.PI)
        ctx.fillStyle = this.fillStyle
        ctx.strokeStyle = this.strokeStyle
        ctx.globalAlpha = this.alpha
        ctx.fill()
        ctx.stroke()
        ctx.restore()
    }
}

// type a<fan>=fan extends 'a'|'b' ? never : fan

// type b=a<'a'|'b'|'c'>

// interface A{
//     a:number
// }
// interface B extends A{
//     b:number
// }
// let AB:B={
//     a:1,
//     b:2
// }

// interface ABS{
//     constructor(a:number,b:number):ABS
//     new 
// }

// type A6 = { age:number } | { age:string; name:string}

// const json = {
//     a: 'a1',
//     b: 'b1',
//     c: 'c1',
//     d: {
//         da: 'da1',
//         db: 'da2'
//     }
// } as const

// type keys = keyof typeof json; //typeof获取类型，keyof获取key值组成的联合类型 a|b|c
// type values = typeof json[keys] 

// const json = {
//     a:1,
//     b:2,
//     c:3,
//     d: {
//         dd:4,
//         dd2:5,
//     }
// } as const 
// type JJ = typeof json;
// type keys = keyof JJ;
// type DeepObjectByValue<T = JJ> =  {   //ts也是支持递归语法的
//     [key in keyof T]:T[key] extends object ? DeepObjectByValue<T[key]>[keyof DeepObjectByValue<T[key]>] : T [key]
// }
// type a = DeepObjectByValue<JJ>[keys]

// function getName(name:string):string
// function getName(name:number):number
// function getName(name:any):any{
//   return name
// }
// getName(1)
// getName('1')
// type getName = typeof getName
// //这时候typeof getName相当于是下面
// type getName2 = {
//   (name:string):string;
//   (name:number):number;
// }

// let obj:{a:unknown} = { a:2 }
// if(typeof obj.a === 'string'){
//     console.log(obj.a.toLocaleLowerCase())
// }

type a = Array<1>
function fn<fn,fn2>(fn:fn){
    console.log(fn)
}
fn(true)

// interface obj2<num>{
//     num:num
// }
// let obj2:obj2<1>={
//     num:1
// }

interface DateConstructor {
    new (value: number | string | Date): Date;
}


class A<type,b>{
    constructor(a:type,b:string){
    
    }
}
new A(1,'2')

type EventConfig<Events extends { kind: string }> = {
    [E in Events as E["kind"]]: (event: E) => void;
}

type SquareEvent = { kind: "square", x: number, y: number };
type CircleEvent = { kind: "circle", radius: number };

type Config = EventConfig<SquareEvent>
let Config:Config={
    square:(event:SquareEvent)=>{

    }
}

function move<type>(str:type):type{
    return str
}
interface As<type>{
    (str:type):type
}
let As:As<string>=move
As('1')



function Move<type>(args:type):type{
    return args
}

let MoveInstance1:<type>(args:type)=>type = Move
let MoveInstance2:<type2>(args:type2)=>type2 = Move
let MoveInstance3:{<type2>(args:type2):type2} = Move
interface iMoveInstance1{
    <type2>(args:type2):type2
}
let MoveInstance4:iMoveInstance1 = Move
interface iMoveInstance2<type3>{
    (args:type3):type3
}
let MoveInstance5:(arg:string)=>string = function(arg){
    return arg
}

const obj={
    a:1
} as const


function getAjax<type extends { length:number }>(args:type):type{
    return args
}
getAjax({a:1,b:2,c:3,length:3})

function getAjax2<type,key extends keyof type>(args:type,key:key){}
getAjax2({a:1,b:2,c:3,length:3},'a')


class Zoom{
    static push(){}
    static pop():any{}
    constructor(public length:number){

    }
    push(a:string){}
}
// class Cjl extends Zoom{
//     instance:Zoom = new Zoom
// }
// class dx extends Zoom{
    
// }

//接口类型描述class类
//原型方法
interface Zoom2{
    pop():any
}
//类构造函数
interface Zoom2Constructor{
    new(length:number):Zoom2
}


let Zoom2:Zoom2 = Zoom



function Type<type>(fn:new()=>type):type{
    return new fn
}

Type(Blob)
Type(File)


type S1 = {
    [prop:string]:string
}
type strList1 = keyof S1;   //string | number
let val1:strList1 = '1'
let val2:strList1 = 2
// let val3:strList1 = true

type S2 = {
    [prop:number]:number
}
type strList2 = keyof S2;   //number
// let val4:strList2 = '1'
let val5:strList2 = 2

//原因就是属性中，number是string的子类型

function Fn2(){
    return { name:'邱文军', age:25 }
}
type Fn2Result = ReturnType<typeof Fn2>

let box:typeof Fn2=()=>({
    name:'123',
    age:123
})