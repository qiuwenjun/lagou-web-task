const { SyncLoopHook } = require('tapable')

const sync = new SyncLoopHook(['name', 'age'])

sync.tap('fn1', function (name, age) {
  console.log('fn1', name, age)
  return false
})

sync.tap('fn2', function (name, age) {
  console.log('fn2', name, age)
  return false
})

sync.tap('fn3', function (name, age) {
  console.log('fn3', name, age)
})

sync.call('邱文军', 25)
