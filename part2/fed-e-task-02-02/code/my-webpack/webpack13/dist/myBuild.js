(function (modules) {

    //缓存模块加载的资源
    const __webpack_module_cache__ = {};

    //模块加载函数
    function __webpack_require__(moduleId) {

        //当加载一个模块资源时，如果这时候已经加载过了，那么就直接返回模块数据
        if (__webpack_module_cache__[moduleId]) {
            return __webpack_module_cache__[moduleId]
        }

        //当前要加载的模块对象
        const module = {
            i: moduleId,
            l: false,
            exports: {}
        }

        //加载模块函数
        modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

        module.l = true;

        return module.exports

    }

    //存放要加载的所有模块配置对象
    __webpack_require__.m = modules

    //存放模块加载后的缓存对象
    __webpack_require__.c = __webpack_module_cache__

    //该方法用于判断对象上是否存在指定的属性
    __webpack_require__.o = function (exports, attr) {
        return Object.hasOwn(exports, attr)
    }

    //该方法用于往对象上添加指定的属性，同时给该属性添加一个getter
    __webpack_require__.d = (exports, definition) => {
        for(var key in definition) {
            if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
                Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
            }
        }
    };

    //对不同类型的模块加载器进行抹平处理
    __webpack_require__.r = function (exports) {
        if (typeof Symbol !== 'undefined' && Symbol.toStringTag) {
            Object.defineProperty(exports, Symbol.toStringTag, { value: "Module" })
        }
        Object.defineProperty(exports, '__esModule', { value: true })
    }

    __webpack_require__.n = function (module) {
        const getter = module && module.__esModule ? () => module.default : () => module;
        Object.defineProperty(getter, 'a', { get: getter });
        return getter
    }

    //用于保存当前资源访问路径
    __webpack_require__.s = '';

    return __webpack_require__(__webpack_require__.s = './src/index.js')

}({
    './src/index.js': (function (__module__, __exports__, __webpack_require__) {
        __webpack_require__.r(__exports__);
        var _login__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./login */ "./src/login.js");
        
        
        console.log('入口文件!');
        
        console.log(_login__WEBPACK_IMPORTED_MODULE_0__["default"],'<!----',_login__WEBPACK_IMPORTED_MODULE_0__.age);
    }),
    './src/login.js': ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

        __webpack_require__.r(__webpack_exports__);
        __webpack_require__.d(__webpack_exports__, {
            "age": () => (age),
            "default": () => (__WEBPACK_DEFAULT_EXPORT__)
        });
            const __WEBPACK_DEFAULT_EXPORT__ = ('登陆模块');
            const age = 40;
        })
}))