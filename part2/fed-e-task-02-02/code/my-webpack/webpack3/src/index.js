import { sum, squre } from './utils/common.js'
const utils = require('./utils/index')
import './utils/lg';
import './utils/login';
import './utils/file';
import './utils/font';
import './utils/core';

console.log(utils.hl(5,4));

console.log(sum(1,2));

console.log(squre(5));

/*
    处理图片文件 file-loader
    img src文件
       1 使用require导入图片，返回一个esModule对象，这时候图片地址需要手动.default获取
       2 也可以在webpack配置文件使用file-loader时候加上配置参数 esModule:false
       3 或者直接使用import *** from 图片资源,此时可以直接使用 ***
    background url
       css-loader 处理到url的时候也是通过require来进行加载，但是返回的是一个对象，所以我们需要在css-loader的配置项中加上esModule：false
*/

console.log(789);

if(module.hot){
   //['./utils/common.js'] 这个里面就是要监听的热更新文件
   module.hot.accept(['./utils/common.js'],()=>{

      console.log('热更新');

   })
}