type Records<T extends string | number,U> = {
    [k in T] : U
}

type o<T> = keyof T;

let obj:o<{a:1,b:2,c:3}>=''