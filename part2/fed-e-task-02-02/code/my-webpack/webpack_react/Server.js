const express = require('express');
const webpack = require('webpack');
const webpackdm = require('webpack-dev-middleware')

const app = express()

const compile = webpack(require('./webpack.qwj.js'));

app.use(webpackdm(compile))

app.listen(3000)