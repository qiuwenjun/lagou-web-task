import fp = require('lodash/fp')

class Io{
    static of(value){
        return new Io(function(){
            return value
        })
    }
    constructor(private value){}
    map(fn){
        return new Io(fp.flowRight(fn,this.value))
    }
    static callFn(This){
        return This.value()
    }
}

let r = Io.of(process).map(e=>e.execPath)
console.log(Io.callFn(r));

