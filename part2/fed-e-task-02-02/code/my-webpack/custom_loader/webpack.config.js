const Path = require('path');
module.exports = {
    entry:'./src/index.js',
    output:{
        filename:"index.js",
        path:Path.resolve(__dirname,'dist'),
    },
    module:{
        rules:[
            {
                test:/\.md$/,
                use:[
                    './markdown.js'
                ]
            }
        ]
    }
}