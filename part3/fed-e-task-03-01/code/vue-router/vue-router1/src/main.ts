import App from '@/views/App.vue';
import '@/assets/css/reset.less';

import '@/assets/js/common.ts';
import { createApp } from "vue";

const app = createApp(App)

app.mount('#app')

Promise.allSettled([1,2,3]).then(result=>{
    console.log(result)
})