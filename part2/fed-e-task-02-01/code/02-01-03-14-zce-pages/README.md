# qwj-gulp-cli

[![NPM Downloads][downloads-image]][downloads-url]
[![NPM Version][version-image]][version-url]
[![License][license-image]][license-url]
[![Dependency Status][dependency-image]][dependency-url]
[![devDependency Status][devdependency-image]][devdependency-url]
[![Code Style][style-image]][style-url]

> static web app workflow

## Installation

```shell
$ yarn add qwj-gulp-cli

# or npm
$ npm install qwj-gulp-cli
```

## Usage

<!-- TODO: Introduction of API use -->

```javascript
const qwj-gulp-cliPages = require('qwj-gulp-cli')
const result = qwj-gulp-cliPages('qwj-gulp-cli')
// result => 'qwj-gulp-cli@qwj-gulp-cli.me'
```

## API

<!-- TODO: Introduction of API -->

### qwj-gulp-cliPages(name[, options])

#### name

- Type: `string`
- Details: name string

#### options

##### host

- Type: `string`
- Details: host string
- Default: `'qwj-gulp-cli.me'`

## Contributing

1. **Fork** it on GitHub!
2. **Clone** the fork to your own machine.
3. **Checkout** your feature branch: `git checkout -b qwj-awesome-feature`
4. **Commit** your changes to your own branch: `git commit -am 'Add some feature'`
5. **Push** your work back up to your fork: `git push -u origin qwj-awesome-feature`
6. Submit a **Pull Request** so that we can review your changes.

> **NOTE**: Be sure to merge the latest from "upstream" before making a pull request!

## License

[MIT](LICENSE) &copy; qwj-gulp-cli <w@qwj-gulp-cli.me> (https://qwj-gulp-cli.me)



[downloads-image]: https://img.shields.io/npm/dm/qwj-gulp-cli.svg
[downloads-url]: https://npmjs.org/package/qwj-gulp-cli
[version-image]: https://img.shields.io/npm/v/qwj-gulp-cli.svg
[version-url]: https://npmjs.org/package/qwj-gulp-cli
[license-image]: https://img.shields.io/github/license/qwj-gulp-cli/qwj-gulp-cli.svg
[license-url]: https://github.com/qwj-gulp-cli/qwj-gulp-cli/blob/master/LICENSE
[dependency-image]: https://img.shields.io/david/qwj-gulp-cli/qwj-gulp-cli.svg
[dependency-url]: https://david-dm.org/qwj-gulp-cli/qwj-gulp-cli
[devdependency-image]: https://img.shields.io/david/dev/qwj-gulp-cli/qwj-gulp-cli.svg
[devdependency-url]: https://david-dm.org/qwj-gulp-cli/qwj-gulp-cli?type=dev
[style-image]: https://img.shields.io/badge/code_style-standard-brightgreen.svg
[style-url]: https://standardjs.com
