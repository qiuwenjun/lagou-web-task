const fs = require('fs');
import folktale = require("folktale/concurrency/task")
import fp = require('lodash/fp')

function readFile(filename:string){
    return folktale.task(resolver=>{
        fs.readFile(filename,'utf-8',(err,data)=>{
            if(err) resolver.reject(err)
            resolver.resolve(data)
        })
    })
}

const task = readFile('package.json')
task.map(fp.flowRight(fp.find(item=>item.includes("version")),fp.split(/\n/))).run().listen({
    onRejected: err => {
        console.log(err);
    },
    onResolved: data => {
        console.log(data)
    }
})

declare global{
    interface Window{
        a:string
    }
}
