const Generator = require('yeoman-generator')

module.exports = class extends Generator {
    prompting(){                //在命令行中提示用户的信息
        return this.prompt([
            {
                type:'input',       //代表是用户输入的
                name:'name',        //字段
                message:"你的项目名称", //提示用户的信息
                default:this.appname    //默认答案
            }
        ]).then(answers=>{
            this.answers = answers  //记录用户输入的字段
        })
    }
    writing(){                      //输出
        const templates = [         //要输出的模板数据
            '.browserslistrc',
            '.editorconfig',
            '.env.development',
            '.env.production',
            '.eslintrc.js',
            '.gitignore',
            'babel.config.js',
            'package.json',
            'postcss.config.js',
            'README.md',
            'public/favicon.ico',
            'public/index.html',
            'src/App.vue',
            'src/main.js',
            'src/router.js',
            'src/assets/logo.png',
            'src/components/HelloWorld.vue',
            'src/store/actions.js',
            'src/store/getters.js',
            'src/store/index.js',
            'src/store/mutations.js',
            'src/store/state.js',
            'src/utils/request.js',
            'src/views/About.vue',
            'src/views/Home.vue'
        ]
        templates.forEach(item=>{
            const temp = this.templatePath(item)        //使用模板
            const output = this.destinationPath(item)   //输出模板导出的内容
            console.log(this.answers);                  
            this.fs.copyTpl(temp,output,this.answers)   //将输出的模板已经内容导入到对应文件夹中
        })
    }
}