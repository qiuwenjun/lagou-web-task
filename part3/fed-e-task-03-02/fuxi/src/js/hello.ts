// type obj3={
//     name:string,
//     fn(this:obj3,a:string,b:string):void
// }
interface obj3{
    name:string,
    fn(this:obj3,a:string,b:string):void
}
let obj3:obj3={
    name:'邱文军',
    fn(a,b){
        console.log(this)
        return ()=>{
            console.log(this);
        }
    }
}
// obj3.fn.call(window,'1','2')
// declare global{

// }
interface Window{
    a:string
}

class Container implements obj3{
    private val:number
    constructor(val:number){
        this.val=val
        this.name='Container'
    }
    name
    fn(this: obj3, a: string, b: string): void {}
    map():this{
        return this
    }
    log(this:Container):Container{
        return this
    }
}
let C1:typeof Container = Container
let C2:Container = new Container(1)

interface Container2 extends Container{}

let Container2:Container2={
    val:1,
    name:'邱文军',
    fn(){},
    map(){ return this},
    log(){ return this}
}

// Person 类（构造函数）
class Person{}
// 通过 Person 实例化出来的对象 p1
const p1 = new Person //p1的类型就是Person
// 构造函数的类型是
const PersonCopy:typeof Person = Person

interface IO<state={ name:string, age:number }>{
    state:state
}
type IO1=IO
type IO2=IO<{gener:string;sex:boolean}>
let IO1:IO1={state:{name:'123',age:123}}
let IO2:IO2={state:{gener:'前端开发',sex:true}}
// let IO3:IO={state:{gener:'前端开发',sex:true}}


type EventConfig<Events extends { kind: string }> = {
        [E in Events as E['kind']]: (event: E) => void;
    }
     
    type SquareEvent = { kind: "square", x: number, y: number };
    type CircleEvent = { kind: "circle", radius: number };
     
    type Config = EventConfig<SquareEvent | CircleEvent>

    let Config:Config={
        square:event=>{},
        circle:event=>{}
    }
    let a = 1
    let num2=2 


//泛型约束
type len = { length:number }

interface iLen<l extends len>{
    l:l
}
let len:iLen<{length:1}>={
    l:{length:1}
}

function getLen<l extends len>(l:l){
    console.log(l);
    
}
getLen<{length:1}>({length:1})



function getName(name:string):string
function getName(name:number):number
function getName(name:any):any{
    return name
}
getName(1)
getName('1')
type getName = typeof getName
//这时候typeof getName相当于是下面
type getName2 = {
    (name:string):string;
    (name:number):number;
}

let obj={
    a:1,
    b:2,
    c:3
}
function getValue<T,keys extends keyof T>(obj:T,keys:keys[]):T[keys][]{
    return keys.map(item=>obj[item])
}
console.log(getValue(obj,['a','b']))
console.log(getValue(obj,['c','d']))    

{
    interface Person {
        name: string;
        age: number; 
        [key:string]:string|number
    }
    type personKeys = keyof Person;
    //type personKeys='name'|'age'
    type newPerson = {
        [k in personKeys]: Person[k];
    }
    /*
        相当于变成
        type newPerson = {
            [x: string]: string | number;
            [x: number]: string | number;
        }
    */
    
    let obj:newPerson={
        name:'邱文军',
        age:25,
        gender:'前端开发'
    }
}

{
    const json = {
        a: 'a1';
        b: 'b1';
        c: 'c1';
        d: {
            da: 'da1';
            db: 'da2'
        }
    } as const
    
    type keys = keyof typeof json; //typeof获取类型，keyof获取key值组成的联合类型 a|b|c
    type values = typeof json[keys] //这里这样写，默认就会循环，typeof获取类型，然后keys就是key的集合，获取value组成的联合类型 a1|b1|c1
}
// type awd = Pick<{a:number;b:string},'a'> 
{
    const json = {
        a:1,
        b:2,
        c:3,
        d: {
            dd:4
        }
    } as const 
    type JJ = typeof json;
    type keys = keyof JJ;
    type DeepObjectByValue<T = JJ> =  {
        [key in keyof T]:T[key] extends object ? DeepObjectByValue<T[key]>[keyof DeepObjectByValue<T[key]>] : T [key]
    }
    type a = DeepObjectByValue<JJ>[keys]
}
function data(d){
    let obj = 
    while(true){

    }
}
// type T = keyof typeof json
// function test<T>(val:T) {
//     console.log(val)
// }

{
    const json = {
        a: 'a1',
        b: 'b1',
        c: 'c1',
        d: {
            da: 'da1',
            db: 'da2'
        }
    } as const
    type values<T> = {
        [k in keyof T]:T[k] extends object ? values<T[k]>[keyof T[k]] : T[k]
    }
    type value<T> = values<T>[keyof T]

    let value:value<typeof json>=''
    type InferTypeFromObjectValue<T extends object> = {
        [K in keyof T]: T[K] extends object ? InferTypeFromObjectValue<T[K]> : T[K]
       }[keyof T]
}

{
    const json = {
        a: 'a1',
        b: 'b1',
        c: 'c1',
        d: {
          da: 'da1',
          db: 'db1'
        },
        e: {
          ea: 'ea1',
          eb: 'eb1',
          ec: {
            eca: 'eca1',
            ecb: 'ecb1'
          }
        }
      } as const

      type a<T,K>={
        [k in keyof T]:T[k] extends 
      }
}