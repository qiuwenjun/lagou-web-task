import RollupPluginJson from 'rollup-plugin-json'; //能够在代码中加载json文件
import RollupPluginNodeResolve from 'rollup-plugin-node-resolve'; //能够在代码中加载npm模块
import RollupPluginCommon from 'rollup-plugin-commonjs'; //能够在代码中加载commonJs模块
import { babel } from '@rollup/plugin-babel'; //处理es6语法
import { uglify } from "rollup-plugin-uglify";  

export default {
    // input:"./src/index.js",  //打包的入口文件路径
    input:[
        './src/index.js',
        './src/main.js',
    ],
    output:{   //输出
        //如果想要支持代码拆分的方式，需要使用以下配置
        dir:'dist',  //输出到的目录
        format:'esm' //此处模块化不能使用umd和iife
    },
    plugins:[
        RollupPluginJson(),
        RollupPluginNodeResolve(),
        RollupPluginCommon(),
        babel({
            exclude:'node_modules/**',
            babelHelpers :"runtime",
            presets: [
                [
                    '@babel/preset-env',
                    {
                        useBuiltIns:"usage",
                        corejs:3
                    }
                ]
            ],
            plugins:[
                '@babel/plugin-transform-runtime'
            ]
        }),
        uglify()
    ]
}