const HtmlWebpackPlugin = require('html-webpack-plugin')
module.exports = {
  mode: 'none',
  stats: 'none',
  devtool: 'source-map',
  entry:{
    "Promise":"./Promise.ts"
  },
  module:{
    rules:[
      {
        test:/\.ts$/,
        use:'ts-loader',
      }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin()
  ]
}
