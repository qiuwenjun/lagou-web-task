var Path = require('path');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var DefinePlugin = require('webpack').DefinePlugin;
var CopyWebpackPlugin = require('copy-webpack-plugin');
module.exports = {
    mode: "development",
    entry: './src/index.ts',
    output: {
        filename: '[name].[hash:6].js',
        path: Path.resolve(__dirname, 'dist')
    },
    resolve: {
        extensions: ['.js', '.ts', 'jsx', '.tsx', '.vue']
    },
    module: {
        rules: [
            {
                test: /\.less$/,
                rules: [
                    {
                        loader: "style-loader"
                    },
                    {
                        loader: "css-loader",
                        options: {
                            esModule: false,
                            importLoaders: 2
                        }
                    },
                    {
                        loader: "postcss-loader"
                    },
                    {
                        loader: "less-loader"
                    }
                ]
            },
            {
                test: /\.ts$/,
                rules: [
                    {
                        loader: 'babel-loader'
                    },
                    {
                        loader: 'ts-loader'
                    }
                ]
            },
            {
                test: /\.vue$/,
                exclude: /node_modules/,
                use: [
                    {
                        loader: 'vue-loader-v16'
                    }
                ]
            },
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            title: "VUE3",
            template: './public/index.html'
        }),
        new DefinePlugin({
            "BASE_URL": '"./"'
        }),
        new CopyWebpackPlugin({
            patterns: [
                {
                    from: "public",
                    globOptions: {
                        ignore: ['**/index.html']
                    }
                }
            ]
        })
    ]
};
