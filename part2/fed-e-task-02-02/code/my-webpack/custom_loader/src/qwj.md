# PageTrack  页面埋点插件

## 插件需要的参数
> options
   
   | key    | value   |    说明  |
   | ---    | -----    |  ------- |
   | distinctId |  null  |  用户ID  |
   | project_name |    | 项目名称(必传) |
   | server_url |     | 数据上报地址(必传) |
   | send_type |  image  | 数据上报方式 |
   | send_interval |  0  | 间隔多少毫秒发一次数据  |
   | send_number |  1  | 一次发送多少条数据  |
   | callback_timeout |  0  | 回调函数执行时间，当用户上传超时，则直接执行当前回调 |
   | datasend_timeout |  3  | 数据发送超时时间 |
   | is_track_single_page |  false  | 是否开启单页面模式 |
   | show_log |  true  | 是否在控制台打印出要发送的数据 |
   | callBackName |  upload_data  | 如果是jsonp，那这时候回调函数的名称 |
   | pageMap |  true  | 页面初始化事件 页面初始化的时候需不需要上报一下 |
   | platform |  0  | 默认0 可以自定义 |

## 实例方法
   | key    | type(参数类型)   |    说明  |
   | ---    | -----    |  ------- |
   | $lib |    |  SDK 类型 |
   | $lib_version |    | SDK 版本 |
   | registerGetProp |     | 获取自定义属性 |
   | registerSetProp |  Object  | 设置自定义属性 |
   | registerDeleteProp |  Object  | 删除自义定属性  |
   | profileSet |  Object  | 用来自定义设置用户属性,直接设置，存在就替换  |
   | profileSetOnce |  Object  | 如果已经存在就不更新，如果没有就设置 |
   | profileIncrement |  Object  |  如果对应是值是数字，那就是递增 |
   | profileDelete |  Object  | 删除 |
   | profileAppend |  Object  | 追加，对于值是数组的进行追加 |
   | profileUnset |  Object  | 删除属性 |
   | pageView |    | 页面一打开就要执行一次,或者当路由变化时 |
   | getProperties |    | 获取上报类型对应的预设属性 |
   | reportMethod | Object(要上传的数据) Function(回调函数)   | 上报方法，可以用来和getProperties方法自定义上报方案 |
   | track |  String(事件名称) Object(要上传的数据)  | 放入队列的数据上报方案,用来对type:track相关数据进行操作 |
   | updateUserId |  String,Number  | 更新用户登陆后ID |
   | updateDistinctId |  String,Number  | 更新用户的插件生成ID |
   | setProject |  String(属性名) String(属性值)  | 设置项目的对应存储数据,当前插件所对应的本地存储都必须通过这个方法来设置 |
   | getProject |  String(属性名)|| 不传就是整个数据  | 获取项目的对应存储数据,当前插件所对应的本地存储都必须通过这个方法来获取 |
   | startQueue |  | 开始执行队列，返回true代表正在执行,返回false代表执行失败 |
   | stopQueue |  | 终止队列执行 |
   | addQueue | Object(数据) | 往队列中添加要上传的数据 |
   | removeQueue |  | 清除整个队列 |
   | hasQueue |  | 判断队列是否正在执行 |

## 构造函数上的方法
   | key    | type(参数类型)   |    说明  |
   | ---    | -----    |  ------- |
   | tabBase64    | String |  对数据进行base64编码或解码 |
   | copyObject    | Object(要拷贝的数据) Boolean(是否深拷贝) Boolean(是否按JSON数据方式进行拷贝) |  对数据进行拷贝 |
   | formatParams    | Object |  对要上传的数据进行编码 |
   | judgmentType    | String |  判断数据类型 |
   | storage    | String |  操作本地存储 |

      

      

      
