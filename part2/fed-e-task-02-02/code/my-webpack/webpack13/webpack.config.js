const Path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    mode:"development",
    devtool:false,
    entry:'./src/index.js',
    output:{
        filename:'[name].js',
        path:Path.resolve(__dirname,'dist')
    },
    plugins:[
        new HtmlWebpackPlugin({
            template:'./src/index.html'
        })
    ],
    resolve:{
        alias:{
            '@':Path.resolve(__dirname,'src')
        },
        extensions:['.js']
    }
}