"use strict";
// import SyncHook from "./SyncHook";
exports.__esModule = true;
// const sync = new SyncHook(['name','age']);
// sync.tap('fn1',function(name:string,age:number){
//     console.log('fn1',name,age);
// })
// sync.tap('fn2',function(name:string,age:number){
//     console.log('fn2',name,age);
// })
// sync.tap('fn3',function(name:string,age:number){
//     console.log('fn3',name,age);
// })
// sync.call('邱文军',25)
var AsyncParalleHook_1 = require("./AsyncParalleHook");
var _async = new AsyncParalleHook_1["default"](['name', 'age']);
_async.tapAsync('fn1', function (name, age, next) {
    console.log('fn1', name, age);
    next();
});
_async.tapAsync('fn2', function (name, age, next) {
    console.log('fn2', name, age);
});
_async.tapAsync('fn3', function (name, age, next) {
    console.log('fn3', name, age);
    next();
});
_async.callAsync('邱文军', 25, function () {
    console.log('结束了~~~');
});
