// let obj:{[prop:string]:string}={
//     a:'1',
//     b:'2',
//     c:'3'
// }

// const obj_copy = new Proxy<typeof obj>(obj,{
//     // apply(target,This,reset){
//     //     return Reflect.apply(target,This,reset)
//     // }
// })

// obj_copy.a='Math'

// console.log(obj_copy);
// console.log(obj);

function target(){
    console.log(arguments);
}

let target2 = new Proxy(target,{
    apply(target,This,reset){
        console.log(arguments);
        return Reflect.apply(target,This,reset)
    }
})

// console.log(Reflect.apply(target2,{},[1,2,3]));
let obj={
    a:1,
    b:2,
    c:3
}
let target3 = new Proxy(obj,{
    defineProperty(target,key,value){
        return Reflect.defineProperty(target,key,value)
    },

})

Reflect.defineProperty(target3,'a',{
    value:Math.random(),
    enumerable:true,
    configurable:true,
    writable:true
})

console.log(target3);
console.log(obj);

function Flow(){}
Flow[Symbol.hasInstance]=function(){
    console.log(123);
    return false
}
let f = new Flow()
console.log(f instanceof Flow);


let obj2={
    [Symbol.asyncIterator]:function(){
        let values = Object.values(this)
        let index = 0
        return {
            next(){
                return Promise.resolve({
                    value:values[index],
                    done:!(values[index++])
                })
            }
        }
    }
}

function *gen(arr){
    console.log(arguments);
    let index = yield 1
    let list = yield * [2,3,4]
    console.log(list);
    let ls = yield * arr
    console.log(ls);
    return 5
}
let l = gen([1,2,3])
console.log(l);



let obj3={
    firstName:"文军",
    lastName:"邱",
    get fullName(){
        return this.firstName + this.lastName
    },
    set fullName(value){
        console.log(value);
        
    }
}

console.log(obj3.fullName);
