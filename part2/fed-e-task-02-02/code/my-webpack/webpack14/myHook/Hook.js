"use strict";
exports.__esModule = true;
require("./common");
var Hook = /** @class */ (function () {
    function Hook(args) {
        this.args = args;
        this.taps = [];
        this._formal = args;
        this._argument = [];
        this._x = [];
    }
    Hook.prototype.tap = function (name, fn) {
        if (typeof name === 'string') {
            name = { name: name };
        }
        this.taps.push(Object.assign({ type: "sync", fn: fn }, name));
    };
    Hook.prototype.tapAsync = function (name, fn) {
        if (typeof name === 'string') {
            name = { name: name };
        }
        this.taps.push(Object.assign({ type: "async", fn: fn }, name));
    };
    Hook.prototype.call = function () {
        var _args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            _args[_i] = arguments[_i];
        }
        this._argument = _args;
        return this.compile().apply(this, _args);
    };
    Hook.prototype.callAsync = function () {
        var _args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            _args[_i] = arguments[_i];
        }
        this._argument = _args;
        return this.compile().apply(this, _args);
    };
    return Hook;
}());
exports["default"] = Hook;
