const Path = require('./path.js')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const { merge } = require('webpack-merge')
const { headList, footList } = require(Path('baseData/base.js'))

const baseOpt = {
  entry: {
    home: {
      import: Path('src/home.js')
    },
    shop: {
      import: Path('src/shop.js')
    },
    detail: {
      import: Path('src/detail.js')
    },
    kefu: {
      import: Path('src/kefu.js')
    }
  },
  output: {
    filename: 'js/[name].[contenthash:8].js',
    path: Path('dist')
  },
  resolve: {
    alias: {
      '@': Path('src')
    },
    extensions: ['.js', '.css', '.less', 'ts', '.html', '.md']
  },
  module: {
    rules: [
      {
        test: /\.(png|jpe?g|gif|svg)$/,
        type: 'asset',
        generator: {
          filename: 'img/[name].[hash:8][ext]'
        },
        parser: {
          dataUrlCondition: {
            maxSize: 10 * 1024
          }
        }
      },
      {
        test: /\.js$/,
        use: [
          'babel-loader',
          'eslint-loader'
        ]
      },
      {
        test: /\.md$/,
        use: [
          Path('mk-loader.js')
        ]
      },
      {
        test: /\.less$/,
        use: [
          'style-loader',
          {
            loader: 'css-loader',
            options: {
              esModule: false,
              importLoaders: 2
            }
          },
          'postcss-loader',
          'less-loader'
        ]
      },
      {
        test: /\.ejs$/,
        use: [
          {
            loader: 'ejs-loader',
            options: {
              esModule: false,
              variable: 'data'
            }
          }
        ]
      }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: Path('public/html/home.ejs'),
      filename: 'home.html',
      title: '首页',
      data: headList,
      footList,
      chunks: ['home']
    }),
    new HtmlWebpackPlugin({
      template: Path('public/html/shop.ejs'),
      filename: 'shop.html',
      title: '商品',
      data: headList,
      footList,
      chunks: ['shop']
    }),
    new HtmlWebpackPlugin({
      template: Path('public/html/kefu.ejs'),
      filename: 'kefu.html',
      title: '客服',
      data: headList,
      footList,
      chunks: ['kefu']
    }),
    new HtmlWebpackPlugin({
      template: Path('public/html/detail.ejs'),
      filename: 'detail.html',
      title: '详情',
      data: headList,
      footList,
      chunks: ['detail']
    })
  ]
}

module.exports = (env, argv) => {
  console.log(env)// cli传递的环境参数
  if (env.production) {
    return merge(baseOpt, require('./webpack.prod'))
  } else {
    return merge(baseOpt, require('./webpack.dev'))
  }
}
