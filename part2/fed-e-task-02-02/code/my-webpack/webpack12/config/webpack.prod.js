const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const TerserPlugin = require('terser-webpack-plugin')
// const ImageminPlugin = require('imagemin-webpack-plugin').default;

module.exports = {
  mode: 'production',
  // mode:"development",
  // devtool:"eval-source-map",
  optimization: {
    runtimeChunk: 'single',
    usedExports: false,
    sideEffects: true,
    minimize: true,
    minimizer: [
      new TerserPlugin({
        extractComments: false
      })
    ],
    splitChunks: {
      chunks: 'all'
    }
  },
  module: {
    rules: [

    ]
  },
  plugins: [
    new CleanWebpackPlugin(),
    new TerserPlugin()
    // new ImageminPlugin()
  ]
}
