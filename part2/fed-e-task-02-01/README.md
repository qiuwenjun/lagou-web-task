## 简答题（老师！这个模块我还需要重新学习一遍，作业暂时搁置，后续补上）

**1、谈谈你对工程化的初步认识，结合你之前遇到过的问题说出三个以上工程化能够解决问题或者带来的价值。**

答:前端工程化是指遵循一定的标准和规范，通过工具去提高效率、降低成本的一种手段。

+ 想要使用ES6+新特性，但是兼容有问题
+ 想要使用Less/Sass/PostCSS增强CSS的编程性，但是运行环境不能直接支持
+ 想要使用模块化的方式提高项目的可维护性，但运行环境不能直接支持
+ 部署上线前需要手动压缩代码及资源文件，部署过程需要手动上传代码到服务器
+ 多人协同开发，无法硬性统一大家的代码风格，从仓库中pull回来的代码质量无法保证

　

**2、你认为脚手架除了为我们创建项目结构，还有什么更深的意义？**

答: 脚手架的本质作用就是创建项目基础结构、提供项目规范和约定。

+ 相同的组织结构
+ 相同的开发范式
+ 相同的模块依赖
+ 相同的工具配置
+ 相同的基础代码

　

　

## 编程题

**1、概述脚手架实现的过程，并使用 NodeJS 完成一个自定义的小型脚手架工具**

　

　

**2、尝试使用 Gulp 完成项目的自动化构建**  ( **[先要作的事情](https://gitee.com/lagoufed/fed-e-questions/blob/master/part2/%E4%B8%8B%E8%BD%BD%E5%8C%85%E6%98%AF%E5%87%BA%E9%94%99%E7%9A%84%E8%A7%A3%E5%86%B3%E6%96%B9%E5%BC%8F.md)** )

(html,css,等素材已经放到code/pages-boilerplate目录)

　

　

## 说明：

本次作业中的编程题要求大家完成相应代码后

- 提交一个项目说明文档，要求思路流程清晰。
- 或者简单录制一个小视频介绍一下实现思路，并演示一下相关功能。
- 说明文档和代码统一提交至作业仓库。