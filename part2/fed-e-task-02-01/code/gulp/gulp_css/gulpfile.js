const { src, dest } = require('gulp')
const gulpCleanCss = require('gulp-clean-css')
const gulpRename = require('gulp-rename')
exports.css = () => {

   return src('./src/css/*.css').pipe(gulpCleanCss()).pipe(gulpRename({extname:".min.css"})).pipe(dest('./dist/css'))   

}