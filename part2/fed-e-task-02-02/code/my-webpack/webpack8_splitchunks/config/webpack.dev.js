const Path = require('path');
const ReactRefreshWebpackPlugin = require('@pmmmwh/react-refresh-webpack-plugin')
const { DefinePlugin } = require('webpack')
module.exports = {
    devtool:"cheap-module-source-map",
    mode:"development",
    devServer:{
        hot:true
    },
    plugins:[
        new ReactRefreshWebpackPlugin(),
        new DefinePlugin({
            "process.env.React_app":JSON.stringify({
                name:'REACT',
                version:18
            }),
        }),
    ]
}