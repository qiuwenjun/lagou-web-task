const Path = require('path');
const webpack = require('webpack');
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const TerserPlugin = require('terser-webpack-plugin')
module.exports = {

    mode:'production',
    entry:{
        react:['react','react-dom']
    },
    output:{
        filename:"dll_[name].js",
        path:Path.resolve(__dirname,'dll'),
        library:'dll_[name]'
    },
    optimization:{
        minimize:true,
        minimizer:[
            new TerserPlugin({
                extractComments:false,    
            })
        ] 
    },
    plugins:[
        new webpack.DllPlugin({
            name:'dll_[name]',
            path:Path.resolve(__dirname,'./dll/[name].manifest.json')
        }),
    ]

}