const webpack = require('webpack');
const webpackDevConfig = require('./webpack.config');

const compile = webpack(webpackDevConfig);
compile.run(function(err,status){
    console.log(status.toJson({
        entries:true,
        chunks:false,
        modules:false,
        assets:false
    }))
})