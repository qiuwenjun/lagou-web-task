# 简答题

#### 1.简述前端兼容性的解决方案及不同工具的使用（CSS及JS）

### browserslistrc

  1 项目工程化需求

  2 需要兼容CSS jS

  3 如何实现兼容：babel postCss

  4 到底要兼容哪些平台可以通过can1use.com这个网站查找各个平台占有率，然后根据条件进行兼容

  npx browserslist || yarn browserslist 会走默认配置，返回对应的浏览器平台

### babel

https://www.babeljs.cn/
处理像js,jsx等兼容性的代码

核心包 @babel/core
单独使用需要安装@babel/cli

```js
npx babel script.js //编译文件
npx babel script.js --out-file script-compiled.js //输出到文件 --out-file 或 -o 参数。
npx babel script.js --watch --out-file script-compiled.js //文件修改后 --watch 或 -w 参数
npx babel src --out-file script-compiled.js //编译整个 src 目录下的文件并输出到 lib 目录，输出目录可以通过 --out-dir 或 -d 指定。这不会覆盖 lib 目录下的任何其他文件或目录。
npx babel src --out-dir lib --ignore "src/**/*.spec.js","src/**/*.test.js" //忽略某些文件
npx babel src --out-dir lib --copy-files //复制不需要编译的文件
npx babel script.js --out-file script-compiled.js --plugins=@babel/proposal-class-properties,@babel/transform-modules-amd //使用插件
npx babel script.js --out-file script-compiled.js --presets=@babel/preset-env,@babel/flow //使用 Preset
```

webpack中使用安装babel-loader

#### @babel/preset-env

@babel/preset-env用来转换es语法，

#### polyfill

对于api和一些表达式就必须使用polyfill
webpack4中内置了polyfill，所以我们并不需要手动引入，但是因为它是全局引入，所以有些内容我们可能是不需要的，所以到了webpack5，就给移除掉了，需要我们手动引入!

核心包:@babel/polyfill 这个是兼容所有api和表达式的包
而这其中其实我们只需要安装下面的两个core-js/stable和regenerator/runtime

```js
npm i -S core-js & npm i -S regenerator
```

配置方式

```js
babel-loader
//单独一个一个的插件
{
    test:/\.js$/,
    exclude:/node_modules/,	//排除掉指定路径下的文件
    use:[
       {
          loader:'babel-loader',
          options:{
             plugins:[
                 '@babel/plugin-transform-arrow-functions',
                 '@babel/plugin-transform-block-scoping'
             ],		
           }
        }
     ]
}
//插件的集合presets
{
    loader:'babel-loader',
    options:{
       presets:[
           [
              "@babel/preset-env",
              {
                  "targets":"chrome 102" //设置要兼容的浏览器版本，这里可以单独配置，如果不配置，那就走browserslistrc的配置或者默认配置项
              }
            ]
       ]
    }
}
* babel-loader 相关的单独配置文件
* babel.config.js(json cjs mjs)
* babelrc.json(js)
module.exports = {
    // plugins:[
    //     '@babel/plugin-transform-arrow-functions',
    //     '@babel/plugin-transform-block-scoping'
    // ],	
    presets:[
      //  [ 
      //      "@babel/preset-env",
      //      {
      //          "targets":"chrome 102"
      //      }
      //  ],
        [ 
            "@babel/preset-env",
            {
                // "targets":"chrome 102"
                /*
                    false: 不对当前的js处理做polyfill的填充
                    usage: 会自动检查用户所使用到的新api或表达式进行兼容
                    entry: 根据browserslistrc配置文件所查找出来的浏览器进行兼容
                    需要在使用到的地方手动引入
                    import "core-js/stable";
                    import "regenerator-runtime/runtime";
                */ 
                "useBuiltIns":'entry',
                "corejs":3  //当babel执行的时候默认使用的是2的版本，而我们现在安装的是3的版本，所以需要我们手动指定版本
            }
        ]
    ]
}
```

****

### postCss

 https://autoprefixer.github.io/
帮助我们使用javascript对css进行转换  

核心包：postcss
单独使用需要安装postcss-cli
webpack中使用安装postcss-loader

当我们需要对css文件进行兼容时候

```css
* PostCSS: v8.4.12, 核心的转换插件
* Autoprefixer: v10.4.4 对css进行兼容处理  需要单独安装 autoprefixer
* Browsers: last 4 version 要兼容的浏览器版本 这个需要安装browserslist
* postcss-preset-env postcss所使用的到的一系列插件的集合
```

自定义配置

```js
1 在webpack.congig.js文件中配置
{
    loader:'postcss-loader',
        options:{
            postcssOptions:{
                plugins:[
                    require('autoprefixer'),	//处理css兼容的插件
                    require('postcss-preset-env')  //很多插件的集合
                ]
            }
        }
},

2 根目录下创建postcss.config.js文件导出参数
当我们使用postcss插件需要配置多个地方的时候，就可以单独配置一个postcss.config.js文件了
module.exports={
    plugins:[
        require('autoprefixer'),
        require('postcss-preset-env')
    ]
}
```

#### 2.列举三种常见的webpack打包优化手段及使用步骤

### 1 按需加载,

import动态导入配置，代码懒加载，预加载模块

```js
//有些代码在首屏时可能不需要立马加载，这时候就可以用代码懒加载
()=>import('加载文件路径信息');

//需要用到某个模块时，再加载这个模块，动态导入的模块会被自动分包。通过动态导入生成的文件只是一个序号，可以使用魔法注释指定分包产生bundle的名称。相同的chunk名会被打包到一起。其实就是vue中的路由懒加载和和把某个路由下的所有组件都打包在同个异步块 (chunk) 中
import(/* webpackChunkName: 'posts' */'./post/posts').then({default: posts}) => {
  mainElement.appendChild(posts())
}

/*
    prefetch 与 preload 预加载模块,
        prefetch：将来某些导航可能需要资源,<link rel="prefetch" href="login-modal-chunk.js">被附加到页面的头部，这将指示浏览器在空闲时间预取login-modal-chunk.js文件
        preload : 在当前导航期间也需要资源
        预加载的块开始与父块并行加载。预取的块在父块完成加载后开始。
        预加载的块具有中等优先级并立即下载。浏览器空闲时会下载预取的块。
        父块应立即请求预加载的块。未来任何时候都可以使用预取的块。
        浏览器支持不同。
        import(/* webpackPrefetch: true */ './path/to/LoginModal.js');
*/
```

### 2 Tree shaking(树摇)

优化代码中没有用到的代码，并在打包时去除!
这个功能对esModule模块化支持最好，common.js模块化支持较弱

##### usedExports 

标记出源代码中没有用到的功能和代码

```js
module.exports = {
	optimization:{
		usedExports:true,		//标记
        minimize:true,
        minimizer:[
			new TerserPlugin({}),//移除
		]
	}
}
```

##### sideEffects 

某些模块下面有些函数是不被使用的,可以通过配置usedExports为true对那些不常用的代码进行标记，然后通过TerserPlugin插件将其排除，但是webpack中还有一些副作用代码，上面的方式无法做到，这时候我们就可以通过sideEffect来做到，具体配置如下

```js
//在webpack.config.js中开启这个功能：
module.exports={
    optimization: {
        usedExports: true,
        minimize: true,
        concatenateModules: true,
        sideEffects: true
    }
}
//在package.json里面增加一个属性sideEffects,值为false，表示没有副作用，没有用到的代码则不进行打包。确保你的代码真的没有副作用，否则在webpack打包时就会误删掉有副作用的代码，比如说在原型上添加方法，则是副作用代码；还有CSS代码也属于副作用代码。
{
  "name": "awesome npm module",
  "version": "1.0.0",
  "sideEffects": false,		//设置为false时,对所有文件起作用
  "sideEffects": [
      "./src/js/*.js", //文件路径，可以使用glob匹配模式
  ],		//内部设置的所有文件将会被忽略优化
}
//上述的配置都是全局的，如果说有些类型的文件不需要优化，也可以单独配置
module.exports = {
    module:{
		rules:[
		   {
               test:/\.css$/,
               sideEffects:true,  //局部配置优化
               use:[
                   env ? MiniCssExtractPlugin.loader : "style-loader",
                   {
                       loader:"css-loader",
                       options:{
                           esModule:false,
                           importLoaders:1
                       }
                   }
               ]
             }
        ]
	}
}
```

### 3 scope hoisting(作用域提升)

过去，捆绑时 webpack 的权衡之一是捆绑中的每个模块都将包装在单独的函数闭包中。这些包装器函数使 JavaScript 在浏览器中的执行速度变慢。相比之下，像 Closure Compiler 和 RollupJS 这样的工具“提升”或将所有模块的范围连接到一个闭包中，并允许您的代码在浏览器中具有更快的执行时间。

这个插件将在 webpack 中启用相同的连接行为。默认情况下，此插件已在[生产`mode`](https://webpack.js.org/configuration/mode/#mode-production)中启用，否则禁用。如果您需要覆盖生产`mode`优化，请将[`optimization.concatenateModules`选项](https://webpack.js.org/configuration/optimization/#optimizationconcatenatemodules)设置为`false`. 要在其他模式下启用串联行为，您可以`ModuleConcatenationPlugin`手动添加或使用以下`optimization.concatenateModules`选项：

```js
module.exports = {
    plugins:[
        new webpack.optimize.ModuleConcatenationPlugin();//以后想要用到，最后到时写esModule模式的代码
    ],
    //或者
	optimization: {
        usedExports: true,
        minimize: true,
        concatenateModules: true
    }
}
```

### Dll库

dll动态链接库，把webpack中常用到的一些共享资源，一些较大的，不长变动的代码随着每次打包而打包！
所以dll就是相当于把这些内容单独封装出来，做成一个dll的动态链接库！

### 引入CDN

1 自己项目打包完成后想要放到cdn
2 取消打包项目中的第三方库，直接使用cdn

#### 作业要求

答案写在当前文件中, 保存后提交到作业仓库即可
