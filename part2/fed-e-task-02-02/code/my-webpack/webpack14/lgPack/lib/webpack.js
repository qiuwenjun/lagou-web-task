const Compiler = require('./Compiler')
const webpack = function (options) {
    // 1 初始化Compiler
    const compiler = new Compiler(options.context)
    compiler.options = options

    // 2 初始化NodeEnviro 让compiler对象具备文件读写能力

    // 3 挂载所有plugins插件至compiler对象身上

    // 4 挂载所有webpack内置插件的入口

    // 5 返回compiler对象
}

module.exports = webpack
