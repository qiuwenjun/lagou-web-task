const Path = require('path');
module.exports = {
    mode:"development",
    entry:'./src/index.ts',
    output:{
        filename:'[name].[hash:6].js',
        path:Path.resolve(__dirname,'dist')
    },
    module:{
        rules:[
            {
                test:/\.ts$/,
                use:[
                    "babel-loader"
                ]
            }
        ]
    }
}