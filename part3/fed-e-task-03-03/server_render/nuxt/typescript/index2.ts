type D = { a:string }
type E = { b:string }
type F = { a?:number, b?:number }

function getAjAX(val:D|E|F){
    if('a' in val){
        return val.a
    }
    return val.b
}


function example(){
  let x:string|number|boolean;
  x=Math.random()<0.5
  if(Math.random() < 0.5){
    x = 'hello'
  }else{
    x=100
  }
  return x  
}

const x = example(); //x的类型是number|string的联合类型  

type Bird = {
    name:string,
    fly:()=>void
}
type Fish = {
    name:string,
    swim:()=>void
}

let bird:Bird = {
    name:"鸟",
    fly:()=>{}
}
let fish:Fish = {
    name:"鱼",
    swim:()=>{}
}

function isFish(type:Fish|Bird):type is Fish{
    return 'swim' in type
}
let value = Math.random()<0.5?bird:fish
if(isFish(value)){
    value.swim()
}else{
    value.fly()
}

type FnDesc={
    desc:string;
    (value:string):string
}
function fn2(value:string):string{
    return value
}
fn2.desc='123'
let FnDesc:FnDesc=fn2

class Array2{
    static getName(str:string):string{
        return str
    }
    constructor(public s:string){

    }
}
type Array2Constructor = {
    getName(str:string):string
    new (s:string):Array2   //当做类构造函数执行
    (s:string):string   //当做普通函数执行
}
function fn(ctor:Array2Constructor){
    return new ctor('邱文军')
}
let value2 = fn(Array2)
let arr:(string|number)[]=[1,'2',3,'4']

export {}

function firstElement<Type>(arr:Type[],str:Type):Type|undefined{
    return arr[0]
}
firstElement([1],['2'])

function Map<Input,Output>(arr:Input[],func:(arg:Input)=>Output):Output[]{
    return arr.map(func)
}
const list = Map([1,2,3],(item)=>item*2)

// function longest<type extends { length:number }>(a:type,b:type){
//     if(a.length > b.length){
//         return a
//     }else{
//         return b
//     }
// }

// const longerString = longest('邱文军','哈哈哈')
// const longerArr = longest([1,2],[3,4,5])

//当泛型没有作用到参数上时，这时候泛型类型需要用户手动传递
function ceshiFx<type>(args:string[]){}
let val = ceshiFx<number>(['123'])

//如果泛型类型作用到形参上,这时候用户使用函数的时候没有指明泛型类型，那这时候ts就会自动从我们传的实参上进行类型推导
//
function longest<type extends { length:number }>(a:type,b:type){
    if(a.length > b.length){
        return a
    }else{
        return b
    }
}
const longerString = longest('邱文军','哈哈哈')
function longest2<type extends { length:number },bool>(a:type,b:bool){
    // if(a.length > b.length){
    //     return a
    // }else{
    //     return b
    // }
}
const longerString2 = longest2('邱文军','哈哈哈')

function Map<Input,Output>(arr:Input[],func:(arg:Input)=>Output):Output[]{
    return arr.map(func)
}
const list = Map([1,2,3],(item)=>item*2)
