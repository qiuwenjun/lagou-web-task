function getAge(min:number):(age:number)=>boolean{
    return function(age:number):boolean{
        return age >= min
    }
}

let checkAge18 = getAge(18)
let checkAge24 = getAge(24)

console.log(checkAge18(20));
console.log(checkAge18(22));
console.log(checkAge24(23));
console.log(checkAge24(24));
