var Dep = /** @class */ (function () {
    function Dep() {
        this.subs = []; //存储所有的观察者
    }
    Dep.prototype.addSub = function (watcher) {
        if (watcher === null || watcher === void 0 ? void 0 : watcher.update) {
            this.subs.push(watcher);
        }
    };
    Dep.prototype.notify = function () {
        this.subs.forEach(function (item) { return item.update(); });
    };
    return Dep;
}());
