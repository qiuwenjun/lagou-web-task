import React,{Component} from 'react'
import './css/App'
export default class App extends Component{
    state={
        title:"前端开发"
    }
    constructor(props){
        super(props)
    }
    render(){
        return (
            <div className="title">
                <h1>{this.state.title}</h1>
            </div>
        )
    }
}