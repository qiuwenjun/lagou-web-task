const { src, dest, series, parallel, watch } = require('gulp')
// const gulpLoad = require('gulp-load-plugins')
// const plugins = gulpLoad();
const plugins = {}
plugins.sass = require('gulp-sass')(require('sass'))
plugins.css = require('gulp-css')
plugins.cssmin=require('gulp-cssmin')
plugins.babel = require('gulp-babel')
plugins.swig = require('gulp-swig')
plugins.clean = require('gulp-clean')
plugins.imagemin = require('gulp-imagemin')
plugins.debug = require('gulp-debug')
plugins.changed=require('gulp-changed')
plugins.useref = require('gulp-useref')
plugins.htmlmin = require('gulp-htmlmin')
plugins.cssmin = require('gulp-cssmin')
plugins.uglify = require('gulp-uglify')
plugins.if = require('gulp-if')
plugins.eslint = require('gulp-eslint')
const del = require('del')
const bs = require('browser-sync').create()

const data = {
    menus: [
      {
        name: 'Home',
        icon: 'aperture',
        link: 'index.html'
      },
      {
        name: 'Features',
        link: 'features.html'
      },
      {
        name: 'About',
        link: 'about.html'
      },
      {
        name: 'Contact',
        link: '#',
        children: [
          {
            name: 'Twitter',
            link: 'https://twitter.com/w_zce'
          },
          {
            name: 'About',
            link: 'https://weibo.com/zceme'
          },
          {
            name: 'divider'
          },
          {
            name: 'About',
            link: 'https://github.com/zce'
          }
        ]
      }
    ],
    pkg: require('./package.json'),
    date: new Date()
  }

const uDel = (tempList = ['dist','temp']) => {
  tempList = Array.isArray(tempList) ? tempList : [tempList]
  return ((done) => {
    del.sync(tempList)
    done()
  })
}

const uCss = () => {
  return src('./src/assets/styles/*.scss',{ base : 'src' }).pipe(plugins.sass({outputStyle:'expanded'})).pipe(plugins.css()).pipe(plugins.cssmin()).pipe(dest('./temp/')).pipe(bs.reload({ stream: true }))
}

const uJs = () => {
  return src('./src/assets/scripts/*.js',{base:'src'}).pie(plugins.eslint()).pipe(plugins.babel({
      "presets":["@babel/preset-env"],
      "plugins": [
        "@babel/plugin-transform-runtime",
      ]
  })).pipe(dest('./temp/')).pipe(bs.reload({ stream: true }))
}

const uHtml = () => {
  return src('./src/*.html').pipe(plugins.swig({ 
    data,
    defaults: {
      cache: false
    }
  })).pipe(dest('./temp/')).pipe(bs.reload({ stream: true }))
}

const uImage = () => {
  return src('./src/assets/images/**',{base:'src'}).pipe(plugins.imagemin()).pipe(dest('./dist/'))
}

const uFont = () => {
  return src('./src/assets/fonts/**',{base:'src'}).pipe(plugins.imagemin()).pipe(dest('./dist/'))
}

const public = () => {
  return src('./public/**').pipe(dest('./dist/public'))
}

const useref = () =>{
  return src('./temp/*.html',{base:"temp"})
  .pipe(plugins.useref({searchPath:['temp','.']}))
  .pipe(plugins.if(/\.js$/,plugins.uglify()))
  .pipe(plugins.if(/\.css$/,plugins.cssmin()))
  .pipe(plugins.if(/\.html$/,plugins.htmlmin({
    collapseWhitespace: true,
    minifyCSS: true,
    minifyJS: true
  })))
  .pipe(dest('dist'))
}

const serve = () => {
  watch('src/**/*.html',uHtml)
  watch('src/assets/scripts/*.js',uJs)
  watch('src/assets/styles/*.scss',uCss)
  // watch('public/**',public)
  // watch('src/assets/images/**',uImage)
  // watch('src/assets/fonts/**',uFont)
  watch(['public/**','src/assets/images/**','src/assets/fonts/**'],()=>{
    bs.reload()
  })

  bs.init({
    notify:false,
    // files:'./dist/**',
    port:9999,
    open:false,
    // watch:true,
    server:{
      baseDir:['temp','src','public'],
      routes:{
        '/node_modules':'node_modules'
      }
    },
  })
}

const base = series(uDel(), parallel(uCss, uJs, uHtml))

const server = series(base, serve)

const build = series(base, parallel(useref, uImage, uFont, public),uDel('temp'))

module.exports = {
  clear:uDel,
  build,
  server
}