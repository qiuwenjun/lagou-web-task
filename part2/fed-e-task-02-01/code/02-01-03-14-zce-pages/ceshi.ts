declare function flowRight(a:number):void
dec
import { flowRight } from 'lodash'

// flowRight()

// type m = 'string' | 'adw' & []
// let str:m = 'string'

// interface obj1{
//     name:'邱文军1'
//     a:1
// }

// interface obj2{
//     name:"邱文军2",
//     b:2
// }
declare var as:number
var as=123

declare function getAjax(options:{x:number,y:number}):void

function getAjax(options){

}

type obj = {
    a:number;
    name:string;
} & {
    b:number;
    name:number;
}

type IntersectionTypeConfict = { id: number; name: string; } & { age: number; name: number; };
let obj:obj={
    a:1,
    b:2,
    name:'awd'
}

function shallowReadonly<T extends object>(target: T): void{

}
shallowReadonly(new Number(1))

let str:string='123';
let unk:unknown=123;
let a:any=123;

type h = (string | number) & {};        //类型缩减

let h:h=123

type Exclues<T,U> = T extends U ? T : never;
type a=Exclues<'a'|'b'|'c','a'|'b'>