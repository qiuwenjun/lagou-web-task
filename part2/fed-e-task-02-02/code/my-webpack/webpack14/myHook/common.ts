//taps 数据对象
interface TapOptions{
    type:string
    name:string
    fn:Function
}

interface FactoryOpt{
    taps:TapOptions[]
    args:any[]
}