//高阶函数-函数作为返回值

function makeFn(){
 let msg='hello,您好全新的邱文军'
 return function(){
     console.log(msg);
 }   
}

console.log(makeFn()());//闭包

function once(){
    let status = 0
    return function(money){
        if(!status){
            status=1
            console.log(`支付${money}RMB`);
        }
    }
}

once()('520')