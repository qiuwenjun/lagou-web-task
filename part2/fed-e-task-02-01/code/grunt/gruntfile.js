const loadGruntTasks= require('load-grunt-tasks')
module.exports = grunt => {

    grunt.initConfig({
        less:{
            development: {
                options: {
                    sourceMap:true
                },
                files: {
                    //首页
                    './dist/css/index.css': ['./src/less/common.less','./src/less/index.less'],
                    //商品
                    './dist/css/shop.css': ['./src/less/common.less','./src/less/shop.less'],
                    //用户
                    './dist/css/user.css': ['./src/less/common.less','./src/less/user.less'],
                }
            },
            production:{
                options: {
                    plugins: [
                      new (require('less-plugin-autoprefix'))({browsers: ["last 2 versions"]}),
                      new (require('less-plugin-clean-css'))({advanced: true})
                    ],
                    // modifyVars: {
                    //   imgPath: '"http://mycdn.com/path/to/images"',
                    //   bgColor: 'red'
                    // }
                  },
                  files: {
                    //首页
                    './dist/css/index.css': ['./src/less/common.less','./src/less/index.less'],
                    //商品
                    './dist/css/shop.css': ['./src/less/common.less','./src/less/shop.less'],
                    //用户
                    './dist/css/user.css': ['./src/less/common.less','./src/less/user.less'],
                }
            }
        }
    })
    
    loadGruntTasks(grunt)

}