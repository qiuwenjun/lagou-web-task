"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const MyArray = [
    { name: '邱文军', age: 26 },
    { name: '程平宝', age: 50 },
    { name: '邱宏卫', age: 52 },
];
let a = [1];
function CreateObject(value) {
    if (typeof value === 'number') {
        return {
            name: '邱文军'
        };
    }
    else {
        return {
            age: 26
        };
    }
}
CreateObject(1);
let Arr = {
    forEach(callback) {
        for (let attr in this) {
            callback(attr);
        }
    }
};
