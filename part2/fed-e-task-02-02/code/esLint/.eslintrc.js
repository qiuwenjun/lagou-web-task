module.exports = {
    //运行环境，根据环境判断能够使用哪些全局变量
    "env": {
        "browser": true,
        "es6":false,
    },
    // eslint 继承风格的共享配置
    "extends": [
        "standard"
    ],
    //使用哪种解析器，这里是typescript
    "parser": "@typescript-eslint/parser",
    //设置语法解析器，控制是否允许使用某个版本的语法
    "parserOptions": {
        "ecmaVersion":5,
        "sourceType": "module"
    },
    //使用到的插件
    "plugins": [
        "@typescript-eslint"
    ],
    // 控制某个校验规则的开启和关闭
    "rules": {
        'no-alert': 'error'
    },
    // 添加自定义的全局变量
    "globals": {
        "$": 'readonly', 
    }
}
