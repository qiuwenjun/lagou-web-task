import Vue from 'vue'
import Router from 'vue-router'
import { normalizeURL, decode } from 'ufo'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _70b43b93 = () => interopDefault(import('..\\pages\\home.vue' /* webpackChunkName: "pages/home" */))
const _aa7b04e2 = () => interopDefault(import('..\\pages\\home\\index.vue' /* webpackChunkName: "pages/home/index" */))
const _2bf7679c = () => interopDefault(import('..\\pages\\home\\home.vue' /* webpackChunkName: "pages/home/home" */))
const _431716a5 = () => interopDefault(import('..\\pages\\list_index.vue' /* webpackChunkName: "pages/list_index" */))
const _2b79f628 = () => interopDefault(import('..\\pages\\list\\order.vue' /* webpackChunkName: "pages/list/order" */))
const _aa7188b6 = () => interopDefault(import('..\\pages\\list\\_index.vue' /* webpackChunkName: "pages/list/_index" */))
const _5c493212 = () => interopDefault(import('..\\pages\\list\\index\\index.vue' /* webpackChunkName: "pages/list/index/index" */))
const _0bdf0b64 = () => interopDefault(import('..\\pages\\index.vue' /* webpackChunkName: "pages/index" */))

const emptyFn = () => {}

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: '/',
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/home",
    component: _70b43b93,
    children: [{
      path: "",
      component: _aa7b04e2,
      name: "home"
    }, {
      path: "home",
      component: _2bf7679c,
      name: "home-home"
    }]
  }, {
    path: "/list_index",
    component: _431716a5,
    name: "list_index"
  }, {
    path: "/list/order",
    component: _2b79f628,
    name: "list-order"
  }, {
    path: "/list/:index",
    component: _aa7188b6,
    children: [{
      path: "",
      component: _5c493212,
      name: "list-index"
    }]
  }, {
    path: "/",
    component: _0bdf0b64,
    name: "index"
  }],

  fallback: false
}

export function createRouter (ssrContext, config) {
  const base = (config._app && config._app.basePath) || routerOptions.base
  const router = new Router({ ...routerOptions, base  })

  // TODO: remove in Nuxt 3
  const originalPush = router.push
  router.push = function push (location, onComplete = emptyFn, onAbort) {
    return originalPush.call(this, location, onComplete, onAbort)
  }

  const resolve = router.resolve.bind(router)
  router.resolve = (to, current, append) => {
    if (typeof to === 'string') {
      to = normalizeURL(to)
    }
    return resolve(to, current, append)
  }

  return router
}
