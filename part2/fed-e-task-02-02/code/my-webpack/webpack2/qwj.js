const Path = require('path');
// console.log(process.env);
module.exports = {
    mode:"development",
    entry:"./src/index.js",
    output:{
        filename:"build.js",
        path:Path.resolve(__dirname,'dist')
    },
    module:{
        rules:[
            // 第一种
            {            
                test:/\.css$/,
                loader:'style-loader'
            },
            {            
                test:/\.css$/,
                loader:'css-loader'
            },
            //第二种
            {
                test:/\.css$/,
                rules:[
                    {
                        loader:"style-loader",
                        options:{}
                    },
                    {
                        loader:"css-loader",
                        options:{}
                    },
                ]
            },
            // 第三种,有点相当于前两种的综合体
            {
                test:/\.css$/,
                use:[
                    {
                        loader:"style-loader",
                        options:{}
                    },
                    {
                        loader:"css-loader",
                        options:{}
                    }
                ]
            }
        ]
    }
}