import folktale = require('folktale/core/lambda')
import fp = require('lodash/fp')

let _curry = folktale.curry(2,function(a:number,b:number):number{
    return a+b
})

console.log(_curry(1)(2))

let _compose = folktale.compose(fp.toUpper,fp.first)
console.log(_compose(['one','two']));
