// import fp from 'lodash/fp'
const fp = require('lodash/fp')

class Io{
    static of(value){
        return new Io(function(){
            return value
        })
    }
    static callFn(This){
        return This.#value()
    }
    #value
    constructor(value){
        this.#value=value
    }
    map(fn){
        return new Io(fp.flowRight(fn,this.#value))
    }
}

let r = Io.of(process).map(e=>e.execPath)
console.log(Io.callFn(r));