import './common'
export default class HookFactory{
    type:string
    options:FactoryOpt
    constructor(){
        this.options = {
            taps:[],
            args:[]
        }
    }
    args(opt?:Partial<{before:string,after:string}>){
        let Args=''
        if(opt?.before) Args += opt.before + ',';
        if(this.options.args) Args += this.options.args.join();
        if(opt?.after) Args += ',' + opt.after;
        return Args.replace(/,$/,'')
    }
    head(){
        return `var _countNum = this.taps.length; var _x = this._x;`
    }
    content(){
        if(this.type === 'asyncFactoryHook'){
            return `
                const next = () => {
                    --_countNum <= 0 && _callback()
                };
                for(let i=0; i<_x.length; i++){
                    _x[i](...this._argument.slice(0,this._formal.length),next)
                }
            `
        }else if(this.type === 'SyncFactory'){
            return this.options.taps.reduce((prev:string,item:TapOptions,index:number)=>{
                prev += `var _fn${index} = _x[${index}]; _fn${index}(...this._argument);`
                return prev
            },'')
        }
    }
}