console.log(process.env.NODE_ENV);

const presets = [
    [
        "@babel/preset-env",
        {
            "useBuiltIns":"usage",
            "corejs":3
        }
    ],
    "@babel/preset-react"
];
const plugins = [];

if(process.env.NODE_ENV === 'development'){
    plugins.push(['react-refresh/babel'])
}else{

}
module.exports = {
    presets,
    plugins
}