import { createApp } from 'vue'
import App from './views/index.vue'
import './js/title.js'


const app = createApp(App)

app.mount('#app')

if(module.hot){
   module.hot.accept(['./js/title.js'],(file)=>{
      console.log(file+'文件更新了!');
   })
}