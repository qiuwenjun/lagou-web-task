module.exports = plop => {
    plop.setGenerator('component',{
        description:'create a component',
        prompts:[
            {
                type:'input',       //用户输入的
                name:'name',        //字段
                message:'component name',   //提示用户的信息
                default:'MyComponent'   //默认值
            }
        ],
        actions:[               //动作
            {
                type:"add",   //代表添加文件
                path:"src/components/{{name}}/{{name}}.tsx",
                templateFile:"./plop-template/component.hbs"
            },
            {
                type:"add",   //代表添加文件
                path:"src/components/{{name}}/{{name}}.css",
                templateFile:"./plop-template/component.hbs"
            },
            {
                type:"add",   //代表添加文件
                path:"src/components/{{name}}/{{name}}.test.ts",
                templateFile:"./plop-template/component.hbs"
            }
        ]
    })
}