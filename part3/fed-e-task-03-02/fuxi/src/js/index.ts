type obj = {
    currentPage:number;
    pageSize:number;
    [prop:string]:string|number;
}

let obj:obj={
    currentPage:1,
    pageSize:10,
    type:'tv',
    from:"电视剧"
}

let arr2:(string|number|Symbol)[]=[1,'2',Symbol(3)]


enum TYPE{
    a,
    b,
    c,
    d,
    e='awd',
    f=4,
    g
}

let str:unknown
(<string>str).length;
(str as string).length;

let num=1 as const;
num=2

interface O<T>{
    [p in T] : T[p]
}
let o:O<obj>={

}

// async function *a<number>(){
//     let a = await yield 1
// }

// interface obj2{
//     a:number;
//     [prop:number]:string
//     [prop:string]:Object
// }
// let objc:obj2&obj3={
//     a:1
// }


type A = 'qwj' | string
type A2 = 'qwj' & string


interface Bird{
    fly():void;
    layEggs():void
}

type keys = keyof Bird
let keys:keys='fly'
interface Fish{
    swim():void;
    layEggs():void
}
type A3 = Bird | Fish
type A4 = Bird & Fish

let A3:A3={
    fly(){},
    layEggs(){}
}

let A4:A4={
    fly(){},
    swim(){},
    layEggs(){}
}

type A5 = { age:number } | { age:never; [prop:string]:string} //这里相当于最后还产生一个 | { age:number|never; [prop:string]:string }
let A5:A5={
    age:123
}

type A6 = { age:number } | { age:string; name:string} //这里相当于最后还产生一个 | { age:number|string; name:string }
let A6:A6={
    age:1,
    name:'邱文军'
}


let str3:Uncapitalize<'Qwj'>='qwj'

type fn = (a:Object,b?:string)=>Array<number|string>;

let fn:fn=(a=1,b)=>{
    return []
}

function reset(...reset:Array<string>){
    console.log(reset);
}

reset('1')

function say(this:Window){
    console.log(this)
}

say.call(window)

