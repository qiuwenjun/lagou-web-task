// const P = new Promise(function(resolve,reject){
//     resolve(1)
// })

// console.log(P.then(res=>{
//     console.log(res)
//     return Promise.reject(2)
// }).finally(()=>{
//     console.log(23)
//     return Promise.reject(213)
// }).catch(e=>{
//     console.log(e)
// }));


// Promise.resolve(3).then(e=>{
//     console.log(e);
//     return 4
// }).then(e=>{
//     console.log(e);
//     return 5
// }).then(e=>{
//     console.log(e);
// })

const api:string[] = [
    "user",
    "shop",
    "order"
]

const content:{[prop:string]:string[]}={
    "user":[ "邱文军" ],
    "shop":[ "华为MATE40 PRO", "华为P40 PRO" ],
    "order":["2022/3/30","2022/4/5","2022/5/2"]
}
let nulls:null = null
let undefineds:undefined = undefined
// let objs:{ a : never } = { a : 2 }
let nsList:(number|string)[] = [1,'2',3,'4']
type Naked<T> = T[] extends string[] ? 1 : 2
type a = Naked<string>

function ajax<T>(data:T){
    return new Promise<T|string>((resolve,reject)=>{
        setTimeout(()=>{
            if(Math.random() < 0.5){
                resolve(data)
            }else{
                reject({
                    message:'请求失败',
                    err:data
                })
            }
        },Math.ceil(Math.random()*2500)+500)
        // let xml = new XMLHttpRequest()
        // xml.open('get',url)
        // xml.onload=function(){
        //     if(xml.status === 200){
        //         resolve(xml.response)
        //     }else{
        //         reject(xml.statusText)
        //     }
        // }
        // xml.send()
    })
}

ajax(api).then((e)=>{
   if(typeof e !== 'string'){
    //    return Promise.all(e.map(key=>ajax(content[key])))
    //    return Promise.race(e.map(key=>ajax(content[key])))
    return Promise.any(e.map(key=>ajax(content[key])))
    // return Promise.allSettled(e.map(key=>ajax(content[key])))
   }
}).then(data=>{
    console.log(data);
}).catch(e=>console.log(e))

let never1:never
let null1:null
let undefined1:undefined
let any1:any
let unknown1:unknown

unknown1=123
interface arr{
    len:number;
    str:string;
    lowercase:boolean
}

interface arr2{
    len:never;
    str:number;
}

let arr3:arr|arr2={
    len:1,
    str:'1',
    lowercase:true
}
class Array1 extends Array implements arr{
    len=1
    str='123'
    lowercase=false
}
// null1=undefined1
// undefined1=null1

interface l{
    age:number
}
interface l2{
    age:never
    [prop:string]:string
}

let l3:l|l2={
    age:1,
    a:'1'
}

