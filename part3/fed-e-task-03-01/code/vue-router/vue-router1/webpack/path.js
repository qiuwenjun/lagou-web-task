const Path = require('path');
const __dirPath = process.cwd();

module.exports = path => Path.resolve(__dirPath,path);