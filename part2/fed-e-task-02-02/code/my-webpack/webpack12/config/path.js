const Path = require('path')
const absoutPath = process.cwd()

module.exports = (dirPath) => {
  return Path.resolve(absoutPath, dirPath)
}
