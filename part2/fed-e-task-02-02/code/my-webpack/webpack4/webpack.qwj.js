const Path = require('path');
// const PostCssPresetEnv = require('postcss-preset-env')
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const { DefinePlugin } = require('webpack')
const CopyWebpackPlugin = require('copy-webpack-plugin')

module.exports = {
    devtool:'none',
    mode:"development",
    entry:'./src/index.js',
    output:{
        filename:'js/[name].[hash:6].js',
        path:Path.resolve(__dirname,'dist'),
        // publicPath:'',        //默认值 空字符串   打包后静态资源(dist,css,img)在服务器的存放位置
        // publicPath:'/',       //绝对路径
        // publicPath:'./'       //相对路径
    },
    devServer:{
        hot:'only',
        compress:true,  //开启gzip压缩
        client:{
            webSocketURL:{
                port:9999
            }
        },
        open:false,
        historyApiFallback:true
    },
    module:{
        rules:[
            {
                test:/\.css$/,
                use:[
                    "style-loader",
                    {
                        loader:"css-loader",
                        options:{
                            esModule:false,
                            importLoaders:1
                        }
                    },
                    'postcss-loader',
                    // {
                    //     loader:"postcss-loader",
                    //     options:{
                    //         postcssOptions:{
                    //             plugins:[
                    //                 PostCssPresetEnv()
                    //             ]
                    //         }
                    //     }
                    // }
                ]
            },
            {
                test:/\.less$/,
                use:[
                    "style-loader",
                    {
                        loader:"css-loader",
                        options:{
                            esModule:false,
                            importLoaders:1
                        }
                    },
                    'postcss-loader',
                    // {
                    //     loader:"postcss-loader",
                    //     options:{
                    //         postcssOptions:{
                    //             plugins:[
                    //                 require('postcss-preset-env')
                    //             ]
                    //         }
                    //     }
                    // },
                    "less-loader"
                ]
            },
            {
                test:/\.js$/,
                exclude:/node_modules/,
                use:[
                    'babel-loader'
                    // {
                    //     loader:'babel-loader',
                    //     options:{
                    //         presets:[
                    //             [
                    //                 "@babel/preset-env",
                    //                 {
                    //                     "useBuiltIns":"usage",
                    //                     "corejs":3
                    //                 }
                    //             ]
                    //         ]
                    //     }
                    // }
                ]
            },
            {
                test:/\.(jpe?g|png|gif|svg)$/,
                type:'asset',
                generator:{
                    filename:"./img/[name].[hash:6][ext]"
                },
                parser:{
                    dataUrlCondition:{
                        maxSize:1000*1024
                    }
                }
            },
            {
                test:/\.(ttf|woff2?)$/,
                type:'asset/resource',
                generator:{
                    filename:"./font/[name].[hash:6][ext]"
                },
            }
        ]
    },
    plugins:[
        new CleanWebpackPlugin(),
        new HtmlWebpackPlugin({
            title:'邱文军-webpack-学习',
            template:'./public/index.html'
        }),
        new DefinePlugin({
            "BASE_URL" : JSON.stringify('./')
        }),
        new CopyWebpackPlugin({
            "patterns":[
                {
                    from:"./public",
                    globOptions:{
                        ignore:['**/index.html']
                    }
                }
            ]
        })
    ]
}