## 一、简答题

### 1、当我们点击按钮的时候动态给 data 增加的成员是否是响应式数据，如果不是的话，如何把新增成员设置成响应式数据，它的内部原理是什么。

```js
let vm = new Vue({
 el: '#el'
 data: {
  o: 'object',
  dog: {}
 },
 method: {
  clickHandler () {
   // 该 name 属性是否是响应式的
   this.dog.name = 'Trump'
  }
 }
})
```

 答:动态给data增加的成员不是响应式数据,可以使用的vue.set或vm.$set方法给对象上动态添加响应式属性，而$set和set方法无非就是调用Observer这个数据监听方法的defineReactive方法
**vue**

```tsx
/** person information */
interface iProps{
    [prop:string]:Function|{type:Function,default?:()=>any|any,required?:Boolean,validate?:Function}
}
interface Options{
    props?:iProps|Array<string>
    data?:()=>({[prop:string]:any}) | {[prop:string]:any},
    el:string | HTMLElement
    methods:{[prop:string]:Function}
}
class Vue{
    $el:HTMLElement
    $data:{[prop:string]:any}
    _Observer:Observer
    constructor(public $options:Options){
        this.$el = typeof $options.el === 'string' ? document.querySelector($options.el) as HTMLElement : $options.el; 
        this.$data = typeof $options.data === 'function' ? $options.data() : typeof $options.data === 'object' ? $options.data : {}
        this._proxyData(this.$data)
        this._methods()
        this._Observer = new Observer(this.$data)
        new Compiler(this)
    }
    _proxyData(data:typeof this.$data){
        for(let attr in data){
            Object.defineProperty(this,attr,{
                enumerable:true,
                configurable:true,
                get(){
                    return data[attr]
                },
                set(val){
                    if(val === data[attr]) return;
                    data[attr] = val
                }
            })
        }
    }
    _methods(){
        for(let attr in this.$options.methods){
            this[attr] = this.$options.methods[attr]
        }
    }
    static set(target,key,value){            //$set
        return Observer.prototype.defineReactive(target,key,value)
    }
    $set(target,key,value){        //$set
        return this._Observer.defineReactive(target,key,value)
    }
}
```

**Observer**

```tsx
class Observer{
    constructor(public data:any){
        this.walk(data)
    }
    walk(data:any){
        if(!data || typeof data !== 'object') return
        for(let attr in data){
            this.defineReactive(data,attr,data[attr])
        }
    }
    defineReactive(obj:object,attr:string,val:any){
        this.walk(val); //如果val是对象，把val内部的属性转换成响应式数据
        const dep = new Dep(); //收集依赖发送通知
        Object.defineProperty(obj,attr,{
            enumerable:true,
            configurable:true,
            get:()=>{
                Dep.target && dep.addSub(Dep.target)
                return val    //不能是obj[attr],这里会发生死递归
            },
            set:(newVal)=>{
                if(newVal === val) return
                val = newVal
                dep.notify()
                this.walk(newVal)
            }
        })
    }
}
```

### 2、请简述 Diff 算法的执行过程

1. 通过init方法初始化模块和 DOM 操作的 api，注册一个patch方法，最终返回 patch，这里的 init 是一个高阶函数，在 init 函数内部缓存了两个参数，在返回的 patch 函数中可以通过闭包访问到 init 中初始化的模块和 DOM 操作的 api

2. patch 函数接收新旧两个 VNode 作为参数，其中第一个参数即可以是 DOM 对象又可以是 VNode 对象，如果第一个参数是 DOM 对象的话，patch 内部会先把 DOM 对象转换成 VNode，再和第二个参数中的新的 VNode 对比，然后更新 DOM 

3. patchVnode函数，更新节点内容或属性，如果这个节点下面有子节点，那这时候就递归掉用updateChildren方法来比较新旧节点的差异，并更新

4. updateChildren 函数的作用是对比新旧 VNode 节点的子节点，在对比的过程中因为 DOM 操作的特殊性，同时也为了优化操作，所以只对比两颗树中的同一层级的子节点。
   
   有下列几种对比方式
   
   - （旧开始节点和新开始节点|旧结束节点和新结束节点）通过sameVnode，如果两个vnode的sel和key相同，调用patchVnode对比和更新节点，更新索引值
   - 旧开始节点和新结束节点，通过sameVnode，如果两个vnode的sel和key相同，调用patchVnode对比和更新节点，这时候如果旧开始节点和新结束节点相同，那这时候就会把旧开始节点移动到最右边，然后对应的索引值变化
   - 旧结束节点和新开始节点, 通过sameVnode，如果两个vnode的sel和key相同，调用patchVnode对比和更新节点，这时候如果旧结束节点和新开始节点相同，那这时候就会把旧结束节点移动到最左边，然后对应的索引值变化
   - 当旧开始节点的索引值大于旧结束节点的，说明这时候新节点有剩余，要把新的剩余节点插入到最右边
   - 当新开始节点的索引值大于新结束节点的，说明老节点有剩余，把剩余节点批量删除

　

## 二、编程题

### 1、模拟 VueRouter 的 hash 模式的实现，实现思路和 History 模式类似，把 URL 中的 # 后面的内容作为路由的地址，可以通过 hashchange 事件监听路由地址的变化。

创建 VueRouter 插件，静态方法 install

- 判断插件是否已经被加载
- 当 Vue 加载的时候把传入的 router 对象挂载到 Vue 实例上（注意：只执行一次）

创建VueRouter 类

- 初始化，options、routeMap、app(简化操作，创建 Vue 实例作为响应式数据记录当前路径)
- initRouteMap() 遍历所有路由信息，把组件和路由的映射记录到 routeMap 对象中
- 注册 hashchange事件，当路由地址发生变化，重新记录当前的路径
- 创建 router-link 和 router-view 组件
- 当路径改变的时候通过当前路径在 routerMap 对象中找到对应的组件，渲染 router-view

```tsx
import Vue, { AsyncComponent } from 'vue'

interface Path{
    path:string
    name?:string
    component:AsyncComponent,
    props?:boolean|Object|(()=>Object)
    alias?:string
    redirect?:string|Object|(()=>Object)
    children?:Path[]
    beforeEnter?:Function
    meta?:any,
}

interface iOpt{
    mode:'history'|'hash'
    routes:Path[]
    scrollBehavior?:Function
}

const Parameter = (data:object|string)=>{
    return typeof data === 'object' ? Object.entries(data).reduce((prev,item)=>`${prev}${prev === '?' ? '' : '&'}${item[0]}=${item[1]}`,'?') : data.replace(/^\?/,'').split('&').reduce((prev,item)=>{
        let value = item.split('=')
        prev[value[0]] = value[1]
        return prev
    },{});
}

let _Vue;
export default class VueRouter{
    routeMap:{[prop:string]:string}
    data:{[prop:string]:string}
    constructor(public options:iOpt){
        if(!/history|hash/.test(options.mode)){
            throw new TypeError("路由类型错误!!!")
        }
        this.routeMap = {}
        this.data = _Vue.observable({current:this.path()})
    }
    path(){
        if(this.options.mode === 'history'){
            return location.pathname
        }else if(this.options.mode === 'hash'){
            return location.hash.replace('#','')
        }
    }
    init(){
        this.createRouteMap()
        this.initComponents()
        this.initEvent()
    }
    initEvent(){
        if(this.options.mode === 'history'){
            window.addEventListener('popstate',({state})=>{
                this.data.current = state
            },false)
        }else if(this.options.mode === 'hash'){
            window.addEventListener('hashchange',({newURL})=>{
                this.data.current = new URL(newURL).hash.replace('#','')
            },false)
        }
    }
    createRouteMap(){               //创建路由和组件的映射关系    
        this.routeMap = this.options.routes.reduce((prev,item)=>{
            prev[item.path] = item.component
            return prev
        },{})
    }
    initComponents(){               //初始化组件
        const This = this
        _Vue.component('router-link',{
            props:{
                to:{
                    type:[Object,String]
                },
                tag:{
                    type:String,
                    default:'a'
                }
            },
            render(h) {
                const configObj = [this.tag]
                if(this.tag === 'a'){
                    configObj.push({
                        attrs:{
                            href:typeof this.to === 'string' ? this.to : this.to.path + Parameter(this.to.query)
                        },
                        on: {
                            click:(e)=>{
                                e.preventDefault()
                                this.$router.push(typeof this.to === 'string' ? this.to : this.to.path + Parameter(this.to.query))
                            }
                        },
                    })
                }else{
                    configObj.push({
                        on: {
                            click:()=>{
                                this.$router.push(typeof this.to === 'string' ? this.to : this.to.path + Parameter(this.to.query))
                            }
                        },
                    })
                }
                configObj.push(this.$slots.default)
                return h(...configObj)
            },
        });
        _Vue.component('router-view',{
            render(h) {
                const Component = This.routeMap[This.data.current]
                return h(Component)
            },
        });
    }
    push(url){
        if(this.options.mode === 'history'){
            history.pushState(url,'',url)
        }else if(this.options.mode === 'hash'){
            location.hash='#'+url
        }
        this.data.current = url
    }
    static isInstall:Boolean        //判断是否注册过
    static install(Vue:Vue){
        //首先判断是否注册过
        if(VueRouter.isInstall) return;
        VueRouter.isInstall = true;
        _Vue = Vue;
        _Vue.mixin({
            beforeCreate() {
                if(_Vue.prototype.$router instanceof VueRouter) return;
                _Vue.prototype.$router = this.$options.router
                this.$router.init()
            },
        })
    }
}
```

　

### 2、在模拟 Vue.js 响应式源码的基础上实现 v-html 指令，以及 v-on 指令。

**vue.js**

```tsx
class Vue{
    $el:HTMLElement
    $data:{[prop:string]:any}
    constructor(public $options:Options){
        this.$el = typeof $options.el === 'string' ? document.querySelector($options.el) as HTMLElement : $options.el; 
        this.$data = typeof $options.data === 'function' ? $options.data() : typeof $options.data === 'object' ? $options.data : {}
        this._proxyData(this.$data)
        this._methods()
        new Observer(this.$data)
        new Compiler(this)
    }
    _proxyData(data:typeof this.$data){
        for(let attr in data){
            Object.defineProperty(this,attr,{
                enumerable:true,
                configurable:true,
                get(){
                    return data[attr]
                },
                set(val){
                    if(val === data[attr]) return;
                    data[attr] = val
                }
            })
        }
    }
    _methods(){
        for(let attr in this.$options.methods){
            this[attr] = this.$options.methods[attr]
        }
    }
    static 
}
```

**compiler.js**

```tsx
class Compiler{
    vm:Vue
    el:HTMLElement
    constructor(vm:Vue){
        this.el = vm.$el
        this.vm = vm
        this.compile(this.el)
    }

    //编译方法Compiler
    compile(el:Node){
        el.childNodes.forEach((item:Node)=>{
            if(this.isTextNode(item)){    //文本节点
                this.compileText(item)
            }else if(this.isElementNode(item)){    //元素节点
                this.compileElement(item as HTMLElement)
            }
            if(item.childNodes && item.childNodes.length){
                this.compile(item)
            }
        })
    }

    //编译指令中的内容
    compileElement(node:HTMLElement){
        const attributes = node.attributes;
        [...(attributes as unknown as Attr[])].forEach(item=>{
           if(item.nodeName.startsWith('v-') || item.nodeName.startsWith('@')){
                let name:string|null = null;
                let attrName:string|null = null;
                if(item.nodeName.startsWith('@')){
                    name = item.nodeName.replace(/^@(.+?)$/,'$1')
                    attrName='on'
                }else{
                    name = item.nodeName.substr(2);
                    if(name.startsWith('on:')){
                        name = name.replace(/^on:(.+?)$/,'$1')
                        attrName='on'
                    }else{
                        return this[name+'Updater'](node,item.textContent)
                    }
                }
                this[attrName+'Updater'](node,name,item.textContent)
           }
        })
    }

    //v-text指令
    textUpdater(node:Node,key:any){
        node.textContent = this.vm[key]
        new Watcher(this.vm,key,value=>{
            node.textContent = value
        })
    }

    //v-html指令
    htmlUpdater(node:HTMLElement,key:any){
        node.innerHTML = this.vm[key]
        new Watcher(this.vm,key,value=>{
            node.innerHTML = value
        })
    }

    //v-on指令
    onUpdater(node:HTMLElement,eventName:string,key:any){
        console.log(arguments);
        let newFn = this.vm[key]
        if(!newFn){     //如果这里写的不是函数名称，而是函数的处理逻辑
            newFn=new Function('$event',key)
        }
        console.log(newFn);
        node.addEventListener(eventName,newFn,false)
    }

    //v-model指令
    modelUpdater(node,key){
        node.value = this.vm[key]
        new Watcher(this.vm,key,value=>{
            node.value = value
        })
        node.addEventListener('input',()=>{
            this.vm[key] = node.value
        },false)
    }

    //编译差值表达式中的内容
    compileText(node){
       const reg = /\{\{(.+?)\}\}/;
       const value = node.textContent
       if(reg.test(value)){
         const variable = RegExp.$1;
         node.textContent = value.replace(reg,this.vm[variable])
         new Watcher(this.vm,variable,newVal=>{
            node.textContent = value.replace(reg,newVal)
        })
       }
    }

    //判断属性是否是指令
    isDirective(node){
        return node.nodeType === 2 && node.nodeName.startsWith('v-')
    }
    //判断是否是文本节点
    isTextNode(node){
        return node.nodeType === 3
    }
    //判断是否是元素节点
    isElementNode(node){
        return node.nodeType === 1
    }
}
```
