"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var Hook_1 = require("./Hook");
var HookFactory_1 = require("./HookFactory");
require("./common");
var SyncFactory = /** @class */ (function (_super) {
    __extends(SyncFactory, _super);
    function SyncFactory() {
        var _this = _super.call(this) || this;
        _this.type = 'SyncFactory';
        return _this;
    }
    SyncFactory.prototype.setup = function (instance, options) {
        this.options = options;
        instance._x = options.taps.map(function (item) { return item.fn; });
    };
    SyncFactory.prototype.create = function (options) {
        return new Function(this.args(), this.head() + this.content());
    };
    return SyncFactory;
}(HookFactory_1["default"]));
var factory = new SyncFactory();
var SyncHook = /** @class */ (function (_super) {
    __extends(SyncHook, _super);
    function SyncHook(args) {
        return _super.call(this, args) || this;
    }
    SyncHook.prototype.compile = function () {
        factory.setup(this, {
            taps: this.taps,
            args: this.args
        });
        return factory.create({
            taps: this.taps,
            args: this.args
        });
    };
    return SyncHook;
}(Hook_1["default"]));
exports["default"] = SyncHook;
