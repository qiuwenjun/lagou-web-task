import Vue from 'vue'
import VueRouter from "../myRouter/index";
import vRouter from "vue-router";
console.dir(vRouter);
Vue.use(VueRouter)

export default new VueRouter({
    mode:'hash',
    routes:[
        {
            path:'/',
            name:'home',
            component:({
                render(h){
                    return h('div',{
                        class:"home"
                    },'home页面')
                }
            })
        },
        {
            path:"/detail",
            name:"detail",
            props:true,
            /**
             * 开启props，会把url中的参数传递给组件，组件通过props接受，
             * 当值为true时params参数传递给组件，
             * 为对象时，对象上的数据传递给组件，
             * 为函数时，函数的返回值传递给组件
            */
            component:({
                render(h){
                    return h('div',{
                        class:"detail"
                    },[
                        h('p','这是详情页')
                    ])
                }
            })
        }
    ]
})