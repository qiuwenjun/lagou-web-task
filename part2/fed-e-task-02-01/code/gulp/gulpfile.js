const gulp = require('gulp')
const fs = require('fs');
const { Transform } = require('stream');

exports.start=function(done){
    console.log('start');
    done()
}

exports.error=function(done){
    console.log('error');
    done(new Error('失败了!'))
}

exports.src = function(done){
    return gulp.src('./src/*.html').pipe(gulp.dest('./dist/'))
}

exports.setTimeout = (done)=>{
    setTimeout(()=>{
        console.log('哈哈哈');
        done()
    },3000)
}

exports.async = async ()=>{
    await new Promise((resolve,reject)=>{
        setTimeout(resolve,3000)
    })
    console.log('async await');
}

exports.pResolve = () => {
    console.log("promise resolve");
    return Promise.resolve(1)
}

exports.pReject = () => {
    console.log("promise reject");
    return Promise.reject(1)
}


//读取文件流，写入文件流
exports.stream = done => {
    const readStream = fs.createReadStream('package.json')
    const writeStream = fs.createWriteStream('package.txt')
    readStream.pipe(writeStream)
    // readStream.on('end',done)
    return readStream
}

//压缩文件
exports.streamSystem = done => {
    const read = fs.createReadStream('normalize.css')
    const wrtie = fs.createWriteStream('normalize.min.css')
    const transform = new Transform({
        transform:(chunk,encoding,callback)=>{
            const input = chunk.toString();
            const output = input.replace(/\s+/g,'').replace(/\/\*.+?\*\//g,'')
            callback(null,output)
        }
    })
    read.pipe(transform).pipe(wrtie)
    return read
}