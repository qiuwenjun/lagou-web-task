module.exports = class MkPlugin {
  apply (compiler) {
    // console.log(compiler.hooks);
    compiler.hooks.emit.tap('MkPlugin', function (Compilation) {
      for (const name in Compilation.assets) {
        console.log(/^js\/home\.[a-z0-9]+\.js/i.test(name)) // 文件名
        if (/^js\/home\.[a-z0-9]+\.js/i.test(name)) {
          const contents = Compilation.assets[name].source()
          const withoutComments = contents.match(/\/\/mk-loader开始([^]*)\/\/mk-loader结束/)[0]
          // console.log(contents);
          // console.log(withoutComments);
          Compilation.hooks.processAssets[name] = {
            source: () => withoutComments,
            size: () => withoutComments.length
          }
          break
        }
      }
    })
  }
}
