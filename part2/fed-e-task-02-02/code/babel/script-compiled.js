"use strict";

var title = [1, 2, 3];

var transform = function transform() {
  return title.map(function (item) {
    return Math.pow(item, item);
  });
};

transform();
