/*
  将下面异步代码使用 Promise 的方法改进
  尽量用看上去像同步代码的方式
  setTimeout(function () {
    var a = 'hello'
    setTimeout(function () {
      var b = 'lagou'
      setTimeout(function () {
        var c = 'I ♥ U'
        console.log(a + b +c)
      }, 10)
    }, 10)
  }, 10)
*/
//Promise
new Promise(res=>{
  setTimeout(res,10,['hello'])
}).then((reset)=>{
  return new Promise(res=>{
    reset.push('lagou')
    setTimeout(res,10,reset)
  })
}).then((reset)=>{
  return new Promise(res=>{
    reset.push('I ♥ U')
    setTimeout(res,10,reset)
  })
}).then((reset)=>{
  console.log(reset.reduce((prev,item)=>prev+' '+item));
})

//generator
function *generatorData(){
  const a = yield new Promise(res=>{
    setTimeout(res,10,'hello')
  })
  const b = yield new Promise(res=>{
    setTimeout(res,10,'lagou')
  })
  const c = yield new Promise(res=>{
    setTimeout(res,10,'I ♥ U')
  })
  return `${a} ${b} ${c}`
}

function callGenerator(generatorObj){
  let f = generatorObj()
  function g(result){
    if(result.done) return result.value
    return result.value.then((data)=>{
      return g(f.next(data))
    })
  }
  return g(f.next())
}

callGenerator(generatorData)


//async await
async function asyncData(){
  const a = await new Promise(res=>{
    setTimeout(res,10,'hello')
  })
  const b = await new Promise(res=>{
    setTimeout(res,10,'lagou')
  })
  const c = await new Promise(res=>{
    setTimeout(res,10,'I ♥ U')
  })
  return `${a} ${b} ${c}`
}
// console.log(await asyncData());
asyncData().then(result=>{
  console.log(result)
})

//async generator
async function *asyncGeneratorData(){
  const a = yield await new Promise(res=>{
    setTimeout(res,10,'hello')
  })
  const b = yield await new Promise(res=>{
    setTimeout(res,10,'lagou')
  })
  const c = yield await new Promise(res=>{
    setTimeout(res,10,'I ♥ U')
  })

  return `${a} ${b} ${c}`
}

let data = asyncGeneratorData()
// for await(let item of data){
//   console.log(item)
// }
data.next()
.then(result=>data.next(result.value))
.then(result=>data.next(result.value))
.then(result=>data.next(result.value))
.then(result=>{
  console.log(result);
})