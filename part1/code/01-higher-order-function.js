//高阶函数-函数作为参数

function forEach(data,fn,This){

    for(let i=0; i < data.length; i++){

        fn.call(This,data[i],i,data)

    }

}


function filter(data,fn,This){

    let result = []
    for(let i=0; i < data.length; i++){

        if(fn.call(This,data[i],i,data)){
            result.push(data[i])
        }

    }
    return result

}

let arr = [ 1, 2, 3, 4, 5, 6 ]
// forEach(arr,function(item,index,data){
//     console.log(arguments,this);
// })
console.log(filter(arr,function(item,index,data){
    return item % 2 === 0
}));