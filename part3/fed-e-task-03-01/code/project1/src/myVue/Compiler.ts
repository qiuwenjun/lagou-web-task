class Compiler{
    vm:Vue
    el:HTMLElement
    constructor(vm:Vue){
        this.el = vm.$el
        this.vm = vm
        this.compile(this.el)
    }

    //编译方法Compiler
    compile(el:Node){
        el.childNodes.forEach((item:Node)=>{
            if(this.isTextNode(item)){    //文本节点
                this.compileText(item)
            }else if(this.isElementNode(item)){    //元素节点
                this.compileElement(item as HTMLElement)
            }
            if(item.childNodes && item.childNodes.length){
                this.compile(item)
            }
        })
    }

    //编译指令中的内容
    compileElement(node:HTMLElement){
        const attributes = node.attributes;
        [...(attributes as unknown as Attr[])].forEach(item=>{
           if(item.nodeName.startsWith('v-') || item.nodeName.startsWith('@')){
                let name:string|null = null;
                let attrName:string|null = null;
                if(item.nodeName.startsWith('@')){          //处理函数
                    name = item.nodeName.replace(/^@(.+?)$/,'$1')
                    attrName='on'
                }else{
                    name = item.nodeName.substr(2);
                    if(name.startsWith('on:')){     //处理函数
                        name = name.replace(/^on:(.+?)$/,'$1')
                        attrName='on'
                    }else{
                        return this[name+'Updater'](node,item.textContent)
                    }
                }
                this[attrName+'Updater'](node,name,item.textContent)
           }
        })
    }

    //v-text指令
    textUpdater(node:Node,key:any){
        node.textContent = this.vm[key]
        new Watcher(this.vm,key,value=>{
            node.textContent = value
        })
    }

    //v-html指令
    htmlUpdater(node:HTMLElement,key:any){
        node.innerHTML = this.vm[key]
        new Watcher(this.vm,key,value=>{
            node.innerHTML = value
        })
    }

    //v-on指令
    onUpdater(node:HTMLElement,eventName:string,key:any){
        let newFn = this.vm[key]
        if(!newFn){     //如果这里写的不是函数名称，而是函数的处理逻辑
            newFn=new Function('$event',key)
        }
        node.addEventListener(eventName,newFn,false)
    }

    //v-model指令
    modelUpdater(node,key){
        node.value = this.vm[key]
        new Watcher(this.vm,key,value=>{
            node.value = value
        })
        node.addEventListener('input',()=>{
            this.vm[key] = node.value
        },false)
    }

    //编译差值表达式中的内容
    compileText(node){
       const reg = /\{\{(.+?)\}\}/;
       const value = node.textContent
       if(reg.test(value)){
         const variable = RegExp.$1;
         node.textContent = value.replace(reg,this.vm[variable])
         new Watcher(this.vm,variable,newVal=>{
            node.textContent = value.replace(reg,newVal)
        })
       }
    }

    //判断属性是否是指令
    isDirective(node){
        return node.nodeType === 2 && node.nodeName.startsWith('v-')
    }
    //判断是否是文本节点
    isTextNode(node){
        return node.nodeType === 3
    }
    //判断是否是元素节点
    isElementNode(node){
        return node.nodeType === 1
    }
}