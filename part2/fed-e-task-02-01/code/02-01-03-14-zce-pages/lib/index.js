const { src, dest, series, parallel, watch } = require('gulp')
// const gulpLoad = require('gulp-load-plugins')
// const plugins = gulpLoad();
const plugins = {}
plugins.sass = require('gulp-sass')(require('sass'))
plugins.css = require('gulp-css')
plugins.cssmin=require('gulp-cssmin')
plugins.babel = require('gulp-babel')
plugins.swig = require('gulp-swig')
plugins.clean = require('gulp-clean')
plugins.imagemin = require('gulp-imagemin')
plugins.debug = require('gulp-debug')
plugins.changed=require('gulp-changed')
plugins.useref = require('gulp-useref')
plugins.htmlmin = require('gulp-htmlmin')
plugins.cssmin = require('gulp-cssmin')
plugins.uglify = require('gulp-uglify')
plugins.if = require('gulp-if')
const del = require('del')
const bs = require('browser-sync').create()
const defaultData = {
  "base":{
    "base":'src',
    "dist":"dist",
    "temp":"temp",
    "public":"public",
    "entry":{
      "scss":'./src/assets/styles/*.scss',
      "js":'./src/assets/scripts/*.js',
      "html":'./src/*.html',
      "image":'./src/assets/images/**',
      "font":'./src/assets/images/**',
      "public":'./public/**'
    },
    "output":{
      "public":'./dist/public',
      "temp":'./temp/*.html'
    }
  }
}
const projectPath = process.cwd();    //获取项目路径

try{
  const projectData = require(`${projectPath}/gulp.config.js`)
  defaultData = {...defaultData,...projectData}
}catch(e){

}

const uDel = (tempList = [defaultData.base.dist,defaultData.base.temp]) => {
  tempList = Array.isArray(tempList) ? tempList : [tempList]
  return ((done) => {
    del.sync(tempList)
    done()
  })
}

const uCss = () => {
  return src(defaultData.base.entry.scss,{ base : defaultData.base.base }).pipe(plugins.sass({outputStyle:'expanded'})).pipe(plugins.css()).pipe(plugins.cssmin()).pipe(dest(defaultData.base.temp)).pipe(bs.reload({ stream: true }))
}

const uJs = () => {
  return src(defaultData.base.entry.js,{base:defaultData.base.base}).pipe(plugins.babel({
      "presets":["@babel/preset-env"],
      "plugins": [
        "@babel/plugin-transform-runtime",
      ]
  })).pipe(dest(defaultData.base.temp)).pipe(bs.reload({ stream: true }))
}

const uHtml = () => {
  return src(defaultData.base.entry.html).pipe(plugins.swig({ 
    data:defaultData.data,
    defaults: {
      cache: false
    }
  })).pipe(dest(defaultData.base.temp)).pipe(bs.reload({ stream: true }))
}

const uImage = () => {
  return src(defaultData.base.entry.image,{base:defaultData.base.base}).pipe(plugins.imagemin()).pipe(dest(defaultData.base.dist))
}

const uFont = () => {
  return src(defaultData.base.entry.font,{base:defaultData.base.base}).pipe(plugins.imagemin()).pipe(dest(defaultData.base.dist))
}

const public = () => {
  return src(defaultData.base.public).pipe(dest(defaultData.base.output.public))
}

const useref = () =>{
  return src(defaultData.base.output.temp,{base:defaultData.base.temp})
  .pipe(plugins.useref({searchPath:[defaultData.base.temp,'.']}))
  .pipe(plugins.if(/\.js$/,plugins.uglify()))
  .pipe(plugins.if(/\.css$/,plugins.cssmin()))
  .pipe(plugins.if(/\.html$/,plugins.htmlmin({
    collapseWhitespace: true,
    minifyCSS: true,
    minifyJS: true
  })))
  .pipe(dest(defaultData.base.dist))
}

const serve = () => {
  watch(defaultData.base.entry.html,uHtml)
  watch(defaultData.base.entry.js,uJs)
  watch(defaultData.base.entry.scss,uCss)
  // watch('public/**',public)
  // watch('src/assets/images/**',uImage)
  // watch('src/assets/fonts/**',uFont)
  watch([defaultData.base.entry.public,defaultData.base.entry.image,defaultData.base.entry.font],()=>{
    bs.reload()
  })

  bs.init({
    notify:false,
    // files:'./dist/**',
    port:9999,
    open:false,
    // watch:true,
    server:{
      baseDir:[defaultData.base.temp, defaultData.base.base, defaultData.base.public],
      routes:{
        '/node_modules':'node_modules'
      }
    },
  })
}

const base = series(uDel(), parallel(uCss, uJs, uHtml))

const server = series(base, serve)

const build = series(base, parallel(useref, uImage, uFont, public),uDel('temp'))

module.exports = {
  clear:uDel,
  build,
  server
}