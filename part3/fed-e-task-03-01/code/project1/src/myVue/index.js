var p = {
    name: '邱文军',
    age: 24,
    sex: '男'
};
var Vue = /** @class */ (function () {
    function Vue($options) {
        this.$options = $options;
        this.$el = typeof $options.el === 'string' ? document.querySelector($options.el) : $options.el;
        this.$data = typeof $options.data === 'function' ? $options.data() : typeof $options.data === 'object' ? $options.data : {};
        this._proxyData(this.$data);
        this._methods();
        this._Observer = new Observer(this.$data);
        new Compiler(this);
    }
    Vue.prototype._proxyData = function (data) {
        var _loop_1 = function (attr) {
            Object.defineProperty(this_1, attr, {
                enumerable: true,
                configurable: true,
                get: function () {
                    return data[attr];
                },
                set: function (val) {
                    if (val === data[attr])
                        return;
                    data[attr] = val;
                }
            });
        };
        var this_1 = this;
        for (var attr in data) {
            _loop_1(attr);
        }
    };
    Vue.prototype._methods = function () {
        for (var attr in this.$options.methods) {
            this[attr] = this.$options.methods[attr];
        }
    };
    Vue.set = function (target, key, value) {
        return Observer.prototype.defineReactive(target, key, value);
    };
    Vue.prototype.$set = function (target, key, value) {
        return this._Observer.defineReactive(target, key, value);
    };
    return Vue;
}());
