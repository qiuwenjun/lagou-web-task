/*
尽可能还原 Promise 中的每一个 API, 并通过注释的方式描述思路和原理.
*/
class Promise{
    #PromiseState = 'pending'   //存储状态
    #PromiseResult = undefined  //存储返回值
    #observeHandler = null      //监听回调
    #div = null                 //用来监听的元素
    #observe = null             //存储监听实例
    #resolve = null             //成功回调
    #reject = null              //失败回调
    constructor(callback){
        if(typeof callback !== 'function'){
            throw new TypeError(arguments[0]+'不是一个函数')
        }

        try{
            const resolve = result => {                 //resolve回调
                if(this.#PromiseState !== 'pending') return     //Promise状态已经更改，就直接return
                this.#observeHandler = ()=>{
                    if(typeof this.#resolve === 'function') this.#resolve(result)           //如果有处理回调才执行
                }
                this.#PromiseState = "fulfilled"
                this.#PromiseResult = result
                this.updatePromiseState()
            }
            const reject = result =>{                   //reject回调
                if(this.#PromiseState !== 'pending') return     //Promise状态已经更改，就直接return
                this.#observeHandler = ()=>{
                    if(typeof this.#reject === 'function') this.#reject(result)             //如果有处理回调才执行
                }
                this.#PromiseState = "rejected"
                this.#PromiseResult = result
                this.updatePromiseState()
            }

            this.microTask()           //开启监听
            callback(resolve,reject)

        }catch(e){
            reject(e)
        }
    }
    then(success,failure){                      //then方法，success成功的回调，failure失败的回调
        if(this.isNothing()) this.updatePromiseState()
        return new Promise((resolve,reject)=>{
            this.#resolve = result => {
                this.#resolve = null
                if(typeof success === 'function'){              //如果success有传入回调，就执行回调
                    let value = success(result)
                    if(toString.call(value) === '[object Promise]'){            //回调返回Promise
                        new Promise((res,rej)=>{            //如果返回的是Promise，那就需要包装一层
                            value.then(res).catch(rej)
                        }).then(resolve).catch(reject)
                    }else{                      
                        resolve(value)
                    }
                }else{                          //否则下一个Promise继承上一个Promise的状态
                    resolve(result)
                }
            }

            this.#reject = result=> {
                this.#reject = null
                if(typeof failure === 'function'){              //如果failure有传入回调，就执行回调
                    let value = failure(result)
                    if(toString.call(value) === '[object Promise]'){            //回调返回Promise
                        new Promise((res,rej)=>{            //如果返回的是Promise，那就需要包装一层
                            value.then(res).catch(rej)
                        }).then(resolve).catch(reject)
                    }else{
                        resolve(value)
                    }
                }else{
                    reject(result)
                }
            }
        })
    }
    catch(failure){
        if(this.isNothing()) this.updatePromiseState()
        return new Promise((resolve,reject)=>{
            this.#resolve = result => {
                this.#resolve = null
                resolve(result)         //catch不处理成功状态的Promise，所以直接继承上一个promise的状态
            }

            this.#reject = result=> {
                this.#reject = null
                if(typeof failure === 'function'){              //如果failure有传入回调，就执行回调
                    let value = failure(result)
                    if(toString.call(value) === '[object Promise]'){            //回调返回Promise
                        new Promise((res,rej)=>{            //如果返回的是Promise，那就需要包装一层
                            value.then(res).catch(rej)
                        }).then(resolve).catch(reject)
                    }else{
                        resolve(value)
                    }
                }else{
                    reject(result)
                }
            }
        })
    }
    finally(handler){
        if(this.isNothing()) this.updatePromiseState()
        return new Promise((resolve,reject)=>{
            this.#resolve = result => {
                this.#resolve = null
                if(typeof handler === 'function'){              //如果handler有传入回调，就执行回调
                    let value = handler()
                    if(toString.call(value) === '[object Promise]'){            //回调返回Promise
                        new Promise((res,rej)=>{            //如果返回的是Promise，那就需要包装一层
                            value.then(res).catch(rej)
                        }).then(()=>{
                            resolve(result)
                        }).catch(rej=>{
                            reject(rej)
                        })
                    }else{                      
                        resolve(result)
                    }
                }else{                          //否则下一个Promise继承上一个Promise的状态
                    resolve(result)
                }
            }

            this.#reject = result=> {
                this.#reject = null
                if(typeof handler === 'function'){              //如果handler有传入回调，就执行回调
                    let value = handler()
                    if(toString.call(value) === '[object Promise]'){            //回调返回Promise
                        new Promise((res,rej)=>{            //如果返回的是Promise，那就需要包装一层
                            value.then(res).catch(rej)
                        }).then(()=>{
                            resolve(result)
                        }).catch(rej=>{
                            reject(rej)
                        })
                    }else{
                        reject(result)
                    }
                }else{
                    reject(result)
                }
            }
        })
    }   
    isNothing(){
        return this.#PromiseState !== 'pending' && this.#resolve === null && this.#reject === null
    }
    get [Symbol.toStringTag](){             //判断Promise类型
        return 'Promise'
    }
    get updatePromiseState(){               //更新状态，往新微任务
        return ()=>{
            this.#div.className = `${Math.random()*100}`
        }
    }
    get microTask(){                //利用MutationObserver向微任务队列添加回调
        return ()=>{
            this.#div = document.createElement('div')
            this.updatePromiseState()
            this.#observe = new MutationObserver(([mutations]) => {
                if(mutations.type === 'attributes'){         //属性变动
                    this.#observeHandler()
                }
            })
            this.#observe.observe(this.#div,{
                attributes:true,      //监听属性的变动
                attributeOldValue:true  //表示观察attributes变动时，是否需要记录变动前的属性值。
            })            
        }
    }
    static all(pDataList){                   //相当于Array.prototype.every &&
        return new Promise((resolve,reject) => {
            let resultList = []
            let index = 0
            for (let i=0; i<pDataList.length; i++){
                let item = pDataList[i]
                if(toString.call(item) !== '[object Promise]'){
                    item = Promise.resolve(item)
                }
                item.then(result => {
                    resultList[i] = result
                    if(++index === pDataList.length){
                        resolve(resultList)
                    }
                
                },reject)
            }
        })
    }                      
    static any(pDataList){                   //相当于Array.prototype.some ||
        return new Promise((resolve,reject) => {
            let resultList = []
            let index = 0
            for (let i=0; i<pDataList.length; i++){
                let item = pDataList[i]
                if(toString.call(item) !== '[object Promise]'){
                    item = Promise.resolve(item)
                }
                item.then(resolve,result => {
                    resultList[i] = result
                    if(++index === pDataList.length){
                        reject(new AggregateError([ resultList ], 'All promises were rejected'))
                    }
                    
                })
            }
        })
    }
    static allSettled(pDataList){            //所有的Promise执行完成，allSettled返回的Promise就完成
        return new Promise((resolve) => {
            let resultList = []
            let index = 0
            for (let i=0; i<pDataList.length; i++){
                let item = pDataList[i]
                if(toString.call(item) !== '[object Promise]'){
                    item = Promise.resolve(item)
                }
                item.then(value => {
                    
                    resultList[i] = {
                        status: "fulfilled",
                        value
                    }
                
                },value => {
                    
                    resultList[i] = {
                        status: "rejected",
                        value
                    }
                    
                }).finally(()=>{
                    
                    if(++index === pDataList.length){
                        resolve(resultList)
                    }

                })
            }
        })
    }
    static race(pDataList){                  //执行最快的Promise来决定race返回的Promise状态
        return new Promise((resolve,reject) => {
            for (let item of pDataList){
                if(toString.call(item) !== '[object Promise]'){
                    item = Promise.resolve(item)
                }
                item.then(resolve,reject)
            }
        })
    }
    static resolve(value){         //返回一个成功状态的Promise
        return toString.call(value) === '[object Promise]' ? value : new Promise(resolve => resolve(value))
    }
    static reject(value){          //返回一个失败状态的Promise
        return toString.call(value) === '[object Promise]' ? value :  new Promise((resolve,reject) => reject(value))
    }
}