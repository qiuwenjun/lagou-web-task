import "reflect-metadata"
​
function L(type = 'log') {
    return function(target: any) {
        Reflect.defineMetadata("type", type, target);
    }
}
// 装饰器函数
function log() {
    return function(target: any, name: string, descriptor: PropertyDescriptor) {
            let value = descriptor.value;
            
        let type = Reflect.getMetadata("type", target);
​
        descriptor.value = function(a: number, b: number) {
            let result = value(a, b);
 
            if (type === 'log') {
                console.log('日志：', {
                  name,
                  a,
                  b,
                  result
                })
            }
            if (type === 'storage') {
                localStorage.setItem('storageLog', JSON.stringify({
                  name,
                  a,
                  b,
                  result
                }));
            }
            return result;
        }
    }
}
​
// 原始类
@L('log')
class M {
    @log()
    static add(a: number, b: number) {
        return a + b;
    }
    @log()
    static sub(a: number, b: number) {
        return a - b;
    }
}
​
let v1 = M.add(1, 2);
console.log(v1);
let v2 = M.sub(1, 2);
console.log(v2);

/** A1的说明 */
interface A1{
    /** A1.a的说明 */
    a:number
}

/** 
 * @property {string} name
 * @property {number} age
 * @property {'男'|'女'} sex
*/
interface Person{
    name:string
    age:number
    sex:'男'|'女'
}