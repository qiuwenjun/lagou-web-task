import fp = require('lodash/fp')

let _underscore = fp.replace(/\W+/g, '_')

let sanitizeNames = fp.map(fp.flowRight(fp.toLower, _underscore)) 
console.log(sanitizeNames(['Hello World']));


let _average = function (xs) {
    return fp.reduce(fp.add, 0, xs) / xs.length
  }
  
//   let averageDollorValue = function (cars) {
//     let dollar_values = fp.map(function(car) {
//       return car.dollar_value
//     }, cars)
//     return _average(dollar_values)
//   }

let averageDollorValue = fp.flowRight(_average,fp.map((car)=>car.dollar_value))