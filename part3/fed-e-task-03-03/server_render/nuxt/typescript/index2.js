function getAjAX(val) {
    if ('a' in val) {
        return val.a;
    }
    return val.b;
}
function example() {
    var x;
    x = Math.random() < 0.5;
    if (Math.random() < 0.5) {
        x = 'hello';
    }
    else {
        x = 100;
    }
    return x;
}
var x = example(); //x的类型是number|string的联合类型  
var bird = {
    name: "鸟",
    fly: function () { }
};
var fish = {
    name: "鱼",
    swim: function () { }
};
function isFish(type) {
    return 'swim' in type;
}
var value = Math.random() < 0.5 ? bird : fish;
if (isFish(value)) {
    value.swim();
}
else {
    value.fly();
}
function fn2(value) {
    return value;
}
fn2.desc = '123';
var FnDesc = fn2;
var Array2 = /** @class */ (function () {
    function Array2() {
    }
    return Array2;
}());
function fn(ctor) {
    return new ctor();
}
var s = fn(Array2);
