const _ = require('lodash')
console.dir(_.join)
console.dir(_.toLower)
console.dir(_.toUpper)
console.dir(_.split)
console.dir(_['flowRight'])

const split = _.curry((reg,str)=>str.split(reg))

const debug = _.curry((tag,val) => {
    console.log(tag,val)
    return val    
})

const map = _.curry((callback,data)=>_.map(data,callback))
const f = _.flowRight(_.join, debug('toLower'), map(_.toLower), debug('split'), split(' '))

console.log(f('NEVER SAY DIE'));

export {}