
module.exports = grunt => {
    
    grunt.initConfig({
        web_swig: {
            options: {
                swigOptions: {
                    cache: false
                },
                getData: function (tpl) {
                    return { myGruntTitle: 'hello,grunt-web-swig' };
                }
            },
            main: {
                expand: true,
                cwd: 'src/',
                src: "**/*.html",
                dest: "dist/"
            },
        },
    })

}