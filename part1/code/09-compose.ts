const arr = ['qiuwenjun','wujieyin']

function compose(...reset:Function[]){
    return args => reset.reduceRight((prev,fn)=>fn(prev),args)
}

let f = compose(e=>e.toUpperCase(),e=>e[0],e=>e.reverse())
console.log(f(arr))