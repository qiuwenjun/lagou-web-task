export {}
type Person = {
    name: string,
    age: number,
    isMen: boolean
}

// type name1 = Person['name']
// type name2 = Person[keyof Person]
type name3 = Person[keyof Person]

const MyArray = [
    { name:'邱文军', age:26 },
    { name:'程平宝', age:50 },
    { name:'邱宏卫', age:52 },
]
//MA = { name:string, age:number }
type MA = typeof MyArray[0]


interface A{
    name:string
}

interface B{
    age:number
}


type AorB<T> =  T extends ()=>infer K ? K : never
type S = AorB<()=>string>
// type InferParameters<T extends Function> = T extends (...args: any[]) => infer R ? R : never;
// type C = InferParameters<(a:number,b:string)=>number|string>
type C<A extends infer Number | String> = A extends String | Number ? 1 : '1'
type Fn<fn> = fn extends Array<infer item> ? item : never
type Fn1 = Fn<number[]>
type InferParameters<T extends Function> = T extends (...args: infer R) => any ? R : never;
type I = InferParameters<(a:number)=>any>
let a:I=[1]
type AorB<T> =  T extends number|string ? A : B 
function CreateObject<T extends string | number>(value:T):AorB<T>{
    type TYPE=AorB<T>
    if(typeof value === 'number'){
        return {
            name:'邱文军'
        } as TYPE
    }else{
        return {
            age:26
        } as TYPE
    }
}
CreateObject(1)

type InferArray<T> = T extends (infer K)[] ? K : never;
type I0 = InferArray<[number, string]>; // string | number
type I1 = InferArray<string[]>; // string
type I2 = InferArray<number[]>; // number

interface Arr{
    forEach:(this:Arr,callback:(item:keyof Arr)=>void)=>void
}
let Arr:Arr={
    forEach(callback){
        for(let attr in this){
            callback(attr)
        }
    }
}

type Shift<T> = T extends [infer L, ...infer R]? [...R] : [];
type S = Shift<[number,string,boolean]>

type StartsWith<T extends string, U extends string> = T extends `${U}${infer R}` ? true : false;

type D = StartsWith<'qiuwenjun','qiuwenjun'>
