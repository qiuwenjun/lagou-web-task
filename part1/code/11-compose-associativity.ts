const arr = ['qiuwenjun','wujieyin']

function compose(...reset:Function[]){
    return args => reset.reduceRight((prev,fn)=>fn(prev),args)
}

let f1 = compose((e:string)=>e.toUpperCase(),e=>e[0],e=>e.reverse())
let f2 = compose(compose((e:string)=>e.toUpperCase(),e=>e[0]),e=>e.reverse())
let f3 = compose((e:string)=>e.toUpperCase(),compose(e=>e[0],e=>e.reverse()))
console.log(f1(arr))
console.log(f2(arr))
console.log(f3(arr))

export {}