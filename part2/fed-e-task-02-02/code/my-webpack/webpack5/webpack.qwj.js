const Path = require('path');
// const PostCssPresetEnv = require('postcss-preset-env')
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const { DefinePlugin } = require('webpack')
const CopyWebpackPlugin = require('copy-webpack-plugin')

module.exports = {
    devtool:'source-map',
    mode:"development",
    entry:'./src/index.ts',
    output:{
        filename:'js/[name].[hash:6].js',
        path:Path.resolve(__dirname,'dist'),
        // publicPath:'',        //默认值 空字符串   打包后静态资源(dist,css,img)在服务器的存放位置
        // publicPath:'/',       //绝对路径
        // publicPath:'./'       //相对路径
    },
    resolve:{
        alias:{                                        
            "@":Path.resolve(__dirname,'src'),          
            "vue_3": Path.resolve(__dirname,'/node_modules/axios')
        },
        /*
            alias 为某些模块import或某些模块创建别名。require例如，给一堆常用src/文件夹起别名：
            "@":Path.resolve(__dirname,'src'),          //这里在项目中就可以通过@来省略./src 原本'./src/**' 现在'@/**'
        */
        extensions:['.ts','tsx','.js','jsx','.json'],
        /*
            extensions 默认 ['.js', '.json', '.wasm']
            省略扩展名称，当我们访问的时候可以省略掉扩展名，尝试按顺序解决这些扩展。
            如果多个文件共享相同的名称但具有不同的扩展名，webpack 将解析具有数组中第一个列出的扩展名的文件并跳过其余文件。
            当我们配置后，想要访问默认配置可以'...'
            ['.ts', '...'],
        */
        fallback:{                          
            Path:require.resolve('path')
        },
        /*
            fallback 正常解析失败时重定向模块请求。
            Webpack 5 不再自动填充 Node.js 核心模块，这意味着如果您在浏览器或类似工具中运行的代码中使用它们，您将必须从 npm 安装兼容的模块并自己包含它们。
            以下是 webpack 5 之前 webpack 使用的 polyfills 列表：
            resolve: {
                fallback: {
                    path: require.resolve('path-browserify'),
                },
            },
        */
        mainFiles:['index','main'],    
        /*
            mainFiles 默认值 ['index']
            当我们访问一个文件路径为'./component/'，这时候是在文件夹下，这时候当我们指定了字段后，它就会按照这个指定的配置查找对应名称文件
        */
       modules:['node_modules'],
       /*
            modules 默认值 ['node_modules']
            告诉 webpack 在解析模块时应该搜索哪些目录。
            [path.resolve(__dirname, 'src'), 'node_modules'],
       */
    },
    devServer:{
        hot:'only',
        compress:true,  //开启gzip压缩
        client:{
            webSocketURL:{
                port:9999
            }
        },
        open:false,
        historyApiFallback:true,
        proxy:{                 //具体配置可以参考官方文档 https://github.com/chimurai/http-proxy-middleware
            '/axios':{
                target:"http://cszsdt.niuhh.cn", //需要请求的地址http://cszsdt.niuhh.cn/api/auth
                pathRewrite:{                   
                    '^/axios':''
                },
                /*
                    重写路径
                    当我们访问http://localhost:8080/axios/api/auth ///api/auth这个相当于是请求参数
                    这时候代理会将我们的请求
                    http://cszsdt.niuhh.cn/axios/api/auth 拼接成这样，但是/axios/api/auth 并不是我想要的
                    这时候需要重写路径，将/axios/api/auth 重写成/api/auth
                    然后拼接到
                    http://cszsdt.niuhh.cn/api/auth 
                */

            }
        }
    },
    module:{
        rules:[
            {
                test:/\.css$/,
                use:[
                    "style-loader",
                    {
                        loader:"css-loader",
                        options:{
                            esModule:false,
                            importLoaders:1
                        }
                    },
                    'postcss-loader',
                    // {
                    //     loader:"postcss-loader",
                    //     options:{
                    //         postcssOptions:{
                    //             plugins:[
                    //                 PostCssPresetEnv()
                    //             ]
                    //         }
                    //     }
                    // }
                ]
            },
            {
                test:/\.less$/,
                use:[
                    "style-loader",
                    {
                        loader:"css-loader",
                        options:{
                            esModule:false,
                            importLoaders:1
                        }
                    },
                    'postcss-loader',
                    // {
                    //     loader:"postcss-loader",
                    //     options:{
                    //         postcssOptions:{
                    //             plugins:[
                    //                 require('postcss-preset-env')
                    //             ]
                    //         }
                    //     }
                    // },
                    "less-loader"
                ]
            },
            {
                test:/\.ts$/,
                exclude:/node_modules/,
                use:[
                    'babel-loader',
                    // 'ts-loader'
                ]
            },
            {
                test:/\.(jpe?g|png|gif|svg)$/,
                type:'asset',
                generator:{
                    filename:"./img/[name].[hash:6][ext]"
                },
                parser:{
                    dataUrlCondition:{
                        maxSize:1000*1024
                    }
                }
            },
            {
                test:/\.(ttf|woff2?)$/,
                type:'asset/resource',
                generator:{
                    filename:"./font/[name].[hash:6][ext]"
                },
            }
        ]
    },
    plugins:[
        new CleanWebpackPlugin(),
        new HtmlWebpackPlugin({
            title:'邱文军-webpack-学习',
            template:'./public/index.html'
        }),
        new DefinePlugin({
            "BASE_URL" : JSON.stringify('./')
        }),
        new CopyWebpackPlugin({
            "patterns":[
                {
                    from:"./public",
                    globOptions:{
                        ignore:['**/index.html']
                    }
                }
            ]
        })
    ]
}