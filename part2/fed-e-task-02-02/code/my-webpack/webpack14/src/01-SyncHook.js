const { SyncHook } = require('tapable')

const sync = new SyncHook(['name', 'age'])

sync.tap('fn1', function (name, age) {
  console.log(name, age)
})

sync.tap('fn2', function (name, age) {
  console.log(name, age)
})

sync.tap('fn3', function (name, age) {
  console.log(name, age)
})

sync.call('邱文军', 25)
