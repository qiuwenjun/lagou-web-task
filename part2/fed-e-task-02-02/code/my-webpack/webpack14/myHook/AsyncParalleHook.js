"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var Hook_1 = require("./Hook");
var HookFactory_1 = require("./HookFactory");
var AsyncFactory = /** @class */ (function (_super) {
    __extends(AsyncFactory, _super);
    function AsyncFactory() {
        var _this = _super.call(this) || this;
        _this.type = 'asyncFactoryHook';
        return _this;
    }
    AsyncFactory.prototype.setup = function (instance, options) {
        instance._x = options.taps.map(function (item) { return item.fn; });
        this.options = options;
    };
    AsyncFactory.prototype.create = function (options) {
        return new Function(this.args({ "after": "_callback" }), this.head() + this.content());
    };
    return AsyncFactory;
}(HookFactory_1["default"]));
var factory = new AsyncFactory();
var AsyncParalleHook = /** @class */ (function (_super) {
    __extends(AsyncParalleHook, _super);
    function AsyncParalleHook() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    AsyncParalleHook.prototype.compile = function () {
        factory.setup(this, {
            taps: this.taps,
            args: this.args
        });
        return factory.create({
            taps: this.taps,
            args: this.args
        });
    };
    return AsyncParalleHook;
}(Hook_1["default"]));
exports["default"] = AsyncParalleHook;
