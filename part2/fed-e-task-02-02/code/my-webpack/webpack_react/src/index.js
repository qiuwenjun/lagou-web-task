import React from "react";
import ReactDOM from "react-dom";
import App from './App.jsx';
import './js/title.js'

ReactDOM.render(<App />,document.getElementById('app'))

if(module.hot){
   module.hot.accept(['./js/title.js'],(file)=>{
      console.log(file+'文件更新了!');
   })
}