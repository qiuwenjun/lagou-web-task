## Vue.js 源码剖析-响应式原理、虚拟 DOM、模板编译和组件化

### 简答题

#### 1、请简述 Vue 首次渲染的过程。

```markdown
当Vue这个构造函数的静态方法，静态属性，以及原型上的方法和属性初始化完成后，当我们使用:
src/core/instance/index.js
首先调用_init方法，进行初始化
   1 首先进行实例属性和方法的初始化:
     (1) 包括组件初始化配置项以及使用配置项的合并
     (2) initLifecycle //$children/$parent/$root/$refs的初始化
     (3) initEvents //事件监听初始化, 父组件绑定在当前组件上的事件
     (4) initRender //编译render初始化 $slots/$scopedSlots/_c/$createElement/$attrs/$listeners
     (5) callHook(vm, 'beforeCreate') //执行beforeCreate的钩子回调
     (6) initInjections //把 inject 的成员注入到 vm 上
     (7) initState //初始化状态 vm 的 _props->methods->_data->computed->watch
     (8) initProvide //初始化 provide
     (9) callHook(vm, 'created') //created 生命钩子的回调
   2 初始化虚拟DOM，将内容渲染到视图层
     这里分两种情况:
        (1) 如果是运行时+编译器版本，这时候$mount方法会被重写，这时候会去判断有没有render函数，
            如果没有则会去找template模板，如果也没有传入template模板，那这时候会去查找当前
            元素所在html当作我们的模板，这时候通过compileToFunctions函数，将模板编译成render函数，
            并保存在options.render中
        (2) 当完成上一步判断，最后我们都可以成功过去到编译后的render函数，这时候调用我们的$mount方法
            内部调用mountComponent
        (3) mouontComponent 方法首先判断是否有render选项，如果没有但有模板，并且是开发环境发出警告，
            警告运行时版本不支持编译器。
        (4) 触发beforeMount生命周期钩子
        (5) 定义updateComponent方法，并初始化一个被观察者Watcher，调用Watcher实例对象的get方法，并立即执行updateComponent方法，将render方法中
            的虚拟DOM转换成正式的DOM，并挂载到页面上
        (6) 最后出发mounted钩子函数，返回Vue实例对象
        　
```

　

　

#### 2、请简述 Vue 响应式原理。

```markdown
在observe方法中判断传进来的数据是不是对象，如果不是对象则直接返回，如果是对象则调用Observer 方法，在内部调用Dep构造函数，创建一个dep实例对象
在Observer构造函数中在去判断是否是数组还是对象，分别做出不同的处理
    (1)数组的处理
       就是将改写后的数组的原型方法(pop,push,splice,reverse等能改变原数组内容的方法)设置为当前数组实例对象的原型方法
       然后在这些改写后的方法中，通过调用实例上所绑定的dep上的notify方法来更新watcher依赖
       并且通过调用observeArray方法循环数组上的每一个内容，进行判断是否为复合类型的值，如果是就递归调用observe
    (2)对象的处理
       调用walk方法，循环对象上的每一个属性，这里通过Object.keys来获取当前对象上的所有普通属性名称的集合
       在调用defineReactive方法，为每一个属性创建一个dep来收集依赖，
       通过defineProperty的get和set存取器，来对用户的读和写进行代理
        通过get来收集watcher依赖，通过set来调用watcher从而更新内容
       最后也需要通过递归调用observe来判断内容是否也是复合类型的值
至于数组为什么不对每一个下标内容进行响应式处理，因为数组的内容会很多，所以会带来性能问题
```

　

　

#### 3、请简述虚拟 DOM 中 Key 的作用和好处。

```markdown
减少操作DOM的次数，优化DOM操作从而提升性能
在updateChild方法中，利用比较虚拟DOM的key值是否相同，从而判断虚拟DOM是否是同一个元素，
能够更加精准的
```

　

　

#### 4、请简述 Vue 中模板编译的过程。

```markdown
　模板编译的主要目的是将模板 (template) 转换为渲染函数 (render)
　
　在入口文件entry-runtime-with-compiler.js文件中，改写Vue的$mount方法，$mount方法中
　调用compileToFunctions方法,传入要编译的模板和配置参数，先从缓存中加载编译好的render函数,
　如果缓存中没有就调用compile方法，compile方法首先合并配置参数，在当前方法中调用baseCompile方法
　
　baseCompile方法中共有三个核心函数
　    首先调用parse函数，将template模板转换成AST树，
　    在调用optimize函数，标记AST树中的静态内容，这样后续转换成虚拟DOM时，检测到是静态树，就不需要每次重新渲染节点内容，和patch节点内容
　    generate 根据Ast树生成js代码字符串，
　
　再回到compileToFunctions方法中，把上一步生成的字符串形式的js代码通过createFunction转换成函数
　这时候render和staticRederFns初始化完毕，挂载到当前组件的$options配置对象的render属性上去　

```

　