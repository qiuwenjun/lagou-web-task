var __spreadArray = (this && this.__spreadArray) || function (to, from, pack) {
    if (pack || arguments.length === 2) for (var i = 0, l = from.length, ar; i < l; i++) {
        if (ar || !(i in from)) {
            if (!ar) ar = Array.prototype.slice.call(from, 0, i);
            ar[i] = from[i];
        }
    }
    return to.concat(ar || Array.prototype.slice.call(from));
};
var Compiler = /** @class */ (function () {
    function Compiler(vm) {
        this.el = vm.$el;
        this.vm = vm;
        this.compile(this.el);
    }
    //编译方法Compiler
    Compiler.prototype.compile = function (el) {
        var _this = this;
        el.childNodes.forEach(function (item) {
            if (_this.isTextNode(item)) { //文本节点
                _this.compileText(item);
            }
            else if (_this.isElementNode(item)) { //元素节点
                _this.compileElement(item);
            }
            if (item.childNodes && item.childNodes.length) {
                _this.compile(item);
            }
        });
    };
    //编译指令中的内容
    Compiler.prototype.compileElement = function (node) {
        var _this = this;
        var attributes = node.attributes;
        __spreadArray([], attributes, true).forEach(function (item) {
            if (item.nodeName.startsWith('v-') || item.nodeName.startsWith('@')) {
                var name_1 = null;
                var attrName = null;
                if (item.nodeName.startsWith('@')) { //处理函数
                    name_1 = item.nodeName.replace(/^@(.+?)$/, '$1');
                    attrName = 'on';
                }
                else {
                    name_1 = item.nodeName.substr(2);
                    if (name_1.startsWith('on:')) { //处理函数
                        name_1 = name_1.replace(/^on:(.+?)$/, '$1');
                        attrName = 'on';
                    }
                    else {
                        return _this[name_1 + 'Updater'](node, item.textContent);
                    }
                }
                _this[attrName + 'Updater'](node, name_1, item.textContent);
            }
        });
    };
    //v-text指令
    Compiler.prototype.textUpdater = function (node, key) {
        node.textContent = this.vm[key];
        new Watcher(this.vm, key, function (value) {
            node.textContent = value;
        });
    };
    //v-html指令
    Compiler.prototype.htmlUpdater = function (node, key) {
        node.innerHTML = this.vm[key];
        new Watcher(this.vm, key, function (value) {
            node.innerHTML = value;
        });
    };
    //v-on指令
    Compiler.prototype.onUpdater = function (node, eventName, key) {
        var newFn = this.vm[key];
        if (!newFn) { //如果这里写的不是函数名称，而是函数的处理逻辑
            newFn = new Function('$event', key);
        }
        node.addEventListener(eventName, newFn, false);
    };
    //v-model指令
    Compiler.prototype.modelUpdater = function (node, key) {
        var _this = this;
        node.value = this.vm[key];
        new Watcher(this.vm, key, function (value) {
            node.value = value;
        });
        node.addEventListener('input', function () {
            _this.vm[key] = node.value;
        }, false);
    };
    //编译差值表达式中的内容
    Compiler.prototype.compileText = function (node) {
        var reg = /\{\{(.+?)\}\}/;
        var value = node.textContent;
        if (reg.test(value)) {
            var variable = RegExp.$1;
            node.textContent = value.replace(reg, this.vm[variable]);
            new Watcher(this.vm, variable, function (newVal) {
                node.textContent = value.replace(reg, newVal);
            });
        }
    };
    //判断属性是否是指令
    Compiler.prototype.isDirective = function (node) {
        return node.nodeType === 2 && node.nodeName.startsWith('v-');
    };
    //判断是否是文本节点
    Compiler.prototype.isTextNode = function (node) {
        return node.nodeType === 3;
    };
    //判断是否是元素节点
    Compiler.prototype.isElementNode = function (node) {
        return node.nodeType === 1;
    };
    return Compiler;
}());
