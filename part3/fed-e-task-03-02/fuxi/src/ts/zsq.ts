/*
 * @Author: 非洲小白狼 1501480244@qq.com
 * @Date: 2022-08-16 10:15:02
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2022-08-16 17:48:47
 * @FilePath: \fuxi\src\ts\zsq.ts
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */

// function log(a:,b,c){
//     console.log(arguments)
// }
import 'reflect-metadata';

@Reflect.metadata('')
class Person{

}
//构造函数的装饰器函数
function constructors(){
    console.log(arguments)
    return (fn:typeof User,...reset:any[])=>{
        console.log(fn,reset);
    }
}


function property(target:User,name:string){
    console.log('属性修饰符',arguments)
}

function properties(target:User,name:string){
    console.log('访问器修饰符',arguments)
}

function method(target:User,name:string){
    console.log('方法修饰符',arguments)
}

function arg(target:User,name:string,index:number){
    console.log('形参修饰符',arguments)

}
class User{
    @method
    sub(@arg num:string){

    }
    @property
    name=1
    @properties
    get name2(){
        return 1
    }

}

// let user = new User()