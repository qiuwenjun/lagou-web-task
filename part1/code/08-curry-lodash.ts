

function curry(callback){
    const len = callback.length
    function computed(...reset){
        if(reset.length >= len){
            return callback(...reset)
        }else{
            return computed.bind(null,...reset)
        }
    }
    return computed
}

let add = curry(function(a:number,b:number,c:number):number{
    return a+b+c
})
console.log(add(1,2,3))
console.log(add(1)(2)(3))
console.log(add(1,2)(3))
console.log(add(1)(2,3))

const match = curry(function(reg,str){
    return str.match(reg)
})

const haveSpace = match(/\s+/g)
const haveNumber = match(/\d+/g)

console.log(haveSpace('helloworld'));
console.log(haveNumber('123awd'));

const filter = curry(function(func,array){
    return array.filter(func)
})

console.log(filter(haveSpace,['John Connor', 'John_Donne']));

const findSpace = filter(haveSpace)
console.log(findSpace(['John Connor', 'John_Donne']));
