const Path = require('./path');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const MiniCssExtractWebpackPlugin = require('mini-css-extract-plugin');
const CssMinimizerWebpackPlugin = require('css-minimizer-webpack-plugin');
const CompressionPlugin = require('compression-webpack-Plugin');
const TerserPlugin = require('terser-webpack-plugin');
// const ImageMinimizerPlugin = require("image-minimizer-webpack-plugin");
// const { extendDefaultPlugins } = require("svgo");

module.exports = {
    mode:"production",
    optimization:{
        usedExports:true,
        minimizer:[
            "...",
            new TerserPlugin({
                extractComments:false
            }),
            new CssMinimizerWebpackPlugin(),
            // new ImageMinimizerPlugin({
            //   minimizer: {
            //     implementation: ImageMinimizerPlugin.imageminMinify,
            //     options: {
            //       // Lossless optimization with custom option
            //       // Feel free to experiment with options for better result for you
            //       plugins: [
            //         ["gifsicle", { interlaced: true }],
            //         ["jpegtran", { progressive: true }],
            //         ["optipng", { optimizationLevel: 5 }],
            //         // Svgo configuration here https://github.com/svg/svgo#configuration
            //         [
            //           "svgo",
            //           {
            //             plugins: extendDefaultPlugins([
            //               {
            //                 name: "removeViewBox",
            //                 active: false,
            //               },
            //               {
            //                 name: "addAttributesToSVGElement",
            //                 params: {
            //                   attributes: [{ xmlns: "http://www.w3.org/2000/svg" }],
            //                 },
            //               },
            //             ]),
            //           },
            //         ],
            //       ],
            //     },
            //   },
            // }),
        ],
        splitChunks:{
            chunks:'all',
        }
    },
    plugins:[
        new CleanWebpackPlugin(),
        new CopyWebpackPlugin({
            patterns:[
                {
                    from:"public",
                    globOptions:{
                        ignore:['**/index.html']
                    }
                }
            ]
        }),
        new MiniCssExtractWebpackPlugin(),
        new CompressionPlugin({
            test:/\.(css|js)$/,	//处理所有匹配此正则的资源
            threshold:10240,//只处理比这个值大的资源。按字节计算
            minRatio:0.8//只有压缩率比这个值小的资源才会被处理
        })
    ]
}