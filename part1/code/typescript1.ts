interface user{
    id:number
    name:string
    age:number
    updateName(newName:string):void
}
type NonFunctionPropertyNames<T> = {
    [K in keyof T]: T[K] extends Function ? never : K
}[keyof T]

type NonFunctionProperties<T> = Omit<T,NonFunctionPropertyNames<T>>;

type T7 = NonFunctionPropertyNames<user>
type T8 = NonFunctionProperties<user>