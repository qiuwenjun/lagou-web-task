// const Path = require('./path.js');
const Path = require('path')
const glob = require('glob')
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin')
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CssMinimizerPlugin = require('css-minimizer-webpack-plugin');
const PurgeCSSPlugin = require('purgecss-webpack-plugin');
const webpack = require('webpack')
const CompressionPlugin = require("compression-webpack-plugin")
module.exports = {
    // mode:"production",
    mode:'development',
    devtool:false,
    optimization:{
        minimizer:[
            new CssMinimizerPlugin()
        ],
        // providedExports:true,
        // usedExports:true,           //标记出源代码中没有用到的功能和代码
    },
    plugins:[
        new CleanWebpackPlugin(),
        new CopyWebpackPlugin({
            "patterns":[
                {
                    from:"./public",
                    globOptions:{
                        ignore:['**/index.html']
                    }
                }
            ]
        }),
        new MiniCssExtractPlugin({
            filename:"css/[name].[contenthash:8].css"
        }),
        new webpack.optimize.ModuleConcatenationPlugin(),
        new PurgeCSSPlugin({
            paths: glob.sync(`${Path.resolve(__dirname,'..','src')}/**/*`,  { nodir: true }),
            // paths: glob.sync(`${Path('src')}/**/*`,  { nodir: true }),
            safelist:['body','html','qiuwenjun']
        }),
        new CompressionPlugin({
            test:/\.(css|js)$/,
            threshold:5120
        })
    ]
}
